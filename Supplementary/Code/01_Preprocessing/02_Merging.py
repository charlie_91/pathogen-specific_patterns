# -*- coding: utf-8 -*-
"""
Created on Tue May 24 15:18:33 2022

@author: cholofss
"""

"""
Script containing the code for pre-processing of the TMLM and DELPRO files, 
and then merging of the two files. The files are merged using dictionaries 
containing a dataframe for each tag. A cowFrame is created containing all
the measurements. The dataframe for each tag and the one containing all
the meausrements are exported as csv files. 
"""

# =============================================================================
# Importing modules
# =============================================================================

# Data handling and plotting
# -----------------------------------------------------------------------------
import pyreadstat
import pandas as pd
import numpy as np


# =============================================================================
# Data 
# =============================================================================

# =============================================================================
# Raw files
# =============================================================================

# Robot data
# -----------------------------------------------------------------------------
delpro, meta_delpro = pyreadstat.read_sas7bdat('../data/delpro.sas7bdat')
variablesDelpro = list(delpro.columns)

delpro['tag'] = delpro['Dyrenummer'].astype(int)
uniqueDelpro = list(delpro.tag.unique())
uniqueDelpro.sort()
delpro['Dyrenummer'].isna().sum()

# Culture data
# -----------------------------------------------------------------------------
tmlm, meta_tmlm = pyreadstat.read_sas7bdat('../data/tmlm.sas7bdat')
variablesTmlm = list(tmlm.columns)

# Dropping samples with no tags
tmlm = tmlm.dropna(subset=['tag'])
tmlm['tag'] = tmlm['tag'].astype(int)
uniqueTmlm = list(tmlm.tag.unique())
uniqueTmlm.sort()

tmlm['date'] = pd.to_datetime(tmlm['visitdate'])
tmlmDescribe = tmlm.describe()
tmlmDescribe.to_excel('tmlmDescribe.xlsx')


# =============================================================================
# Culture data preprocessing
# =============================================================================

# Reading stata file
# -----------------------------------------------------------------------------
all_quarters = pd.read_stata('../data/dta/all_quarters_Re.dta')
quarters = list(all_quarters.quarter.unique())
tmlm = all_quarters.copy()
tmlm = tmlm.dropna(subset=['tag'])
tmlm['tag'] = tmlm['tag'].astype(int)
tmlmCows = list(tmlm.tag.unique())


# Changing nan to 0 -> negative result
# -----------------------------------------------------------------------------
for column in tmlm.columns:
    tmlm[column] = tmlm[column].replace([np.nan], 0)


# Changing 999 to nan -> not analysed
# -----------------------------------------------------------------------------
for column in tmlm.columns:
    tmlm[column] = tmlm[column].replace(['999', 999], np.nan)
    
    
# Adding binary values to mastitis treatment record
# -----------------------------------------------------------------------------
prevMastitis = []

for record in tmlm['mastitis']:
    if type(record) == int:
        prevMastitis.append(0)
    else:
        prevMastitis.append(1)
        
tmlm['prev_mastitis'] = prevMastitis
tmlm.prev_mastitis.describe()

# Describing processed file
# -----------------------------------------------------------------------------
tmlmDescribe = tmlm.describe()
tmlmDescribe.to_excel('tmlmDescribeAP.xlsx')


len(list(tmlm.tag.unique()))
len(list(tmlm.taglact.unique()))


# =============================================================================
# Looking at QMS-statistics
# =============================================================================

# Cow-samples
# -----------------------------------------------------------------------------
tmlmCow = tmlm[tmlm['sampletype'] == 'CMS']
len(list(tmlmCow.tag.unique()))
len(list(tmlmCow.taglact.unique()))

# Quarter samples
# -----------------------------------------------------------------------------
tmlmQms = tmlm[tmlm['sampletype'] == 'QMS']
len(list(tmlmQms.tag.unique()))
len(list(tmlmQms.taglact.unique()))

# Pathogens
aurBact = tmlmQms[tmlmQms['saurbact'] == 1]
aurCow = list(aurBact.taglact.unique())

epiBact = tmlmQms[tmlmQms['epibact'] == 1]
epiCow = list(epiBact.taglact.unique())

simuBact = tmlmQms[tmlmQms['simulbact'] == 1]
simuCow = list(simuBact.taglact.unique())

dysBact = tmlmQms[tmlmQms['dysbact'] == 1]
dysCow = list(dysBact.taglact.unique())

uberBact = tmlmQms[tmlmQms['uberbact'] == 1]
uberCow = list(uberBact.taglact.unique())

lactBact = tmlmQms[tmlmQms['lactobact'] == 1]
lactCow = list(lactBact.taglact.unique())

enterBact = tmlmQms[tmlmQms['enterbact'] == 1]
enterCow = list(enterBact.taglact.unique())

bovBact = tmlmQms[tmlmQms['bovbact'] == 1]
bovCow = list(bovBact.taglact.unique())

chromoBact = tmlmQms[tmlmQms['chromobact'] == 1]
chromoCow = list(chromoBact.taglact.unique())

hemoBact = tmlmQms[tmlmQms['hemobact'] == 1]
hemoCow = list(hemoBact.taglact.unique())

viriBact = tmlmQms[tmlmQms['viribact'] == 1]
viriCow = list(viriBact.taglact.unique())

homiBact = tmlmQms[tmlmQms['homibact'] == 1]
homiCow = list(homiBact.taglact.unique())

xyloBact = tmlmQms[tmlmQms['xylobact'] == 1]
xyloCow = list(xyloBact.taglact.unique())


# Removing samples with three or more species
# -----------------------------------------------------------------------------
tmlmQms = tmlmQms[tmlmQms['cfu3'] == 0]
tmlmQms = tmlmQms[tmlmQms['cfu4'] == 0]
len(list(tmlmQms.tag.unique()))
len(list(tmlmQms.taglact.unique()))

# OBS!
tmlm = tmlmQms.copy()


# =============================================================================
# Robot data preprocessing
# =============================================================================

# Only incuding values for EC that are [3, 12]
# -----------------------------------------------------------------------------
delpro = delpro[delpro['Konduktivitet_HF'] >= 3]
delpro = delpro[delpro['Konduktivitet_HF'] < 12]
delpro = delpro[delpro['Konduktivitet_VF'] >= 3]
delpro = delpro[delpro['Konduktivitet_VF'] < 12]
delpro = delpro[delpro['Konduktivitet_HB'] >= 3]
delpro = delpro[delpro['Konduktivitet_HB'] < 12]
delpro = delpro[delpro['Konduktivitet_VB'] >= 3]
delpro = delpro[delpro['Konduktivitet_VB'] < 12]


# Only including yield above 3.5 kg
# -----------------------------------------------------------------------------
delpro = delpro[delpro['Melkemengde__kg_'] > 3.5]


# Only including DIM from 5 to 305
# -----------------------------------------------------------------------------
delpro = delpro[delpro['Dager_i_melk'] >= 5]
delpro = delpro[delpro['Dager_i_melk'] < 305]


# Creating a variable for IQR between highest and lowest EC-value
# -----------------------------------------------------------------------------
EIQR = []
EMax = []
EAvg = []
EVar = []

for value in delpro.values:
    EC1, EC2, EC3, EC4 = value[14], value[15], value[16], value[17]
    EC = [EC1, EC2, EC3, EC4]
    ECMax, ECMin = max(EC), min(EC)
    ECAvg = sum(EC)/4
    IQR = ECMax/ECMin
    E1D, E2D, E3D, E4D = (EC1-ECAvg)**2, (EC2-ECAvg)**2, (EC3-ECAvg)**2, (EC4-ECAvg)**2
    ECD = [E1D, E2D, E3D, E4D]
    ECVar = sum(ECD)/4
    EMax.append(ECMax)
    EAvg.append(ECAvg)
    EIQR.append(IQR)
    EVar.append(round(ECVar, 5))

delpro['ECMax'] = EMax
delpro['ECAvg'] = EAvg    
delpro['EIQR'] = EIQR
delpro['ECVar'] = EVar


# Creating a variable for IQR between highest and lowest milk yield
# -----------------------------------------------------------------------------
MYIQR, MYFIQR, MYBIQR = [], [], []

for value in delpro.values:
    MY1, MY2, MY3, MY4 = value[23], value[24], value[25], value[26]
    MYF = [MY1, MY2]
    MYB = [MY3, MY4]
    MY = [MY1, MY2, MY3, MY4]
    MYMax, MYmin = max(MY), min(MY)
    MYFMax, MYFMin = max(MYF), min(MYF)
    MYBMax, MYBMin = max(MYB), min(MYB)
    IQR = MYMax/MYmin
    IQRF = MYFMax/MYFMin
    IQRB = MYBMax/MYBMin
    MYIQR.append(IQR)
    MYFIQR.append(IQRF)
    MYBIQR.append(IQRB)
    
delpro['MYIQR'], delpro['MYFIQR'], delpro['MYBIQR'] = MYIQR, MYFIQR, MYBIQR


# Creating a variable for IQR between highest and lowest average flow
# -----------------------------------------------------------------------------
FIQR = []

for value in delpro.values:
    F1, F2, F3, F4 = value[37], value[38], value[39], value[40]
    F = [F1, F2, F3, F4]
    FMax, Fmin = max(F), min(F)
    IQR = FMax/Fmin
    FIQR.append(IQR)
    
delpro['FIQR'] = FIQR


# Final robot cows
# -----------------------------------------------------------------------------
delpro['Dyrenummer'] = delpro['Dyrenummer'].astype(int)
delproCows = list(delpro.Dyrenummer.unique())


# Non-zero OCC
# -----------------------------------------------------------------------------
nonZeroOCC = delpro[delpro['OCC__1000_celler_ml_'] != 0]
len(list(nonZeroOCC.Dyrenummer.unique()))


# =============================================================================
# Creating dictionaries
# =============================================================================

"""
Creating a dictionary of cows, with a dataframe for each tag. 
The dataframe contains measurements from AMS. 
Setting the date as index for each dataframe.
"""
robotCows = {}

for cow in delproCows:
    temp_list = []
    for row in delpro.index:
        tag, date = delpro.loc[row, 'Dyrenummer'], delpro.loc[row, 'Dato']
        dim = delpro.loc[row, 'Dager_i_melk']
        station = delpro.loc[row, 'Melkestasjonsnummer']
        amount, occ = delpro.loc[row, 'Melkemengde__kg_'], delpro.loc[row, 'OCC__1000_celler_ml_']
        number = delpro.loc[row, 'Gruppenummer'],
        ECIQR, ECMax = delpro.loc[row, 'EIQR'], delpro.loc[row, 'ECMax']
        ECAvg = delpro.loc[row, 'ECAvg']
        ECVar = delpro.loc[row, 'ECVar']
        MYIQR, FIQR = delpro.loc[row, 'MYIQR'], delpro.loc[row, 'FIQR']
        MYFIQR, MYBIQR = delpro.loc[row, 'MYFIQR'], delpro.loc[row, 'MYBIQR']
        EC1, EC2 = delpro.loc[row, 'Konduktivitet_HF'], delpro.loc[row, 'Konduktivitet_VF']
        EC3, EC4 = delpro.loc[row, 'Konduktivitet_HB'], delpro.loc[row, 'Konduktivitet_VB']
        MY1, MY2 = delpro.loc[row, 'Mengde_HF'], delpro.loc[row, 'Mengde_VF']
        MY3, MY4 = delpro.loc[row, 'Mengde_HB'], delpro.loc[row, 'Mengde_VB']
        FA1, FA2 = delpro.loc[row, 'Gjennomsnittlig_melkestr_m_HF'], delpro.loc[row, 'Gjennomsnittlig_melkestr_m_VF']
        FA3, FA4 = delpro.loc[row, 'Gjennomsnittlig_melkestr_m_HB'], delpro.loc[row, 'Gjennomsnittlig_melkestr_m_VB']
        FM1, FM2 = delpro.loc[row, 'Maks_melkestr_m_HF'], delpro.loc[row, 'Maks_melkestr_m_VF']
        FM3, FM4 = delpro.loc[row, 'Maks_melkestr_m_HB'], delpro.loc[row, 'Maks_melkestr_m_VF']
        duration = delpro.loc[row, 'Total_varighet___dag']
        if tag == cow:
            temp_list.append([tag, date, dim, station, amount, occ, number,
                              ECIQR, ECMax, ECAvg, ECVar, MYIQR, FIQR, MYFIQR, MYBIQR, 
                              EC1, EC2, EC3, EC4, MY1, MY2, MY3, MY4, FA1, FA2,
                              FA3, FA4, FM1, FM2, FM3, FM4, duration])
    temp_frame = pd.DataFrame(temp_list, columns=['tag', 'date', 'dim', 
                                                  'station', 'amount', 'OCC',
                                                  'Number',
                                                  'ECIQR', 'ECMax', 'ECAvg', 
                                                  'ECVar',
                                                  'MYIQR', 'FIQR', 'MYFIQR',
                                                  'MYBIQR',
                                                  'EC1', 'EC2', 'EC3', 'EC4',
                                                  'MY1', 'MY2', 'MY3', 'MY4',
                                                  'FA1', 'FA2', 'FA3', 'FA4',
                                                  'FM1', 'FM2', 'FM3', 'FM4',
                                                  'duration'])
    
    # Setting date as index
    date = pd.DataFrame(temp_frame['date'])
    date['date'] = pd.to_datetime(date['date'])
    temp_frame['datetime'] = date
    temp_frame = temp_frame.set_index('datetime')
    temp_frame.index = temp_frame.index.date
    temp_frame = temp_frame.sort_index()
    
    # Removing OCC values that are 0
    temp_frame = temp_frame[temp_frame['OCC'] != 0]
    
    # Creating lnocc and lndim
    temp_frame['lnocc'] = np.log(np.array(temp_frame['OCC'].values))
    temp_frame['lndim'] = np.log(temp_frame['dim'].values)
    
    # Storing dateframe for each cow
    robotCows[cow] = temp_frame
        


"""
Creating a dictionary of cows, with a dataframe for each tag. 
The dataframe contains information about biological analysis from
the file that is pre-processed with STATA. 
Setting the date as index for each dataframe.
"""
processedCows = {}

for cow in tmlmCows:
    temp_list = []
    for row in tmlm.index:
        tag, date = tmlm.loc[row, 'tag'], tmlm.loc[row, 'visitdate']
        visit = tmlm.loc[row, 'visit']
        lactation, calvdate = tmlm.loc[row, 'lactation'], tmlm.loc[row, 'calvdate']
        taglact, prev_mastitis = tmlm.loc[row, 'taglact'], tmlm.loc[row, 'prev_mastitis']
        quarter, tagquarter = tmlm.loc[row, 'quarter'], tmlm.loc[row, 'tagquarter']
        cfu1, cfu2 = tmlm.loc[row, 'cfu1'], tmlm.loc[row, 'cfu2']
        cfu3, cfu4 = tmlm.loc[row, 'cfu3'], tmlm.loc[row, 'cfu4']
        aurBact, dysBact = tmlm.loc[row, 'saurbact'], tmlm.loc[row, 'dysbact']
        uberBact, enterBact = tmlm.loc[row, 'uberbact'], tmlm.loc[row, 'enterbact']
        lactoBact, epiBact = tmlm.loc[row, 'lactobact'], tmlm.loc[row, 'epibact']
        simuBact, bovBact = tmlm.loc[row, 'simulbact'], tmlm.loc[row, 'bovbact']
        chromoBact, hemoBact = tmlm.loc[row, 'chromobact'], tmlm.loc[row, 'hemobact']
        viriBact, homiBact = tmlm.loc[row, 'viribact'], tmlm.loc[row, 'homibact']
        xyloBact, otherBact = tmlm.loc[row, 'xylobact'], tmlm.loc[row, 'otherbact']
        maaTest, bactTest = tmlm.loc[row, 'maatest'], tmlm.loc[row, 'bactest']

        
        if tag == cow:
            temp_list.append([tag, date,  visit, lactation, calvdate, taglact, 
                              prev_mastitis, quarter, tagquarter, cfu1, cfu2, 
                              cfu3, cfu4, aurBact, dysBact, uberBact, 
                              enterBact, lactoBact, epiBact, simuBact, bovBact, 
                              chromoBact, hemoBact, viriBact, homiBact, 
                              xyloBact, otherBact, maaTest, bactTest])
    columns = ['tag', 'date', 'visit', 'lactation', 'calvdate', 'taglact', 'mastitis', 
               'quarter', 'tagquarter', 'cfu1', 'cfu2', 'cfu3', 'cfu4', 
               'aurBact', 'dysBact', 'uberBact', 'enterBact', 'lactoBact', 
               'epiBact', 'simuBact', 'bovBact', 'chromoBact', 'hemoBact',
               'viriBact', 'homiBact', 'xyloBact', 'otherBact',
               'maaTest', 'bactTest']
    temp_frame = pd.DataFrame(temp_list, columns=columns)
    
    # Setting date as index
    date = pd.DataFrame(temp_frame['date'])
    date['date'] = pd.to_datetime(date['date'])
    temp_frame['datetime'] = date
    temp_frame = temp_frame.set_index('datetime')
    temp_frame.index = temp_frame.index.date
    temp_frame = temp_frame.sort_index()

    # Storing dateframe for each cow
    processedCows[cow] = temp_frame


# Merging the two frames
# -----------------------------------------------------------------------------

cows = {}
unique = [cow for cow in delproCows if cow in tmlmCows]
unique.sort()

for cow in unique:
    temp_frame1 = robotCows[cow]
    temp_frame2 = processedCows[cow]
    joined_frame = temp_frame1.join(temp_frame2, lsuffix='tag')
    
    # Setting date as index
    date = pd.DataFrame(joined_frame['datetag'])
    date['datetag'] = pd.to_datetime(date['datetag'])
    joined_frame['datetime'] = date
    joined_frame = joined_frame.set_index('datetime', drop=False)
    joined_frame.index = joined_frame.index.date
    joined_frame = joined_frame.sort_index()
    
    # Storing dateframe for each cow
    cows[cow] = joined_frame


# Creating one dataframe with all the cows
# -----------------------------------------------------------------------------
cowFrame = pd.DataFrame()

for cow_ in cows:
    cow = cows[cow_]
    cowFrame = cowFrame.append(cow)
    
cowFrameCows = list(cowFrame.taglact.unique())
    
# =============================================================================
# Exporting files
# =============================================================================

# Exporting dataframes
# -----------------------------------------------------------------------------
for sheet, dataframe in cows.items():
    dataframe.to_csv(f'{sheet}.csv',index=False)
    
    
# Exporting to csv
# -----------------------------------------------------------------------------
cowFrame.to_csv('cowFrame.csv')


# Exporting to xlsx 
# -----------------------------------------------------------------------------
cowFrame.to_excel('cowFrame.xlsx')
