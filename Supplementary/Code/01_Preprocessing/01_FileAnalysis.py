# -*- coding: utf-8 -*-
"""
Created on Tue May 10 11:21:15 2022

@author: cholofss
"""

"""
A script containing some initial analysis of different files.
"""

# =============================================================================
# Importing modules
# =============================================================================

import pyreadstat
import pandas as pd
import numpy as np


# =============================================================================
# Data 
# =============================================================================

# Raw files
# -----------------------------------------------------------------------------
# Robot
delpro, meta_delpro = pyreadstat.read_sas7bdat('../data/delpro.sas7bdat')
delproDate = list(delpro['Dato'].unique())
gruppenummer = list(delpro.Gruppenummer.unique())
delproCows = list(delpro.Dyrenummer.unique())

# Bacteriology
tmlm, meta_tmlm = pyreadstat.read_sas7bdat('../data/tmlm.sas7bdat')
tmlmDate = list(tmlm['visitdate'].unique())
tmlmCows = list(tmlm.tag.unique())
# Dta file
tmlmDta = pd.read_stata('../data/170926 TMLM.dta')


# Merged file
# -----------------------------------------------------------------------------
ams = pd.read_csv('../data/171003Ams.csv')
amsDate = list(ams['date'].unique())
date = pd.DataFrame(ams['date'])
date['date'] = pd.to_datetime(date['date'])

variables = list(ams.columns)
uniqueCows = list(ams.tag.unique())

# Reducing the number of variables
variablesList = ['tag', 'date', 'lactation', 'melkesta', 'dim', 'melkemen', 
                 'occ', 'occavg', 'pat1', 'lnocc' ]
amsRed = ams[variablesList]

# Missing values
amsRed.isna().sum()

# Positive cultures
pat1 = amsRed.loc[amsRed['pat1'] == 1]
uniquePat1 = list(pat1['tag'].unique())
amsRed['occ'].isna().sum() 


# Smoothed file
# -----------------------------------------------------------------------------
amsS = pd.read_csv('../data/amsSmooth.csv')
variablesS = list(amsS.columns)
uniqueCowsS = list(amsS.tag.unique())

# Reducing the number of variables
variablesListS = ['tag', 'date', 'lactation', 'melkesta', 'dim', 'melkemen', 
                  'occ', 'occavg', 'pat1', 'taglact', 'lnocc', 'dailyyield',
                  'adj_occ', 'occdim', 'fc_occ', 'emr']
amsRedS = amsS[variablesListS]

# Missing values
amsRedS.isna().sum()

# Positive cultures
pat1S = amsRedS.loc[amsRedS['pat1'] == 1]
uniquePat1S = list(pat1S['tag'].unique())
amsRedS['occ'].isna().sum()


# Zadimi file
# -----------------------------------------------------------------------------
amsZ = pd.read_csv('../data/180206AmsZadimi.csv')
variablesZ = list(amsZ.columns)
uniqueCowsZ = list(amsZ.tag.unique())

# Reducing the number of variables
variablesListZ = ['tag', 'date', 'lactation', 'melkesta', 'dim', 'melkemen', 
                  'occ', 'occavg', 'pat1', 'taglact', 'lnocc', 'dailyyield',
                  'adj_occ', 'occdim', 'fc_occ', 'emr', 'pat1_tag', 'pat1cow',
                  'rounded_emr', 'rounded_occ']
amsRedZ = amsZ[variablesListZ]

emr = amsRedZ['emr']

# Missing values
amsRedZ.isna().sum()

# Positive cultures
pat1Z = amsRedZ.loc[amsRedZ['pat1'] == 1]
uniquePat1Z = list(pat1Z['tag'].unique())
amsRedZ['occ'].isna().sum()