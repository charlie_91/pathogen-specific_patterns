# -*- coding: utf-8 -*-
"""
Created on Mon Jun 20 11:00:46 2022

@author: cholofss
"""


"""
A script for separating lactations for each cow. 
Using taglact as unique ID. 
A lot of manual correction due to uneven lengths of lactations and 
periods of missing data.
"""

# =============================================================================
# Importing modules
# =============================================================================

# Data handling and plotting
# -----------------------------------------------------------------------------
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


# =============================================================================
# Data 
# =============================================================================

# Frame with all the cows
# -----------------------------------------------------------------------------
cowFrame = pd.read_csv('../data/cowframe.csv')
cowFrame = cowFrame.dropna(subset=['tagtag'])
cowFrame['tagtag'] = cowFrame['tagtag'].astype(int)
taglacts = list(cowFrame.taglact.unique())
cows = list(cowFrame.tag.unique())



# Dictionaries with frames for each cow individually
# -----------------------------------------------------------------------------
tags = list(cowFrame.tagtag.unique())
cows = {}

for sheet in tags:
    frame = pd.read_csv(f"../data/cows/{sheet}.csv")
    # Setting date as index
    date = pd.DataFrame(frame['datetag'])
    date['datetag'] = pd.to_datetime(date['datetag'])
    frame['datetime'] = date
    frame = frame.set_index('datetime')
    frame.index = frame.index.date
    frame = frame.sort_index()
    # Storing dateframe for each cow
    cows[sheet] = frame
    
    
# =============================================================================
# Creating separate frames for each lactation
# =============================================================================


# Manually correcting for short lactations before splitting
# -----------------------------------------------------------------------------
cows[5575] = cows[5575].loc[:pd.to_datetime('2016-01-17').date(), :]
cows[5613] = cows[5613].loc[pd.to_datetime('2016-11-02').date():, :]
cows[5651] = cows[5651].loc[pd.to_datetime('2015-12-25').date():, :]
cows[5851] = cows[5851].loc[pd.to_datetime('2015-12-12').date():, :]
cows[5854] = cows[5854].loc[pd.to_datetime('2016-02-18').date():, :]
cows[5903] = cows[5903].loc[pd.to_datetime('2016-01-13').date():pd.to_datetime('2016-08-16').date(), :]
cows[5921] = cows[5921].loc[pd.to_datetime('2015-12-05').date():, :]
cows[5928] = cows[5928].loc[pd.to_datetime('2016-01-15').date():pd.to_datetime('2016-09-19').date(), :]
cows[5946] = cows[5946].loc[pd.to_datetime('2015-08-14').date():pd.to_datetime('2016-05-29').date(), :]
cows[5989] = cows[5989].loc[pd.to_datetime('2015-12-01').date():pd.to_datetime('2016-07-04').date(), :]
cows[5990] = cows[5990].loc[pd.to_datetime('2015-12-31').date():, :]
cows[6021] = cows[6021].loc[pd.to_datetime('2016-01-05').date():pd.to_datetime('2016-03-11').date(), :]
cows[6063] = cows[6063].loc[pd.to_datetime('2016-01-12').date():pd.to_datetime('2016-10-03').date(), :]
cows[6072] = cows[6072].loc[pd.to_datetime('2016-02-11').date():pd.to_datetime('2016-11-09').date(), :]
cows[6144] = cows[6144].loc[pd.to_datetime('2015-12-20').date():pd.to_datetime('2016-10-01').date(), :]


# Lists for storing tagLact and dates that are more than 30 days 
# between eachother
# -----------------------------------------------------------------------------
tagLact = {}
splitDate = {}

for cow_ in cows:
    cow = cows[cow_]
    lastDate = []
    
    idx = list(cow.index)
    for i in range(len(idx)):
        if (idx[i] - idx[i-1]).days > 30:
            lastDate.append(idx[i])
            
    splitDate[cow_] = lastDate
    
    taglact = list(cow['taglact'].unique())
    taglactlist = [int(item) for item in taglact if str(item) != 'nan']
    tagLact[cow_] = taglactlist
    

# Manually correcting some errors
# -----------------------------------------------------------------------------
# tagLact
tagLact[5377] = [53771]
tagLact[5714] = [57145]
tagLact[5838] = [58384, 58385]
tagLact[5988] = [59882]
tagLact[5990] = [59902, 59903]
tagLact[6074] = [60742]
tagLact[6087] = [60871, 60872]
tagLact[6138] = [61381, 61382]
tagLact[6191] = [61911]
tagLact[6218] = [62181, 62182]

# splitDate
splitDate[5575] = [pd.to_datetime('2016-08-17').date()]
splitDate[5589] = []
splitDate[5613] = [pd.to_datetime('2016-08-17').date()]
splitDate[5613] = [pd.to_datetime('2016-06-14').date()]
splitDate[5704] = [pd.to_datetime('2016-09-14').date()]
splitDate[5725] = [pd.to_datetime('2016-05-04').date()]
splitDate[5729] = [pd.to_datetime('2016-05-26').date()]
splitDate[5733] = [pd.to_datetime('2016-07-04').date()]
splitDate[5830] = [pd.to_datetime('2016-05-30').date()]
splitDate[5840] = [pd.to_datetime('2016-05-30').date()]
splitDate[5851] = [pd.to_datetime('2016-07-04').date()]
splitDate[5854] = [pd.to_datetime('2016-11-10').date()]
splitDate[5879] = [pd.to_datetime('2016-05-30').date()]
splitDate[5979] = [pd.to_datetime('2016-07-04').date()]
splitDate[5990] = [pd.to_datetime('2016-10-13').date()]
splitDate[6044] = [pd.to_datetime('2016-06-07').date()]
splitDate[6138] = [pd.to_datetime('2016-08-13').date()]
splitDate[6167] = [pd.to_datetime('2016-10-14').date()]
splitDate[6168] = [pd.to_datetime('2016-10-03').date()]
splitDate[6170] = [pd.to_datetime('2016-10-30').date()]


# Creating a new dictionary with cows, using tagLact as key
# -----------------------------------------------------------------------------
cows2 = {}

for cow_ in cows:
    cow = cows[cow_]
    tag = list(cow['tagtag'].values)[0]
    
    if tag in tagLact.keys():
        tagList = tagLact[tag]
        
    if len(tagList) == 1:
        tagLactList = [tagList[0] for x in range(len(cow))]
        cow['tagLact'] = tagLactList
        cows2[tagList[0]] = cow
    
    elif len(tagList) > 1:
        tag1 = tagList[0]
        tag2 = tagList[1]
        print(splitDate[cow_])
        split = splitDate[cow_][0]
        
        list1 = cow.loc[:split, :]
        tagLactList1 = [tag1 for x in range(len(list1))]
        list1['tagLact'] = tagLactList1
        list2 = cow.loc[split:, :]
        tagLactList2 = [tag2 for x in range(len(list2))]
        list2['tagLact'] = tagLactList2
        
        cows2[tag1] = list1
        cows2[tag2] = list2
            
    else:
        print(tagList)
        

# More correction of errors
# -----------------------------------------------------------------------------

cows2[51464] = cows2[51464].iloc[:-2, :]
# cows2[56504] = cows2[56504].iloc[:-1, :]
cows2[56515] = cows2[56515].iloc[:-8, :]
# cows2[56755] = cows2[56755].iloc[8:, :]
cows2[57045] = cows2[57045].iloc[9:, :]
cows2[57144] = cows2[57145].iloc[:637,:]
cows2[57145] = cows2[57145].iloc[637:, :]
cows2[57214] = cows2[57214].iloc[39:, :]
cows2[57255] = cows2[57255].iloc[40:, :]
cows2[57295] = cows2[57295].iloc[1:, :]
cows2[57334] = cows2[57334].iloc[:-7, :]
cows2[57335] = cows2[57335].iloc[1:, :]
cows2[57564] = cows2[57564].iloc[:-3, :]
cows2[58304] = cows2[58304].iloc[1:, :]
cows2[58384] = cows2[58384].iloc[:-2, :]
cows2[58404] = cows2[58404].iloc[2:, :]
cows2[58433] = cows2[58433].iloc[:-2, :]
cows2[58453] = cows2[58453].iloc[:-8, :]
cows2[58513] = cows2[58513].iloc[:-7, :]
cows2[58514] = cows2[58514].iloc[1:, :]
cows2[58544] = cows2[58544].iloc[1:, :]
cows2[58794] = cows2[58794].iloc[1:, :]
cows2[58623] = cows2[58623].iloc[:-1, :]
cows2[58633] = cows2[58633].iloc[:-2, :]
cows2[58823] = cows2[58823].iloc[:-2, :]
cows2[58833] = cows2[58833].iloc[33:, :]
cows2[59102] = cows2[59103].iloc[:78, :]
cows2[59103] = cows2[59103].iloc[78:, :]
cows2[59123] = cows2[59123].iloc[89:, :]
cows2[59203] = cows2[59203].iloc[:-4, :]
cows2[59253] = cows2[59253].iloc[:-2, :]
cows2[59302] = cows2[59303].iloc[:128, :]
cows2[59303] = cows2[59303].iloc[128:, :]
# cows2[59372] = cows2[59372].iloc[:-2, :]
cows2[59393] = cows2[59393].iloc[:-2, :]
cows2[59632] = cows2[59632].iloc[:-12, :]
cows2[59772] = cows2[59772].iloc[:-2, :]
cows2[59792] = cows2[59792].iloc[:-7, :]
cows2[59793] = cows2[59793].iloc[1:, :]
cows2[59903] = cows2[59903].iloc[1:, :]
cows2[60252] = cows2[60252].iloc[:-3, :]
cows2[60432] = cows2[60432].iloc[:-3, :]
cows2[60443] = cows2[60443].iloc[3:, :]
# cows2[60571] = cows2[60571].iloc[:-1, :]
cows2[60741] = cows2[60742].iloc[:145, :]
cows2[60742] = cows2[60742].iloc[145:, :]
# cows2[60771] = cows2[60771].iloc[:-1, :]
cows2[60851] = cows2[60852].iloc[:197, :]
cows2[60852] = cows2[60852].iloc[197:, :]
cows2[60871] = cows2[60871].iloc[:-2, :]
cows2[60881] = cows2[60881].iloc[:-3, :]
cows2[60941] = cows2[60941].iloc[:-3, :]
cows2[61041] = cows2[61041].iloc[:-2, :]
cows2[61051] = cows2[61051].iloc[:-3, :]
cows2[61131] = cows2[61131].iloc[:-2, :]
cows2[61141] = cows2[61141].iloc[:-2, :]
cows2[61151] = cows2[61151].iloc[:-2, :]
cows2[61161] = cows2[61161].iloc[:-2, :]
cows2[61211] = cows2[61211].iloc[:-2, :]
cows2[61251] = cows2[61251].iloc[:-1, :]
cows2[61291] = cows2[61291].iloc[:-12, :]
cows2[61301] = cows2[61301].iloc[:-2, :]
cows2[61351] = cows2[61351].iloc[:-2, :]
cows2[61371] = cows2[61371].iloc[:-3, :]
cows2[61382] = cows2[61382].iloc[1:, :]
cows2[61411] = cows2[61411].iloc[:-1, :]
cows2[61431] = cows2[61431].iloc[:-3, :]
cows2[61531] = cows2[61531].iloc[:-3, :]
cows2[61551] = cows2[61551].iloc[:-3, :]
cows2[61561] = cows2[61561].iloc[:-2, :]
cows2[61601] = cows2[61601].iloc[:-2, :]
cows2[61631] = cows2[61631].iloc[:-4, :]
cows2[61672] = cows2[61672].iloc[1:, :]
cows2[61682] = cows2[61682].iloc[1:, :]
cows2[61691] = cows2[61691].iloc[:-3, :]
cows2[61741] = cows2[61741].iloc[:-3, :]
cows2[61781] = cows2[61781].iloc[:-2, :]
cows2[61791] = cows2[61791].iloc[:-1, :]
cows2[61841] = cows2[61841].iloc[:-3, :]
cows2[61951] = cows2[61951].iloc[:-4, :]
cows2[62131] = cows2[62131].iloc[:-1, :]
cows2[62181] = cows2[62181].iloc[:-3, :]
cows2[62291] = cows2[62291].iloc[:-2, :]
cows2[62521] = cows2[62521].iloc[:-4, :]


# Adding lactation number to all rows
# -----------------------------------------------------------------------------
for cow_ in cows2:
    cow = cows2[cow_]
    lact_ = list(cow['lactation'].unique())
    lact = [int(item) for item in lact_ if str(item) != 'nan']
    if len(lact) > 0:    
        lactlist = [lact[0] for x in range(len(cow))]
        cow['lactation'] = lactlist
    
    cow = cow.reset_index()
    cows2[cow_] = cow
    
    
# Adding binary columns for pathogen and parity
# -----------------------------------------------------------------------------
pathogens = ['aurBact', 'epiBact', 'simuBact', 'dysBact', 'uberBact', 
             'lactoBact', 'enterBact', 'bovBact', 'chromoBact', 
             'hemoBact', 'viriBact', 'homiBact', 'xyloBact']

for cow_ in cows2:
    cow = cows2[cow_]
    pathogen = []
    parity = []
    parity1 = []
    parity2 = []
    parity3 = []
    for row in cow.index:
        values = cow.loc[row]
        pat = values[pathogens]
        par = values['lactation']
        if pat.any() == 1:
            pathogen.append(1)
        else:
            pathogen.append(0)
        if par == 1:
            parity.append(1)
            parity1.append(1)
            parity2.append(0)
            parity3.append(0)
        elif par == 2:
            parity.append(2)
            parity1.append(0)
            parity2.append(1)
            parity3.append(0)
        elif par >= 3:
            parity.append(3)
            parity1.append(0)
            parity2.append(0)
            parity3.append(1)
        else:
            parity.append(np.nan)
            parity1.append(0)
            parity2.append(0)
            parity3.append(0)
            
    cow['Pathogen'] = pathogen
    cow['Parity'] = parity
    cow['Parity1'] = parity1
    cow['Parity2'] = parity2
    cow['Parity3'] = parity3


# Exporting dataframes
# -----------------------------------------------------------------------------
for sheet, dataframe in cows2.items():
    dataframe.to_csv(f"{sheet}.csv", index=False)


# Creating one dataframe with all the cows
# -----------------------------------------------------------------------------
cowFrame2 = pd.DataFrame()

for cow_ in cows2:
    cow = cows2[cow_]
    cowFrame2 = cowFrame2.append(cow)
    
# Exporting to csv
cowFrame2.to_csv('cowFrameTagLact.csv')  

"""
# Exporting to excel
cowFrame2.to_excel('cowFrameTagLact.xlsx')
cowFrame2_ = cowFrame2.reset_index()
cowFrame2_ = cowFrame2_.drop(columns=['index', 'calvdate'])
cowFrame2_.to_stata('cowFrameTagLact.dta')
"""
    
# -----------------------------------------------------------------------------
cowFrame2Cows = list(cowFrame2.tagtag.unique())
cowlact = list(cowFrame2.taglact.unique())
cowLact = list(cowFrame2.tagLact.unique())


