# -*- coding: utf-8 -*-
"""
Created on Tue Jun 21 09:23:53 2022

@author: cholofss
"""

"""
A script containing:
    * Code for obtaining the decriptive statistics. 
      The cows are divided into groups according to the pathogen that is causing 
      the infection and also into the different stages of lactation.
    * Visualisation of relationship between traits through scatterplots.
    * Pieplots of distribution of pathogens.
    * Distribution when separated by parity.
    * Correlation heatmaps can be created for the parities separately.
"""

# =============================================================================
# Importing modules
# =============================================================================

# Data handling and plotting
# -----------------------------------------------------------------------------
import pandas as pd
import numpy as np
import math
from scipy import stats
import matplotlib.pyplot as plt
import seaborn as sns


# =============================================================================
# Data 
# =============================================================================

# Frame with all the cows
# -----------------------------------------------------------------------------
cowFrame = pd.read_csv('../data/cowFrameTagLact.csv')
cowFrame = cowFrame.dropna(subset=['tagLact'])
cowFrame['tagLact'] = cowFrame['tagLact'].astype(int)
# Only QMS
cowFrame = cowFrame[cowFrame['quarter'] != 'COW']
lact = list(cowFrame.taglact.unique())
des = cowFrame.describe()


# Dictionaries with frames for each cow individually
# -----------------------------------------------------------------------------
tags = list(cowFrame.tagLact.unique())
cows = {}

for sheet in tags:
    frame = pd.read_csv(f"../data/cowsTagLact/{sheet}.csv")
    frame['dim'] = frame['dim'].astype(int)
    frame = frame[frame['quarter'] != 'COW']
    cows[sheet] = frame
    
    
# Statistics
# -----------------------------------------------------------------------------
cowFrame['count'] = [1 for x in range(len(cowFrame))]
milkings = cowFrame.groupby(by=cowFrame['dim']).sum()
milkings2 = cowFrame.groupby(by=cowFrame['datetag']).sum()
    

# =============================================================================
# Lactation
# =============================================================================

cowsL1 = {}
cowsL2 = {}
cowsL3 = {}

for cow_ in cows:
    cow = cows[cow_]
    cowL1 = cow[cow['dim'] <= 100]
    if len(cowL1) > 0:
        cowsL1[cow_] = cowL1
        
    cowL2 = cow[(cow['dim'] > 100) & (cow['dim'] <= 200)]
    if len(cowL2) > 0:
        cowsL2[cow_] = cowL2
        
    cowL3 = cow[cow['dim'] > 200]
    if len(cowL3) > 0:
        cowsL3[cow_] = cowL3
        
        
        
# Creating one dataframe with all the cows for each period
# -----------------------------------------------------------------------------
cowFrameL1 = pd.DataFrame()
cowFrameL2 = pd.DataFrame()
cowFrameL3 = pd.DataFrame()

for cow_ in cowsL1:
    cow = cowsL1[cow_]
    cowFrameL1 = cowFrameL1.append(cow)
    
for cow_ in cowsL2:
    cow = cowsL2[cow_]
    cowFrameL2 = cowFrameL2.append(cow)
    
for cow_ in cowsL3:
    cow = cowsL3[cow_]
    cowFrameL3 = cowFrameL3.append(cow)
    
    
# Number of cow lactations in each period
cl1 = list(cowFrameL1.tagLact.unique())
cl2 = list(cowFrameL2.tagLact.unique())
cl3 = list(cowFrameL3.tagLact.unique()) 
        

# =============================================================================
# Separating groups
# =============================================================================

cowFrame = cowFrame.copy()                                   
cows = cows.copy()


# Finding postive culture results for the different pathogens
# -----------------------------------------------------------------------------

# PAT-1
# -----------------------------------------------------------------------------

aurBact = cowFrame[cowFrame['aurBact'] == 1]
aurBactC = aurBact.drop_duplicates(subset=['tagtag', 'taglact',
                                           'datetag', 'quarter', 'cfu1'])
aurCow = list(aurBact.tagLact.unique())
aurDes = aurBact.describe()
aurVar = aurBact.var()

epiBact = cowFrame[cowFrame['epiBact'] == 1]
epiBactC = epiBact.drop_duplicates(subset=['tagtag', 'taglact',
                                           'datetag', 'quarter', 'cfu1'])
epiCow = list(epiBactC.tagLact.unique())
epiDes = epiBact.describe()

simuBact = cowFrame[cowFrame['simuBact'] == 1]
simuBactC = simuBact.drop_duplicates(subset=['tagtag', 'taglact',
                                             'datetag', 'quarter', 'cfu1'])
simuCow = list(simuBactC.tagLact.unique())
simuDes = simuBact.describe()

dysBact = cowFrame[cowFrame['dysBact'] == 1]
dysBactC = dysBact.drop_duplicates(subset=['tagtag', 'taglact', 
                                           'datetag', 'quarter', 'cfu1'])
dysCow = list(dysBactC.tagLact.unique())
dysDes = dysBact.describe()

uberBact = cowFrame[cowFrame['uberBact'] == 1]
uberBactC = uberBact.drop_duplicates(subset=['tagtag', 'taglact',
                                             'datetag', 'quarter', 'cfu1'])
uberCow = list(uberBactC.tagLact.unique())
uberDes = uberBact.describe()

lactBact = cowFrame[cowFrame['lactoBact'] == 1]
lactBactC = lactBact.drop_duplicates(subset=['tagtag', 'taglact',
                                             'datetag', 'quarter', 'cfu1'])
lactCow = list(lactBactC.tagLact.unique())
lactDes = lactBact.describe()

enterBact = cowFrame[cowFrame['enterBact'] == 1]
enterBactC = enterBact.drop_duplicates(subset=['tagtag', 'taglact',
                                               'datetag', 'quarter', 'cfu1'])
enterCow = list(enterBactC.tagLact.unique())
enterDes = enterBact.describe()

# PAT-2
# -----------------------------------------------------------------------------

bovBact = cowFrame[cowFrame['bovBact'] == 1]
bovBactC = bovBact.drop_duplicates(subset=['tagtag', 'taglact',
                                           'datetag', 'quarter', 'cfu1'])
bovCow = list(bovBactC.tagLact.unique())
bovDes = bovBact.describe()

chromoBact = cowFrame[cowFrame['chromoBact'] == 1]
chromoBactC = chromoBact.drop_duplicates(subset=['tagtag', 'taglact',
                                                 'datetag', 'quarter', 'cfu1'])
chromoCow = list(chromoBactC.tagLact.unique())
chromoDes = chromoBact.describe()

hemoBact = cowFrame[cowFrame['hemoBact'] == 1]
hemoBactC = hemoBact.drop_duplicates(subset=['tagtag', 'taglact',
                                             'datetag', 'quarter', 'cfu1'])
hemoCow = list(hemoBactC.tagLact.unique())
hemoDes = hemoBact.describe()

viriBact = cowFrame[cowFrame['viriBact'] == 1]
viriBactC = viriBact.drop_duplicates(subset=['tagtag', 'taglact',
                                             'datetag', 'quarter', 'cfu1'])
viriCow = list(viriBactC.tagLact.unique())
viriDes = viriBact.describe()

homiBact = cowFrame[cowFrame['homiBact'] == 1]
homiBactC = homiBact.drop_duplicates(subset=['tagtag', 'taglact',
                                             'datetag', 'quarter', 'cfu1'])
homiCow = list(homiBactC.tagLact.unique())
homiDes = homiBact.describe()

xyloBact = cowFrame[cowFrame['xyloBact'] == 1]
xyloBactC = xyloBact.drop_duplicates(subset=['tagtag', 'taglact',
                                             'datetag', 'quarter', 'cfu1'])
xyloCow = list(xyloBactC.tagLact.unique())
xyloDes = xyloBact.describe()


# List of frames and names
# -----------------------------------------------------------------------------

bact = [aurBact, epiBact, simuBact, dysBact, uberBact, lactBact, enterBact,
        bovBact, chromoBact, hemoBact, viriBact, homiBact, xyloBact]
bactNames = ['Aureus', 'Epidermidis', 'Simulans', 'Dysgalactiae', 
             'Uberis', 'Lactis', 'Enterococcus', 'Bovis', 'Chromogens',
             'Haemolyticus', 'Viridans', 'Hominis', 'Xylosus']


# Combining pathogens from the groups
# -----------------------------------------------------------------------------

# PAT-1
# -----------------------------------------------------------------------------
pat1Frame = pd.concat([aurBact, epiBact, simuBact, dysBact, uberBact,
                       lactBact, enterBact])
pat1CowList = list(pat1Frame.tagLact.unique())
pat1Des = pat1Frame.describe()
pat1Frame.to_csv('Pat1Frame.csv')


# Dictionary with PAT-1 lactations
# -----------------------------------------------------------------------------
pat1Cows = {}

for cow_ in pat1CowList:
    cow = cows[cow_]
    pat1Cows[cow_] = cow
    
for sheet, dataframe in pat1Cows.items():
    dataframe.to_csv(f'{sheet}.csv',index=False)


# PAT-2
# -----------------------------------------------------------------------------
pat2Frame = pd.concat([bovBact, chromoBact, hemoBact, viriBact, homiBact,
                       xyloBact])
pat2CowList = list(pat2Frame.tagLact.unique())
pat2Des = pat2Frame.describe()
pat2Frame.to_csv('Pat2Frame.csv')


# Dictionary with PAT-2 lactations
# -----------------------------------------------------------------------------
pat2Cows = {}

for cow_ in pat2CowList:
    cow = cows[cow_]
    pat2Cows[cow_] = cow
    
for sheet, dataframe in pat2Cows.items():
    dataframe.to_csv(f'{sheet}.csv',index=False)
    
    
# PAT-1 + PAT-2
# -----------------------------------------------------------------------------
infectionFrame = pd.concat([pat1Frame, pat2Frame])
infectionCowsList = list(infectionFrame.tagLact.unique())
infectionDes = infectionFrame.describe()
infectionFrame.to_csv('InfectionFrame.csv')


# Creating dictionary with infected lactations
# -----------------------------------------------------------------------------
infectionCows = {}

for cow_ in infectionCowsList:
    cow = cows[cow_]
    infectionCows[cow_] = cow
    
for sheet, dataframe in infectionCows.items():
    dataframe.to_csv(f'{sheet}.csv',index=False)
    


# Finding healthy cows
# -----------------------------------------------------------------------------

healthy, healthyCows, healthyFrame = [], {}, pd.DataFrame()

for cow in cows:
    if cow in pat1CowList:
        print('PAT-1')
    elif cow in pat2CowList:
        print('PAT-2')
    else:
        healthy.append(cow)
        healthyCows[cow] = cows[cow]


# Exporting dataframes for the baseline-cows
# -----------------------------------------------------------------------------

for sheet, dataframe in healthyCows.items():
    dataframe.to_csv(f'{sheet}.csv',index=False)


for cow_ in healthyCows:
    cow = healthyCows[cow_]
    healthyFrame = healthyFrame.append(cow)

healthyDes = healthyFrame.describe()
healtyVar = healthyFrame.var()

# Exporting to csv
healthyFrame.to_csv('HealthyFrame.csv') 


# =============================================================================
# Scatterplots
# =============================================================================

data = healthyFrame.copy()

# OCC vs Yield
# ----------------------------------------------------------------------------- 

x = data['amount']
y = data['OCC']
m, b = np.polyfit(x, y, 1)

plt.figure(figsize=(10,6), tight_layout=True)
ax = sns.scatterplot(data=data, x='amount', y='OCC', hue='station', 
                     palette='Set2', s=60)
ax.set(xlabel='Amount of milk [kg]', ylabel='OCC count [1000 cells/mL]')
ax.legend(title='Milking Station', title_fontsize = 12) 
ax.tick_params(axis='x', labelsize=12)
ax.tick_params(axis='y', labelsize=12)
ax.set_title('OCC count vs Yield', fontsize=16)
plt.plot(x, m*x+b)
plt.show()


# OCC vs ECIQR
# -----------------------------------------------------------------------------

plt.figure(figsize=(10,6), tight_layout=True)
ax = sns.scatterplot(data=data, x='ECIQR', y='OCC', hue='station', 
                     palette='Set2', s=60)
ax.set(xlabel='EC [IQR]', ylabel='OCC count [1000 cells/mL]')
ax.legend(title='Milking Station', title_fontsize = 12) 
ax.tick_params(axis='x', labelsize=12)
ax.tick_params(axis='y', labelsize=12)
ax.set_title('OCC count vs IQR of EC', fontsize=16)
plt.show()


# ECIQR vs Yield
# -----------------------------------------------------------------------------

x = data['amount']
y = data['ECIQR']
m, b = np.polyfit(x, y, 1)

plt.figure(figsize=(10,6), tight_layout=True)
ax = sns.scatterplot(data=data, x='amount', y='ECIQR',   hue='station', 
                     palette='Set2', s=60)
ax.set(xlabel='Amount of milk [kg]', ylabel='EC [IQR]')
ax.legend(title='Milking Station', title_fontsize = 12) 
ax.tick_params(axis='x', labelsize=12)
ax.tick_params(axis='y', labelsize=12)
ax.set_title('Yield vs IQR of EC', fontsize=16)
plt.plot(x, m*x+b)
plt.show()


# =============================================================================
# 3D heatmap
# =============================================================================
x = data['lnocc']
y = data['amount']
z = data['ECIQR']

# importing required libraries
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
from pylab import *

colo = np.random.randn(10, 1000)*1000
  
# creating 3d figures
fig = plt.figure(figsize=(10, 10))
ax = Axes3D(fig)
  
# configuring colorbar
color_map = cm.ScalarMappable(cmap=cm.gray)
color_map.set_array(colo)
  
# creating the heatmap
img = ax.scatter(x, y, z, marker='s',
                 s=100, color='gray')
plt.colorbar(color_map)
  
# adding title and labels
ax.set_title("3D Heatmap")
ax.set_xlabel('X-axis')
ax.set_ylabel('Y-axis')
ax.set_zlabel('Z-axis')
  
# displaying plot
plt.show()

# =============================================================================
# 3D heatmap
# =============================================================================
# creating 3d figures
fig = plt.figure(figsize=(8, 5))
ax = fig.add_subplot(111, projection='3d')
  
# configuring colorbar
color_map = cm.ScalarMappable(cmap=cm.gray)
color_map.set_array(colo)
  
# creating the heatmap
img = ax.scatter(x, y, z, marker='s', 
                 s=99, color='gray')
plt.colorbar(color_map)
  
# adding title and labels
ax.set_title("3D Heatmap")
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('')
  
# displaying plot
plt.show()


# =============================================================================
# 3D heatmap
# =============================================================================
import numpy as np
from pylab import *
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

def randrange(n, vmin, vmax):
    return (vmax-vmin)*np.random.rand(n) + vmin

fig = plt.figure(figsize=(8,6))

ax = fig.add_subplot(111,projection='3d')

colmap = cm.ScalarMappable(cmap=cm.hsv)
colmap.set_array(z)

yg = ax.scatter(x, y, z, c=cm.hsv(z/max(z)), marker='o')
cb = fig.colorbar(colmap)

ax.set_xlabel('X Label')
ax.set_ylabel('Y Label')
ax.set_zlabel('Z Label')


plt.show()


# =============================================================================
# 3D heatmap
# =============================================================================
import matplotlib.tri as mtri
from mpl_toolkits.mplot3d import Axes3D
triang = mtri.Triangulation(x, y)

fig = plt.figure()
ax = fig.add_subplot(1,1,1, projection='3d')

ax.plot_trisurf(triang, z, cmap='jet')
ax.scatter(x,y,z, marker='.', s=10, c="black", alpha=0.5)
ax.view_init(elev=60, azim=-45)

ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')
plt.show()

# =============================================================================
# Correlation
# =============================================================================

# Data to use
# -----------------------------------------------------------------------------
x = healthyFrame.copy()


# Converting duration to seconds
# -----------------------------------------------------------------------------
def get_seconds(time_str):
    mm, ss = time_str.split(':')
    return int(mm) * 60 + int(ss)

duration = pd.DataFrame(x['duration'])
durationSec = []

for dur in duration.values:
    durationSec.append(get_seconds(dur[0]))

x['Milking time'] = durationSec
x = x.drop(columns=['duration'])


# Correlation between the traits
# -----------------------------------------------------------------------------
xtraits = ['dim', 'amount', 'OCC', 'ECMax', 'ECAvg', 'ECIQR', 'MYIQR',
           'FIQR', 'Milking time', 'mastitis']

traits = x[xtraits]
traitsCorr = traits.corr()


# Plotting heatmap of correlations
# -----------------------------------------------------------------------------
labels = ['DIM', 'MY', 'OCC', 'EC (Max)', 'EC (Avg)', 'EC (IQR)', 'MY (IQR)',
          'Flow (IQR)', 'Milking time', 'Mastitis']


fig, ax = plt.subplots(figsize=(24,20))
sns.set_theme()
sns.heatmap(traitsCorr,
            vmin=-1,
            vmax=1,
            cmap="YlGnBu",
            cbar=True,
            annot=True,
            square=True,
            fmt='.2f')
ax.set_xticklabels(labels, rotation = 40, fontsize = 20)
ax.set_yticklabels(labels, rotation = 0, fontsize = 20)

