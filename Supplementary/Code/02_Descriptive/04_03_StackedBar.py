# -*- coding: utf-8 -*-
"""
Created on Tue Dec 20 13:13:59 2022

@author: cholofss
"""


# =============================================================================
# Importing modules
# =============================================================================

import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np


# =============================================================================
# Stages of lactation
# =============================================================================

# Figure
# -----------------------------------------------------------------------------
fig, axs = plt.subplots(figsize=(9, 6), tight_layout=True) 
sns.set_theme()

# Data
# -----------------------------------------------------------------------------
labels = ['Early lactation', 'Mid-lactation', 'Late-lactation']
aur = np.array([13, 10, 3])
dys = np.array([12, 6, 5])
lact = np.array([2, 4, 2])
simu = np.array([4, 2, 2])
epi = np.array([26, 25, 14])
uber = np.array([4, 3, 0])
enter = np.array([6, 3, 0])
width = 0.35       

# Plotting
# -----------------------------------------------------------------------------
axs.bar(labels, epi, label=r'$Staph. epidermidis$', color='#71C671')
axs.bar(labels, aur, bottom=epi, label=r'$Staph. aureus$', color='#458B74')
axs.bar(labels, dys, bottom=epi+aur, label=r'$Strep. dysgalactiae$', color='#006400')
axs.bar(labels, enter, bottom=epi+aur+dys, label=r'$Enterococcus \ spp.$', color='#191970')
axs.bar(labels, simu, bottom=epi+aur+dys+enter, label=r'$Staph. simulans$', color='#33A1C9')
axs.bar(labels, lact, bottom=epi+aur+dys+enter+simu, label=r'$Lactococcus \ lactis$', color='#87CEFF')
axs.bar(labels, uber, bottom=epi+aur+dys+enter+simu+lact, label=r'$Strep. uberis$', color='#836FFF')

"""
axs.bar(labels, aur, label='Aureus', color='#FFD39B')
axs.bar(labels, dys, bottom=aur, label='Dysgalactiae', color='#71C671')
axs.bar(labels, lact, bottom=aur+dys, label='Lactis', color='#8B4C39')
axs.bar(labels, simu, bottom=aur+dys+lact, label='Simulans', color='#B0E0E6')
axs.bar(labels, epi, bottom=aur+dys+lact+simu, label='Epidermidis', color='#CDAF95')
axs.bar(labels, uber, bottom=aur+dys+lact+simu+epi, label='Uberis', color='#00868B')
axs.bar(labels, enter, bottom=aur+dys+lact+simu+epi+uber, label='Enterococcus', color='#66CDAA')
"""
# Labels
# -----------------------------------------------------------------------------
axs.tick_params(axis='x', labelsize=14)
axs.tick_params(axis='y', labelsize=14)
axs.set_ylabel('Number of lactations', fontsize=16)
# axs.set_title('Distribution of pathogens for different stages of lactation', fontsize=18)
axs.legend(loc='lower right', bbox_to_anchor=(1.3, 0.5), fontsize=10)

plt.show()


# =============================================================================
# Parity
# =============================================================================

# Figure
# -----------------------------------------------------------------------------
fig, axs = plt.subplots(figsize=(9, 6), tight_layout=True) 
sns.set_theme()

# Data
# -----------------------------------------------------------------------------
labels = ['First parity', 'Second parity', 'Third or later parity']
aur = np.array([8, 6, 4])
dys = np.array([4, 6, 5])
lact = np.array([1, 0, 2])
simu = np.array([2, 0, 2])
epi = np.array([20, 11, 14])
uber = np.array([0, 0, 5])
enter = np.array([1, 2, 3])
width = 0.35       

# Plotting
# -----------------------------------------------------------------------------
axs.bar(labels, epi, label=r'$Staph. epidermidis$', color='#71C671')
axs.bar(labels, aur, bottom=epi, label=r'$Staph. aureus$', color='#458B74')
axs.bar(labels, dys, bottom=epi+aur, label=r'$Strep. dysgalactiae$', color='#006400')
axs.bar(labels, enter, bottom=epi+aur+dys, label=r'$Enterococcus \ spp.$', color='#191970')
axs.bar(labels, simu, bottom=epi+aur+dys+enter, label=r'$Staph. simulans$', color='#33A1C9')
axs.bar(labels, lact, bottom=epi+aur+dys+enter+simu, label=r'$Lactococcus \ lactis$', color='#87CEFF')
axs.bar(labels, uber, bottom=epi+aur+dys+enter+simu+lact, label=r'$Strep. uberis$', color='#836FFF')
"""
axs.bar(labels, aur, label='Aureus', color='#FFD39B')
axs.bar(labels, dys, bottom=aur, label='Dysgalactiae', color='#71C671')
axs.bar(labels, lact, bottom=aur+dys, label='Lactis', color='#8B4C39')
axs.bar(labels, simu, bottom=aur+dys+lact, label='Simulans', color='#B0E0E6')
axs.bar(labels, epi, bottom=aur+dys+lact+simu, label='Epidermidis', color='#CDAF95')
axs.bar(labels, uber, bottom=aur+dys+lact+simu+epi, label='Uberis', color='#00868B')
axs.bar(labels, enter, bottom=aur+dys+lact+simu+epi+uber, label='Enterococcus', color='#66CDAA')
"""
# Labels
# -----------------------------------------------------------------------------
axs.tick_params(axis='x', labelsize=14)
axs.tick_params(axis='y', labelsize=14)
axs.set_ylabel('Number of lactations', fontsize=16)
axs.set_ylim([0, 40])
# axs.set_title('Distribution of pathogens for different stages of lactation', fontsize=18)
axs.legend(loc='lower right', bbox_to_anchor=(1.3, 0.5), fontsize=10)

plt.show()