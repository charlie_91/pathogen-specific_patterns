# -*- coding: utf-8 -*-
"""
Created on Tue Jun 14 10:00:53 2022

@author: cholofss
"""

"""
A script for performing two-way ANOVA on parity-grouping and the
separation of lactation stage.
* Boxplots of distribution.
* Interaction plot.
* ANOVA statistics.
* QQ-plot of residuals.
* Plot of distribtuion of residuals.
"""


# =============================================================================
# Importing modules
# =============================================================================

# Data handling and plotting
# -----------------------------------------------------------------------------
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


# =============================================================================
# Data 
# =============================================================================
# =============================================================================
# All cows
# =============================================================================

# Frame
# -----------------------------------------------------------------------------
cowFrame = pd.read_csv('../data/CowFrameTagLact.csv')
cowFrame = cowFrame.dropna(subset=['tagLact'])
cowFrame['tagLact'] = cowFrame['tagLact'].astype(int)
cowFrame['dim'] = cowFrame['dim'].astype(int)
cowFrame = cowFrame[cowFrame['quarter'] != 'COW']

# Dictionary
# -----------------------------------------------------------------------------
tags = list(cowFrame.tagLact.unique())
cows = {}

for sheet in tags:
    frame = pd.read_csv(f'../data/CowsTagLact/{sheet}.csv')     
    frame['dim'] = frame['dim'].astype(int)
    frame = frame[frame['quarter'] != 'COW']
    cows[sheet] = frame
  

# =============================================================================
# Baseline    
# =============================================================================

# Frame 
# -----------------------------------------------------------------------------
healthyFrame = pd.read_csv('../data/healthyFrame.csv')
healthyFrame = healthyFrame.dropna(subset=['tagLact'])
healthyFrame['tagLact'] = healthyFrame['tagLact'].astype(int)
healthyFrame['dim'] = healthyFrame['dim'].astype(int)
healthyFrame = healthyFrame[healthyFrame['quarter'] != 'COW']

    
# Dictionary
# -----------------------------------------------------------------------------
tags = list(healthyFrame.tagLact.unique())
healthyCows = {}

for sheet in tags:
    frame = pd.read_csv(f'../data/Baseline/{sheet}.csv')     
    frame['dim'] = frame['dim'].astype(int)
    healthyCows[sheet] = frame
    
    
# =============================================================================
# Infected
# =============================================================================
    
# Frame
# -----------------------------------------------------------------------------
# When only including measurements with positive sample
# -----------------------------------------------------------------------------

infectionFrameM = pd.read_csv('../data/infectionFrame.csv')
infectionFrameM = infectionFrameM.dropna(subset=['tagLact'])
infectionFrameM['tagLact'] = infectionFrameM['tagLact'].astype(int)
infectionFrameM['dim'] = infectionFrameM['dim'].astype(int)
infectionFrameM = infectionFrameM.dropna(subset=['OCC'])
infectionFrameM = infectionFrameM[infectionFrameM['quarter'] != 'COW']

# Dictionary
# -----------------------------------------------------------------------------
tags = list(infectionFrameM.tagLact.unique())
infectionCowsM = {}

for sheet in tags:
    frame = pd.read_csv(f'../data/infectedCows/{sheet}.csv')     
    frame['dim'] = frame['dim'].astype(int)
    infectionCowsM[sheet] = frame


# =============================================================================
# Health status
# =============================================================================

infectionCows = {}

for cow_ in cows:
    cow = cows[cow_]
    if cow_ in healthyCows.keys():
        print('Healthy Cow!')
        cow['Health status'] = ['Baseline' for x in range(len(cow))]
    else:
        print('Not Healthy Cow!')
        cow['Health status'] = ['Infected' for x in range(len(cow))]
        infectionCows[cow_] = cow
        
        
# Updating cow frame
# -----------------------------------------------------------------------------
cowFrame = pd.DataFrame()

for cow_ in cows:
    cow = cows[cow_]
    cowFrame = cowFrame.append(cow)
        
    
# =============================================================================
# Infection Frame
# =============================================================================

# When including infected lactations
# -----------------------------------------------------------------------------
infectionFrameL = pd.DataFrame()

for cow_ in infectionCows:
    cow = cows[cow_]
    infectionFrameL = infectionFrameL.append(cow)
    
    

# =============================================================================
# OBS!
# =============================================================================

# What data to use
# -----------------------------------------------------------------------------
cows = infectionCowsM.copy()
cowFrame = infectionFrameM.copy()


# =============================================================================
# Lactation
# =============================================================================

# For dictionary
# -----------------------------------------------------------------------------
for cow_ in cows:
    cow = cows[cow_]
    period = []
    
    for idx in cow.index:
        row = cow.loc[idx, :]
        days = row['dim']
        if days <= 100:
            period.append('Early')
        elif ((days > 100) and (days <= 200)):
            period.append('Mid')
        elif days > 200:
            period.append('Rest')
            
    cow['Period'] = period
    
    
# For frame
# -----------------------------------------------------------------------------
lact = []
for row_ in cowFrame.index:
    row = cowFrame.loc[row_, 'dim']
    if row <= 100:
        lact.append('Early')
    elif ((row > 100) and (row <= 200)):
        lact.append('Mid')
    elif row > 200:
        lact.append('Rest')
        
cowFrame['Period'] = lact
    

     
"""
# Creating new dataframe with all the cows
# -----------------------------------------------------------------------------
cowFrame = pd.DataFrame()

for cow_ in cows:
    cow = cows[cow_]
    cowFrame = cowFrame.append(cow)
"""    
    
# =============================================================================
# Parity
# =============================================================================

parity1 = cowFrame[cowFrame['lactation'] == 1]
parity2 = cowFrame[cowFrame['lactation'] == 2]
parity3 = cowFrame[cowFrame['lactation'] >= 3]

parity = cowFrame['lactation']


# =============================================================================
# OCC
# =============================================================================

# Preprocessing
# -----------------------------------------------------------------------------

x1 = parity1[['Period', 'OCC', 'lnocc']].dropna()
x1['Parity'] = ['Parity 1' for x in range(len(x1))]
x2 = parity2[['Period', 'OCC', 'lnocc']].dropna()
x2['Parity'] = ['Parity 2' for x in range(len(x2))]
x3 = parity3[['Period', 'OCC', 'lnocc']].dropna()
x3['Parity'] = ['Parity 3+' for x in range(len(x3))]


xdf = pd.concat([x1, x2, x3])
xDf = xdf.reset_index()
xDf = xDf.sort_values(by=['Parity', 'Period'])

# Boxplot
# -----------------------------------------------------------------------------
fig = plt.figure(figsize =(12,7))
ax = fig.add_subplot(111)
sns.set_theme() 
sns.boxplot(x='Period', y='lnocc', hue='Parity', data=xDf, palette='YlGnBu') 

# Labels
ax.set_ylabel('ln(OCC) [1000 cells/mL]', fontsize=16)
ax.tick_params(axis='x', labelsize=15)
ax.tick_params(axis='y', labelsize=13)
ax.set_title('Cell count distribution for infected observations', fontsize=16)
ax.legend(loc='lower right', bbox_to_anchor=(1.1, 0.5))

# Show plot
plt.show()


# =============================================================================
# EC
# =============================================================================

# Preprocessing
# -----------------------------------------------------------------------------

x1 = parity1[['Period', 'ECIQR']].dropna()
x1['Parity'] = ['Parity 1' for x in range(len(x1))]
x2 = parity2[['Period', 'ECIQR']].dropna()
x2['Parity'] = ['Parity 2' for x in range(len(x2))]
x3 = parity3[['Period', 'ECIQR']].dropna()
x3['Parity'] = ['Parity 3+' for x in range(len(x3))]

xAll = cowFrame[['Period', 'ECIQR']].dropna().values

xdf = pd.concat([x1, x2, x3])
xDf = xdf.reset_index()


xDf = xDf.sort_values(by=['Parity', 'Period'])


# Boxplot
# -----------------------------------------------------------------------------
fig = plt.figure(figsize =(12,7))
ax = fig.add_subplot(111)
sns.set_theme() 
sns.boxplot(x='Period', y='ECIQR', hue='Parity', data=xDf, palette='YlGnBu') 


# Labels
ax.set_ylabel('EC [IQR]', fontsize=16)
ax.tick_params(axis='x', labelsize=15)
ax.tick_params(axis='y', labelsize=13)
ax.set_title('EC distribution for infected observations', fontsize=16)
ax.legend(loc='lower right', bbox_to_anchor=(1.1, 0.5))

# Show plot
plt.show()


# =============================================================================
# Yield
# =============================================================================

# Preprocessing
# -----------------------------------------------------------------------------

x1 = parity1[['Period', 'amount']].dropna()
x1['Parity'] = ['Parity 1' for x in range(len(x1))]
x2 = parity2[['Period', 'amount']].dropna()
x2['Parity'] = ['Parity 2' for x in range(len(x2))]
x3 = parity3[['Period', 'amount']].dropna()
x3['Parity'] = ['Parity 3+' for x in range(len(x3))]

xAll = cowFrame[['Period', 'amount']].dropna().values

xdf = pd.concat([x1, x2, x3])
xDf = xdf.reset_index()
xDf = xDf.sort_values(by=['Parity', 'Period'])


# Boxplot
# -----------------------------------------------------------------------------
fig = plt.figure(figsize =(12,7))
ax = fig.add_subplot(111)
sns.set_theme() 
sns.boxplot(x='Period', y='amount', hue='Parity', data=xDf, palette='YlGnBu') 


# Labels
ax.set_ylabel('Yield [kg]', fontsize=16)
ax.tick_params(axis='x', labelsize=15)
ax.tick_params(axis='y', labelsize=13)
ax.set_title('Yield distribution for infected observations', fontsize=16)
ax.legend(loc='lower right', bbox_to_anchor=(1.1, 0.5))

# Show plot
plt.show()