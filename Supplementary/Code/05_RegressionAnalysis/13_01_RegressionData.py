# -*- coding: utf-8 -*-
"""
Created on Mon Feb 13 10:17:06 2023

@author: cholofss
"""

"""
A script containg code for initial analysis of the data used for the 
regression analysis.
"""


# =============================================================================
# Modules
# =============================================================================

# Data handling and plotting
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


# =============================================================================
# Data
# =============================================================================

# Relative measures
dataStaph = pd.read_csv('../data/Regression/StaphAureusID.csv')
dataDys = pd.read_csv('../data/Regression/StrepDysgalactiaeID.csv')
dataSim = pd.read_csv('../data/Regression/StaphSimulansID.csv')
dataLact = pd.read_csv('../data/Regression/LactococcusLactisID.csv')
dataUber = pd.read_csv('../data/Regression/StrepUberisID.csv')

data = dataUber.copy()
data.isna().sum()
data = data[data['Period'] == 1]

# Distribution
path = data[data['Pathogen'] == 1]
base = data[data['Pathogen'] == 0]

data = data.drop(columns=['Unnamed: 0', 'Period'])
# data.columns = ['Tag', 'Date', 'tagLact', 'DIM', 'MY', 'OCC', 'ECIQR', 
#                 'MYIQR', 'FIQR', 'lnocc', 'Lactation', 'Pathogen', 
#                 'RelativeMeasure']


data.to_stata('StrepUberisPeriod.dta')
data.to_csv('StrepUberisPeriod.csv')
data.to_excel('StrepUberisPeriod.xlsx')

"""
# Dropping missing values
data = data.drop(columns=['RelativeTime', 'RelativeMeasure'])
data = data.dropna()

occ = data['Ln(OCC)']
occ.plot(kind='kde')
"""

# Distribution
path = data[data['Pathogen'] == 1]
base = data[data['Pathogen'] == 0]



