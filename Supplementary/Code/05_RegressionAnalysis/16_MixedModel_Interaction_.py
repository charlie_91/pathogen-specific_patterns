# -*- coding: utf-8 -*-
"""
Created on Thu Apr 27 07:50:03 2023

@author: cholofss
"""

"""
A script containg code for regression analysis.
    * Creating models
    * Checking assumptions
    * Plotting predictions (margins)
"""


# =============================================================================
# Importing modules
# =============================================================================

# Data handling and plotting
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# Statistics
import scipy.stats as stats
import statsmodels.api as sm
import statsmodels.formula.api as smf
import researchpy as rp
from statsmodels.stats.diagnostic import het_white


# =============================================================================
# Data
# =============================================================================

# New relativeTime ID
# -----------------------------------------------------------------------------
dataStaphID = pd.read_csv('../data/Regression/StaphAureusID.csv')
dataDysID = pd.read_csv('../data/Regression/StrepDysgalactiaeID.csv')
dataSimID = pd.read_csv('../data/Regression/StaphSimulansID.csv')
dataLactID = pd.read_csv('../data/Regression/LactococcusLactisID.csv')
dataUberID = pd.read_csv('../data/Regression/StrepUberisID.csv')
dataEpiID = pd.read_csv('../data/Regression/StaphEpidermidisID.csv')
dataEnterID = pd.read_csv('../data/Regression/EnterococcusID.csv')


# Lists for looping
# -----------------------------------------------------------------------------
# dataPathF = [dataStaphF, dataDysF, dataSimF, dataLactF, dataUberF, dataEpiF, dataEnterF]
dataPathID = [dataStaphID, dataDysID, dataSimID, dataLactID, dataUberID, dataEpiID, dataEnterID]
names = ['Aureus', 'Dysgalactiae', 'Simulans', 'Lactis', 'Uberis', 'Epidermidis', 'Enterococcus']
# OCC list
dataPathID = [dataStaphID, dataDysID, dataLactID, dataUberID, dataEpiID, dataEnterID]
names = ['Aureus', 'Dysgalactiae', 'Lactis', 'Uberis', 'Epidermidis', 'Enterococcus']
# ECIQR list
dataPathID = [dataDysID, dataSimID, dataLactID, dataEnterID]
names = ['Dysgalactiae', 'Simulans', 'Lactis', 'Enterococcus']
# MY list
dataPathID = [dataSimID, dataLactID, dataEpiID, dataEnterID]
names = ['Simulans', 'Lactis', 'Epidermidis', 'Enterococcus']

# =============================================================================
# Looping through the different pathogens
# =============================================================================

models = {}
modelResults = {}

for data, name in zip(dataPathID, names): 

    data = data.drop(columns=['Mastitis', 'RelativeTimePos'])
    data = data.dropna()
    
    # =========================================================================
    # Mixed models
    # =========================================================================
    
    
    vcf = {'Lactation':'0+C(tagLact)', 'Time':'0+C(RelativeTime)'}
    model = smf.mixedlm('MY ~ C(RelativeTime) + C(ParityGroup) + C(Pathogen) + C(RelativeTime)*C(Pathogen)',
                        data=data, groups=data['CaseID'], re_formula='1', vc_formula=vcf)
    
    md = model.fit()

    # Results
    results = md.summary()
    modelResults[name] = results
    

    # =========================================================================
    # Checking assumptions
    # =========================================================================
    """
    model = md
    
    # Normality
    # -------------------------------------------------------------------------
    # Distribution of residuals
    fig = plt.figure(figsize = (16, 9))
    sns.set_theme() 
    ax = sns.distplot(model.resid, hist=False, kde_kws={'shade' : True, 'lw': 1}, fit = stats.norm)
    ax.tick_params(axis='x', labelsize=16)
    ax.tick_params(axis='y', labelsize=16)
    ax.set_title('KDE Plot of Model Residuals (Blue) and Normal Distribution (Black)', fontsize=18)
    ax.set_xlabel('Residuals', fontsize=18)
    ax.set_ylabel('Density', fontsize=18)
    fig.savefig(str(name) + '_DistRes.pdf', bbox_inches='tight')


    # Q-Q PLot
    fig = plt.figure(figsize = (16, 9))
    sns.set_theme() 
    ax = fig.add_subplot(111)
    sm.qqplot(model.resid, dist = stats.norm, line = 's', ax = ax)
    ax.tick_params(axis='x', labelsize=16)
    ax.tick_params(axis='y', labelsize=16)
    ax.set_title('Q-Q Plot', fontsize=18)
    ax.set_xlabel('Theoretical Quantiles', fontsize=18)
    ax.set_ylabel('Sample Quantiles', fontsize=18)
    fig.savefig(str(name) + '_QQPlot.pdf', bbox_inches='tight')
    
    
    # Shapir Wilk test for normality
    labels = ['Statistic', 'p-value']
    norm_res = stats.shapiro(model.resid)
    for key, val in dict(zip(labels, norm_res)).items():
        print(key, val)
        
    """
    """
    Low p-value: The test is significant which indicates that the assumption 
    of normality for the residuals is violated.
    """
    """
    
    # Homoscendacity of variance
    # -------------------------------------------------------------------------
    # Residuals vs fitted values
    fig = plt.figure(figsize = (16, 9))
    sns.set_theme() 
    ax = sns.scatterplot(y = model.resid, x = model.fittedvalues)
    plt.axhline(y=0, linewidth=3) 
    ax.tick_params(axis='x', labelsize=16)
    ax.tick_params(axis='y', labelsize=16)
    ax.set_title('RVF Plot', fontsize=18)
    ax.set_xlabel('Fitted Values', fontsize=18)
    ax.set_ylabel('Residuals', fontsize=18)
    fig.savefig(str(name) + '_RVF.pdf', bbox_inches='tight')
    
    
    # Boxplot
    # Differences in variability between observations
    fig = plt.figure(figsize = (20, 9))
    sns.set_theme() 
    ax = sns.boxplot(x=model.model.groups, y=model.resid)
    ax.tick_params(axis='x', labelsize=14, rotation=90)
    ax.tick_params(axis='y', labelsize=16)
    ax.set_title('Distribution of Residuals for OCC by Groups', fontsize=18)
    ax.set_ylabel('Residuals', fontsize=18)
    ax.set_xlabel('Groups: Individual lactations', fontsize=18)
    fig.savefig(str(name) + '_DistResBoxGrouped.pdf', bbox_inches='tight')
    
    
    x=model.model.exog_re.reshape(-1)
    
    # White’s Lagrange Multiplier Test for Heteroscedasticity
    het_white_res = het_white(model.resid, model.model.exog)
    labels = ['LM Statistic', 'LM-Test p-value', 'F-Statistic', 'F-Test p-value']
    for key, val in dict(zip(labels, het_white_res)).items():
        print(key, val)
        
    """
    """
    Low p-values: formal testing indicates this assumption 
    (of homoskedasticity of the variance) is violated
    """
    
    
    
# =============================================================================
# Margins
# =============================================================================

modelsD = {}
coefficientsD = {}
stdD = {}


for data, name in zip(dataPathID, names): 
    
    data = data.drop(columns=['Mastitis', 'RelativeTimePos'])
    data = data.dropna()

    # Random intercept models
    # -------------------------------------------------------------------------
    vcf = {'Lactation':'0+C(tagLact)', 'Time':'0+C(RelativeTime)'}
    model = smf.mixedlm('ECIQR ~ C(RelativeTime) + C(ParityGroup) + C(Pathogen) + C(RelativeTime)*C(Pathogen)',
                        data=data, groups=data['CaseID'], re_formula='1', vc_formula=vcf)
    md = model.fit()
    
    results = md.summary()
    modelsD[name] = results

    # Coefficients
    params = md.params
    coefficientsD[name] = params
    
    # Standard errors
    std = results.tables[1]['Std.Err.']
    stdD[name] = std
    
    
# =============================================================================
# Replacing empty strings with 0 for coefficients and std
# =============================================================================

for path in stdD:
    series = stdD[path]
    series = series.replace('', '0')
    series = series.astype(float)
    stdD[path] = series
    

# =============================================================================
# Preparing data for plotting
# =============================================================================
# Data
tickList = [-15, -14, -13, -12, -11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0]
iL = [x for x in range(1, 16)]
jL = [x for x in range(19, 34)]

# Aureus
# -----------------------------------------------------------------------------
"""
# Coefficients
xA = pd.DataFrame(interA + pathA + coefficientsD['Aureus'].values[19:33], index=tickList)
xAC = pd.DataFrame(interA + coefficientsD['Aureus'].values[1:15], index=tickList)
# Standard errors
sA = pd.DataFrame(interAS + pathAS + stdD['Aureus'].values[19:33], index=tickList)
sAC = pd.DataFrame(interAS + stdD['Aureus'].values[1:15], index=tickList)
"""

# Coefficients
interA = coefficientsD['Aureus'].values[0]
pathA = coefficientsD['Aureus'].values[18]
xA, xAC = [], []
xA.append(interA+pathA)
xAC.append(interA)
for i, j in zip(iL, jL):
    xA.append(interA + pathA + (coefficientsD['Aureus'].values[j]))
    xAC.append(interA +  (coefficientsD['Aureus'].values[i]))
xA = pd.DataFrame(xA, index=tickList)
xAC = pd.DataFrame(xAC, index=tickList)
# Standard errors
interAS = stdD['Aureus'].values[0]
pathAS = stdD['Aureus'].values[18]
sA, sAC = [], []
sA.append(interAS+pathAS)
sAC.append(interAS)
for i, j in zip(iL, jL):
    sA.append(interAS + pathAS + (stdD['Aureus'].values[j]))
    sAC.append(interAS +  (stdD['Aureus'].values[i]))
sA = pd.DataFrame(sA, index=tickList)
sAC = pd.DataFrame(sAC, index=tickList)

xa, ya, ea = xA.index.values, xA.values.reshape(-1), sA.values.reshape(-1)
xac, yac, eac = xAC.index.values, xAC.values.reshape(-1), sAC.values.reshape(-1)
    

# Dysgalactiae
# -----------------------------------------------------------------------------
# Coefficients
interD = coefficientsD['Dysgalactiae'].values[0]
pathD = coefficientsD['Dysgalactiae'].values[18]
xD, xDC = [], []
xD.append(interD+pathD)
xDC.append(interD)
for i, j in zip(iL, jL):
    xD.append(interD + pathD + (coefficientsD['Dysgalactiae'].values[j]))
    xDC.append(interD + (coefficientsD['Dysgalactiae'].values[i]))
xD = pd.DataFrame(xD, index=tickList)
xDC = pd.DataFrame(xDC, index=tickList)
# Standard errors
interDS = stdD['Dysgalactiae'].values[0]
pathDS = stdD['Dysgalactiae'].values[18]
sD, sDC = [], []
sD.append(interDS+pathDS)
sDC.append(interDS)
for i, j in zip(iL, jL):
    sD.append(interDS + pathDS + stdD['Dysgalactiae'].values[j])
    sDC.append(interDS + stdD['Dysgalactiae'].values[i])
sD = pd.DataFrame(sD, index=tickList)
sDC = pd.DataFrame(sDC, index=tickList)

xd, yd, ed = xD.index[:-1].values, xD.values[:-1].reshape(-1), sD.values[:-1].reshape(-1)
xdc, ydc, edc = xDC.index[:-1].values, xDC.values[:-1].reshape(-1), sDC.values[:-1].reshape(-1)


# Simulans
# -----------------------------------------------------------------------------
# Coefficients
interS = coefficientsD['Simulans'].values[0]
pathS = coefficientsD['Simulans'].values[18]
xS, xSC = [], []
xS.append(interS+pathS)
xSC.append(interS)
for i, j in zip(iL, jL):
    xS.append(interS + pathS + (coefficientsD['Simulans'].values[j]))
    xSC.append(interS + (coefficientsD['Simulans'].values[i]))
xS = pd.DataFrame(xS, index=tickList)
xSC = pd.DataFrame(xSC, index=tickList)
# Standard errors
interSS = stdD['Simulans'].values[0]
pathSS = stdD['Simulans'].values[18]
sS, sSC = [], []
sS.append(interSS+pathSS)
sSC.append(interSS)
for i, j in zip(iL, jL):
    sS.append(interSS + pathSS + stdD['Simulans'].values[j])
    sSC.append(interSS + stdD['Simulans'].values[i])
sS = pd.DataFrame(sS, index=tickList)
sSC = pd.DataFrame(sSC, index=tickList)

xs, ys, es = xS.index[:-1].values, xS.values[:-1].reshape(-1), sS.values[:-1].reshape(-1)
xsc, ysc, esc  = xSC.index[:-1].values, xSC.values[:-1].reshape(-1), sSC.values[:-1].reshape(-1)


# Lactis
# -----------------------------------------------------------------------------
# Coefficients
interL = coefficientsD['Lactis'].values[0]
pathL = coefficientsD['Lactis'].values[18]
xL, xLC = [], []
xL.append(interL+pathL)
xLC.append(interL)
for i, j in zip(iL, jL):
    xL.append(interL + pathL + (coefficientsD['Lactis'].values[j]))
    xLC.append(interL + (coefficientsD['Lactis'].values[i]))
xL = pd.DataFrame(xL, index=tickList)
xLC = pd.DataFrame(xLC, index=tickList)
# Standard errors
interLS = stdD['Lactis'].values[0]
pathLS = stdD['Lactis'].values[18]
sL, sLC = [], []
sL.append(interLS+pathLS)
sLC.append(interLS)
for i, j in zip(iL, jL):
    sL.append(interLS + pathLS + stdD['Lactis'].values[j])
    sLC.append(interLS + stdD['Lactis'].values[i])
    # sL.append(stdD['Lactis'].values[i])
    # sLC.append(stdD['Lactis'].values[i])
sL = pd.DataFrame(sL, index=tickList)
sLC = pd.DataFrame(sLC, index=tickList)

xl, yl, el = xL.index[:-1].values, xL.values[:-1].reshape(-1), sL.values[:-1].reshape(-1)
xlc, ylc,elc  = xLC.index[:-1].values, xLC.values[:-1].reshape(-1), sLC.values[:-1].reshape(-1) 


# Uberis
# -----------------------------------------------------------------------------
# Coefficients
interU = coefficientsD['Uberis'].values[0]
pathU = coefficientsD['Uberis'].values[18]
xU, xUC = [], []
xU.append(interU+pathU)
xUC.append(interU)
for i, j in zip(iL, jL):
    xU.append(interU + pathU + (coefficientsD['Uberis'].values[j]))
    xUC.append(interU + (coefficientsD['Uberis'].values[i]))
xU = pd.DataFrame(xU, index=tickList)
xUC = pd.DataFrame(xUC, index=tickList)
# Standard errors
interUS = stdD['Uberis'].values[0]
pathUS = stdD['Uberis'].values[18]
sU, sUC = [], []
sU.append(interUS+pathUS)
sUC.append(interUS)
for i, j in zip(iL, jL):
    sU.append(interUS + pathUS + stdD['Uberis'].values[j])
    sUC.append(interUS + stdD['Uberis'].values[i])
sU = pd.DataFrame(sU, index=tickList)
sUC = pd.DataFrame(sUC, index=tickList)

xu, yu, eu = xU.index[:-1].values, xU.values[:-1].reshape(-1), sU.values[:-1].reshape(-1)
xuc, yuc, euc   = xUC.index[:-1].values, xUC.values[:-1].reshape(-1), sUC.values[:-1].reshape(-1)


# Enterococcus
# -----------------------------------------------------------------------------
# Coefficients
interE = coefficientsD['Enterococcus'].values[0]
pathE = coefficientsD['Enterococcus'].values[18]
xE, xEC = [], []
xE.append(interE+pathE)
xEC.append(interE)
for i, j in zip(iL, jL):
    xE.append(interE + pathE + (coefficientsD['Enterococcus'].values[j]))
    xEC.append(interE + (coefficientsD['Enterococcus'].values[i]))
xE = pd.DataFrame(xE, index=tickList)
xEC = pd.DataFrame(xEC, index=tickList)
# Standard errors
interES = stdD['Enterococcus'].values[0]
pathES = stdD['Enterococcus'].values[18]
sE, sEC = [], []
sE.append(interES+pathES)
sEC.append(interES)
for i, j in zip(iL, jL):
    sE.append(interES + pathES + stdD['Enterococcus'].values[j])
    sEC.append(interES + stdD['Enterococcus'].values[i])
sE = pd.DataFrame(sE, index=tickList)
sEC = pd.DataFrame(sEC, index=tickList)

xe, ye, ee = xE.index[:-1].values, xE.values[:-1].reshape(-1), sE.values[:-1].reshape(-1)
xec, yec, eec = xEC.index[:-1].values, xEC.values[:-1].reshape(-1), sEC.values[:-1].reshape(-1)


# Epidermidis
# -----------------------------------------------------------------------------
# Coefficients
interEp = coefficientsD['Epidermidis'].values[0]
pathEp = coefficientsD['Epidermidis'].values[18]
xEp, xEpC = [], []
xEp.append(interEp+pathEp)
xEpC.append(interEp)
for i, j in zip(iL, jL):
    xEp.append(interEp + pathEp + (coefficientsD['Epidermidis'].values[j]))
    xEpC.append(interEp + (coefficientsD['Epidermidis'].values[i]))
xEp = pd.DataFrame(xEp, index=tickList)
xEpC = pd.DataFrame(xEpC, index=tickList)
# Standard errors
interEpS = stdD['Epidermidis'].values[0]
pathEpS = stdD['Epidermidis'].values[18]
sEp, sEpC = [], []
sEp.append(interEpS+pathEpS)
sEpC.append(interEpS)
for i, j in zip(iL, jL):
    sEp.append(interEpS + pathEpS + stdD['Epidermidis'].values[j])
    sEpC.append(interEpS + stdD['Epidermidis'].values[i])
sEp = pd.DataFrame(sEp, index=tickList)
sEpC = pd.DataFrame(sEpC, index=tickList)

xep, yep, eep = xEp.index[:-1].values, xEp.values[:-1].reshape(-1), sEp.values[:-1].reshape(-1)
xepc, yepc, eepc = xEpC.index[:-1].values, xEpC.values[:-1].reshape(-1), sEpC.values[:-1].reshape(-1)


# =============================================================================
# Figure
# =============================================================================

fig, axs = plt.subplots(figsize=(15, 7), tight_layout=True) 
sns.set_theme()
# fig.suptitle('Margins-Plot', fontsize=20)

axs.errorbar(xa, ya, yerr=ea, fmt='-o', color='#48D1CC', label=r'$Staph. aureus \ cases $')
axs.errorbar(xd, yd, yerr=ed, fmt='-o', color='#1874CD', label=r'$Strep. dysgalactiae \ cases $')
# axs.errorbar(xs, ys, yerr=es, fmt='-o', color='#9A32CD', label=r'$Staph. simulans \ cases $')
# axs.errorbar(xl, yl, yerr=el, fmt='-o', color='#FF7D40', label=r'$Lactococcus \ lactis \ cases $')
axs.errorbar(xu, yu, yerr=eu, fmt='-o', color='#8A3324', label=r'$Strep. uberis \ cases $')
axs.errorbar(xe, ye, yerr=ee, fmt='-o', color='#CD4F39', label=r'$Enterococcus \ sp. \ cases $')
axs.errorbar(xep, yep, yerr=eep, fmt='-o', color='#FFC125', label=r'$Staph. epidermidis \ cases $')

axs.errorbar(xac, yac, yerr=eac, fmt='-o', color='#006400', label=r'$Staph. aureus \ controls $')
axs.errorbar(xdc, ydc, yerr=edc, fmt='-o', color='#3D9140', label=r'$Strep. dysgalactiae \ control $')
# axs.errorbar(xsc, ysc, yerr=esc, fmt='-o', color='#66CD00', label=r'$Staph. simulans \ control $')
# axs.errorbar(xlc, ylc, yerr=elc, fmt='-o', color='#228B22', label=r'$Lactococcus \ lactis \ controls $')
axs.errorbar(xuc, yuc, yerr=euc, fmt='-o', color='#BCEE68', label=r'$Strep. uberis \ control $')
axs.errorbar(xec, yec, yerr=eec, fmt='-o', color='#6E8B3D', label=r'$Enterococcus \ sp. \ controls $')
axs.errorbar(xepc, yepc, yerr=eepc, fmt='-o', color='#76EE00', label=r'$Staph. epidermidis \ controls $')


# axs.set_xticklabels(tickList)
axs.tick_params(axis='x', labelsize=16)
axs.tick_params(axis='y', labelsize=16)
axs.set_ylabel('Predicted values for ln(OCC)', fontsize=20)
axs.set_xlabel('Days relative to sample', fontsize=20)
axs.legend(loc='lower right', bbox_to_anchor=(1.3, 0.3), fontsize=14)

plt.show()


# =============================================================================
# Figure with 4 subplots, OCC
# =============================================================================

fig, axs = plt.subplots(2,2, figsize=(15, 15), 
                        sharex=True, sharey=True)
sns.set_theme()

axs[0, 0].plot(xa, ya, marker='o', color='#48D1CC', label=r'$Staph. aureus \ cases $')
axs[0, 0].fill_between(xa, ya-ea, ya+ea, color='#48D1CC', alpha=0.5)
axs[0, 0].plot(xac, yac, marker='o', color='#006400', label=r'$Staph. aureus \ controls $')
axs[0, 0].fill_between(xac, yac-eac, yac+eac, color='#006400', alpha=0.5)
axs[0, 0].legend(loc='lower right', bbox_to_anchor=(1.1, 0.85), fontsize=14)
axs[0, 0].tick_params(axis='y', labelsize=16)
axs[0, 0].set_ylabel('Predicted values for ln(OCC)', fontsize=20)

axs[0, 1].plot(xd, yd, marker='o', color='#1874CD', label=r'$Strep. dysgalactiae \ cases $')
axs[0, 1].fill_between(xd, yd-ed, yd+ed, color='#1874CD', alpha=0.5)
axs[0, 1].plot(xdc, ydc, marker='o', color='#3D9140', label=r'$Strep. dysgalactiae \ control $')
axs[0, 1].fill_between(xdc, ydc-edc, ydc+edc, color='#3D9140', alpha=0.5)
axs[0, 1].legend(loc='lower right', bbox_to_anchor=(1.1, 0.85), fontsize=14)

axs[1, 0].plot(xu, yu, marker='o', color='#8A3324', label=r'$Strep. uberis \ cases $')
axs[1, 0].fill_between(xu, yu-eu, yu+eu, color='#8A3324', alpha=0.5)
axs[1, 0].plot(xuc, yuc, marker='o', color='#BCEE68', label=r'$Strep. uberis \ control $')
axs[1, 0].fill_between(xuc, yuc-euc, yuc+euc, color='#BCEE68', alpha=0.5)
axs[1, 0].legend(loc='lower right', bbox_to_anchor=(1.1, 0.85), fontsize=14)
axs[1, 0].tick_params(axis='x', labelsize=16)
axs[1, 0].tick_params(axis='y', labelsize=16)
axs[1, 0].set_ylabel('Predicted values for ln(OCC)', fontsize=20)
axs[1, 0].set_xlabel('Days relative to sample', fontsize=20)

axs[1, 1].plot(xep, yep, marker='o', color='#FFC125', label=r'$Staph. epidermidis \ cases $')
axs[1, 1].fill_between(xep, yep-eep, yep+eep, color='#FFC125', alpha=0.5)
axs[1, 1].plot(xepc, yepc, marker='o', color='#76EE00', label=r'$Staph. epidermidis \ controls $')
axs[1, 1].fill_between(xepc, yepc-eepc, yepc+eepc, color='#76EE00', alpha=0.5)

axs[1, 1].legend(loc='lower right', bbox_to_anchor=(1.1, 0.85), fontsize=14)
axs[1, 1].tick_params(axis='x', labelsize=16)
axs[1, 1].set_xlabel('Days relative to sample', fontsize=20)

plt.show()


# =============================================================================
# Figure with 4 subplots, EC
# =============================================================================

fig, axs = plt.subplots(2,2, figsize=(15, 15), 
                        sharex=True, sharey=True)
sns.set_theme()

axs[0, 0].plot(xd, yd, marker='o', color='#1874CD', label=r'$Strep. dysgalactiae \ cases $')
axs[0, 0].fill_between(xd, yd-ed, yd+ed, color='#1874CD', alpha=0.5)
axs[0, 0].plot(xdc, ydc, marker='o', color='#3D9140', label=r'$Strep. dysgalactiae \ control $')
axs[0, 0].fill_between(xdc, ydc-edc, ydc+edc, color='#3D9140', alpha=0.5)
axs[0, 0].legend(loc='lower right', bbox_to_anchor=(1.1, 0.85), fontsize=14)
axs[0, 0].tick_params(axis='y', labelsize=16)
axs[0, 0].set_ylabel('Predicted values for EC(IQR)', fontsize=20)

axs[0, 1].plot(xs, ys, marker='o', color='#48D1CC', label=r'$Staph. simulans \ cases $')
axs[0, 1].fill_between(xs, ys-es, ys+es, color='#48D1CC', alpha=0.5)
axs[0, 1].plot(xsc, ysc, marker='o', color='#006400', label=r'$Staph. simulans \ controls $')
axs[0, 1].fill_between(xsc, ysc-esc, ysc+esc, color='#006400', alpha=0.5)
axs[0, 1].legend(loc='lower right', bbox_to_anchor=(1.1, 0.85), fontsize=14)

axs[1, 0].plot(xl, yl, marker='o', color='#1874CD', label=r'$Lactococcus \ lactis \ cases $')
axs[1, 0].fill_between(xl, yl-el, yl+el, color='#1874CD', alpha=0.5)
axs[1, 0].plot(xlc, ylc, marker='o', color='#3D9140', label=r'$Lactococcus \ lactis \ control $')
axs[1, 0].fill_between(xlc, ylc-elc, ylc+elc, color='#3D9140', alpha=0.5)
axs[1, 0].legend(loc='lower right', bbox_to_anchor=(1.1, 0.85), fontsize=14)
axs[1, 0].tick_params(axis='x', labelsize=16)
axs[1, 0].tick_params(axis='y', labelsize=16)
axs[1, 0].set_ylabel('Predicted values for EC(IQR)', fontsize=20)
axs[1, 0].set_xlabel('Days relative to sample', fontsize=20)

axs[1, 1].plot(xe, ye, marker='o', color='#8A3324', label=r'$Enterococcus \ spp. \ cases $')
axs[1, 1].fill_between(xe, ye-ee, ye+ee, color='#8A3324', alpha=0.5)
axs[1, 1].plot(xec, yec, marker='o', color='#BCEE68', label=r'$Enterococcus \ spp. \ control $')
axs[1, 1].fill_between(xec, yec-eec, yec+eec, color='#BCEE68', alpha=0.5)
axs[1, 1].legend(loc='lower right', bbox_to_anchor=(1.1, 0.85), fontsize=14)
axs[1, 1].tick_params(axis='x', labelsize=16)
axs[1, 1].set_xlabel('Days relative to sample', fontsize=20)

plt.show()


# =============================================================================
# Figure with 4 subplots, MY
# =============================================================================

fig, axs = plt.subplots(2,2, figsize=(15, 15), 
                        sharex=True, sharey=True)
sns.set_theme()

axs[0, 0].plot(xs, ys, marker='o', color='#48D1CC', label=r'$Staph. simulans \ cases $')
axs[0, 0].fill_between(xs, ys-es, ys+es, color='#48D1CC', alpha=0.5)
axs[0, 0].plot(xsc, ysc, marker='o', color='#006400', label=r'$Staph. simulans \ controls $')
axs[0, 0].fill_between(xsc, ysc-esc, ysc+esc, color='#006400', alpha=0.5)
axs[0, 0].legend(loc='lower right', bbox_to_anchor=(1.1, 0.85), fontsize=14)
axs[0, 0].tick_params(axis='y', labelsize=16)
axs[0, 0].set_ylabel('Predicted values for MY(kg)', fontsize=20)

axs[0, 1].plot(xl, yl, marker='o', color='#1874CD', label=r'$Lactococcus \ lactis \ cases $')
axs[0, 1].fill_between(xl, yl-el, yl+el, color='#1874CD', alpha=0.5)
axs[0, 1].plot(xlc, ylc, marker='o', color='#3D9140', label=r'$Lactococcus \ lactis \ control $')
axs[0, 1].fill_between(xlc, ylc-elc, ylc+elc, color='#3D9140', alpha=0.5)
axs[0, 1].legend(loc='lower right', bbox_to_anchor=(1.1, 0.85), fontsize=14)

axs[1, 0].plot(xe, ye, marker='o', color='#8A3324', label=r'$Enterococcus \ spp. \ cases $')
axs[1, 0].fill_between(xe, ye-ee, ye+ee, color='#8A3324', alpha=0.5)
axs[1, 0].plot(xec, yec, marker='o', color='#BCEE68', label=r'$Enterococcus \ spp. \ control $')
axs[1, 0].fill_between(xec, yec-eec, yec+eec, color='#BCEE68', alpha=0.5)
axs[1, 0].legend(loc='lower right', bbox_to_anchor=(1.1, 0.85), fontsize=14)
axs[1, 0].tick_params(axis='x', labelsize=16)
axs[1, 0].tick_params(axis='y', labelsize=16)
axs[1, 0].set_ylabel('Predicted values for MY(kg)', fontsize=20)
axs[1, 0].set_xlabel('Days relative to sample', fontsize=20)

axs[1, 1].plot(xep, yep, marker='o', color='#FFC125', label=r'$Staph. epidermidis \ cases $')
axs[1, 1].fill_between(xep, yep-eep, yep+eep, color='#FFC125', alpha=0.5)
axs[1, 1].plot(xepc, yepc, marker='o', color='#76EE00', label=r'$Staph. epidermidis \ controls $')
axs[1, 1].fill_between(xepc, yepc-eepc, yepc+eepc, color='#76EE00', alpha=0.5)

axs[1, 1].legend(loc='lower right', bbox_to_anchor=(1.1, 0.85), fontsize=14)
axs[1, 1].tick_params(axis='x', labelsize=16)
axs[1, 1].set_xlabel('Days relative to sample', fontsize=20)

plt.show()

# =============================================================================
# Figure with 4 subplots, 2
# =============================================================================

fig, axs = plt.subplots(2,2, figsize=(15, 15), 
                        sharex=True, sharey=True)
sns.set_theme()

axs[0, 0].errorbar(xa, ya, yerr=ea, fmt='-o', color='#48D1CC', label=r'$Staph. aureus \ cases $')
axs[0, 0].errorbar(xac, yac, yerr=eac, fmt='-o', color='#006400', label=r'$Staph. aureus \ controls $')
axs[0, 0].legend(loc='lower right', bbox_to_anchor=(1.1, 0.85), fontsize=14)
axs[0, 0].tick_params(axis='y', labelsize=16)
axs[0, 0].set_ylabel('Predicted values for ln(OCC)', fontsize=20)

axs[0, 1].errorbar(xd, yd, yerr=ed, fmt='-o', color='#1874CD', label=r'$Strep. dysgalactiae \ cases $')
axs[0, 1].errorbar(xdc, ydc, yerr=edc, fmt='-o', color='#3D9140', label=r'$Strep. dysgalactiae \ control $')
axs[0, 1].legend(loc='lower right', bbox_to_anchor=(1.1, 0.85), fontsize=14)

axs[1, 0].errorbar(xu, yu, yerr=eu, fmt='-o', color='#8A3324', label=r'$Strep. uberis \ cases $')
axs[1, 0].errorbar(xuc, yuc, yerr=euc, fmt='-o', color='#BCEE68', label=r'$Strep. uberis \ control $')
axs[1, 0].legend(loc='lower right', bbox_to_anchor=(1.1, 0.85), fontsize=14)
axs[1, 0].tick_params(axis='x', labelsize=16)
axs[1, 0].tick_params(axis='y', labelsize=16)
axs[1, 0].set_ylabel('Predicted values for ln(OCC)', fontsize=20)
axs[1, 0].set_xlabel('Days relative to sample', fontsize=20)

axs[1, 1].errorbar(xep, yep, yerr=eep, fmt='-o', color='#FFC125', label=r'$Staph. epidermidis \ cases $')
axs[1, 1].errorbar(xepc, yepc, yerr=eepc, fmt='-o', color='#76EE00', label=r'$Staph. epidermidis \ controls $')
axs[1, 1].legend(loc='lower right', bbox_to_anchor=(1.1, 0.85), fontsize=14)
axs[1, 1].tick_params(axis='x', labelsize=16)
axs[1, 1].set_xlabel('Days relative to sample', fontsize=20)

plt.show()
    