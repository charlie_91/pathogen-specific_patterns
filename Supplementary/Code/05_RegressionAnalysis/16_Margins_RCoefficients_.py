# -*- coding: utf-8 -*-
"""
Created on Thu Apr 27 07:50:03 2023

@author: cholofss
"""

"""
A script containg code for visualizing margins (estimated values) 
when creating separate models for cases and controls.
"""

# =============================================================================
# Importing modules
# =============================================================================

# Data handling and plotting
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
# Statistics
import statsmodels.api as sm
import statsmodels.formula.api as smf


# =============================================================================
# Data
# =============================================================================


# Coefficients from R
# -----------------------------------------------------------------------------
coeffAur = pd.read_excel('../data/Regression/CoefficientsR/StraphAureusCoeff(OCC)(F).xlsx')
coeffDys = pd.read_excel('../data/Regression/CoefficientsR/StrepDysgalactiaeCoeff(OCC)(F).xlsx')
coeffSim = pd.read_excel('../data/Regression/CoefficientsR/StaphSimulansCoeff(OCC)(F).xlsx')
coeffLact = pd.read_excel('../data/Regression/CoefficientsR/LactococcusLactisCoeff(OCC)(F).xlsx')
coeffUber = pd.read_excel('../data/Regression/CoefficientsR/StrepUberisCoeff(OCC)(F).xlsx')
coeffEpi = pd.read_excel('../data/Regression/CoefficientsR/StaphEpidermidisCoeff(OCC)(F).xlsx')
coeffEnter = pd.read_excel('../data/Regression/CoefficientsR/EnterococcusCoeff(OCC)(F).xlsx')


# Lists for looping
# -----------------------------------------------------------------------------
dataPathID = [coeffAur, coeffDys, coeffSim, coeffLact, coeffUber, coeffEpi, coeffEnter]
names = ['Aureus', 'Dysgalactiae', 'Simulans', 'Lactis', 'Uberis', 'Epidermidis', 'Enterococcus']


# =============================================================================
# Preparing data for plotting
# =============================================================================
# Data
tickList = [-15, -14, -13, -12, -11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0]
iL = [x for x in range(4, 18)]
jL = [x for x in range(19, 33)]


# Aureus
# -----------------------------------------------------------------------------
# Coefficients
interA = coeffAur['Estimate'].values[0]
pathA = coeffAur['Estimate'].values[1]
day0A = coeffAur['Estimate'].values[33]
day0AC = coeffAur['Estimate'].values[18]
xA, xAC = [], []
xA.append(interA+pathA)
xAC.append(interA)
for i, j in zip(iL, jL):
    xA.append(interA + pathA + (coeffAur['Estimate'].values[j]))
    xAC.append(interA + (coeffAur['Estimate'].values[i]))
xA.append(interA+pathA+day0A)
xAC.append(interA+day0AC)
xA = pd.DataFrame(xA, index=tickList)
xAC = pd.DataFrame(xAC, index=tickList)

# Standard errors
interAS = coeffAur['Std. Error'].values[0]
pathAS = coeffAur['Std. Error'].values[1]
day0A = coeffAur['Std. Error'].values[33]
day0AC = coeffAur['Std. Error'].values[18]
sA, sAC = [], []
sA.append(interAS+pathAS)
sAC.append(interAS)
for i, j in zip(iL, jL):
    sA.append(interAS + pathAS + (coeffAur['Std. Error'].values[j]))
    sAC.append(interAS +  (coeffAur['Std. Error'].values[i]))
sA.append(interAS+pathAS+day0A)
sAC.append(interAS+day0AC)

sA = pd.DataFrame(sA, index=tickList)
sAC = pd.DataFrame(sAC, index=tickList)
xa, ya, ea = xA.index.values, xA.values.reshape(-1), sA.values.reshape(-1)
xac, yac, eac = xAC.index.values, xAC.values.reshape(-1), sAC.values.reshape(-1)


# Dysgalactiae
# -----------------------------------------------------------------------------
# Coefficients
interD = coeffDys['Estimate'].values[0]
pathD = coeffDys['Estimate'].values[1]
day0D = coeffDys['Estimate'].values[33]
day0DC = coeffDys['Estimate'].values[18]
xD, xDC = [], []
xD.append(interD+pathD)
xDC.append(interD)
for i, j in zip(iL, jL):
    xD.append(interD + pathD + (coeffDys['Estimate'].values[j]))
    xDC.append(interD + (coeffDys['Estimate'].values[i]))
xD.append(interD+pathD+day0D)
xDC.append(interD+day0DC)
xD = pd.DataFrame(xD, index=tickList)
xDC = pd.DataFrame(xDC, index=tickList)

# Standard errors
interDS = coeffDys['Std. Error'].values[0]
pathDS = coeffDys['Std. Error'].values[1]
day0D = coeffDys['Std. Error'].values[33]
day0DC = coeffDys['Std. Error'].values[18]
sD, sDC = [], []
sD.append(interDS+pathDS)
sDC.append(interDS)
for i, j in zip(iL, jL):
    sD.append(interDS + pathDS + coeffDys['Std. Error'].values[j])
    sDC.append(interDS + coeffDys['Std. Error'].values[i])
sD.append(interDS+pathDS+day0D)
sDC.append(interDS+day0DC)
sD = pd.DataFrame(sD, index=tickList)
sDC = pd.DataFrame(sDC, index=tickList)

xd, yd, ed = xD.index.values, xD.values.reshape(-1), sD.values.reshape(-1)
xdc, ydc, edc = xDC.index.values, xDC.values.reshape(-1), sDC.values.reshape(-1)


# Simulans
# -----------------------------------------------------------------------------
# Coefficients
interS = coeffSim['Estimate'].values[0]
pathS = coeffSim['Estimate'].values[1]
day0S = coeffSim['Estimate'].values[33]
day0SC = coeffSim['Estimate'].values[18]
xS, xSC = [], []
xS.append(interS+pathS)
xSC.append(interS)
for i, j in zip(iL, jL):
    xS.append(interS + pathS + (coeffSim['Estimate'].values[j]))
    xSC.append(interS + (coeffSim['Estimate'].values[i]))
xS.append(interS+pathS+day0S)
xSC.append(interS+day0SC)
xS = pd.DataFrame(xS, index=tickList)
xSC = pd.DataFrame(xSC, index=tickList)

# Standard errors
interSS = coeffSim['Std. Error'].values[0]
pathSS = coeffSim['Std. Error'].values[1]
day0S = coeffSim['Std. Error'].values[33]
day0SC = coeffSim['Std. Error'].values[18]
sS, sSC = [], []
sS.append(interSS+pathSS)
sSC.append(interSS)
for i, j in zip(iL, jL):
    sS.append(interSS + pathSS + coeffSim['Std. Error'].values[j])
    sSC.append(interSS + coeffSim['Std. Error'].values[i])
sS.append(interSS+pathSS+day0S)
sSC.append(interSS+day0SC)
sS = pd.DataFrame(sS, index=tickList)
sSC = pd.DataFrame(sSC, index=tickList)

xs, ys, es = xS.index.values, xS.values.reshape(-1), sS.values.reshape(-1)
xsc, ysc, esc  = xSC.index.values, xSC.values.reshape(-1), sSC.values.reshape(-1)


# Lactis
# -----------------------------------------------------------------------------
# Coefficients
interL = coeffLact['Estimate'].values[0]
pathL = coeffLact['Estimate'].values[1]
day0L = coeffLact['Estimate'].values[33]
day0LC = coeffLact['Estimate'].values[18]
xL, xLC = [], []
xL.append(interL+pathL)
xLC.append(interL)
for i, j in zip(iL, jL):
    xL.append(interL + pathL + (coeffLact['Estimate'].values[j]))
    xLC.append(interL + (coeffLact['Estimate'].values[i]))
xL.append(interL+pathL+day0L)
xLC.append(interL+day0LC)
xL = pd.DataFrame(xL, index=tickList)
xLC = pd.DataFrame(xLC, index=tickList)

# Standard errors
interLS = coeffLact['Std. Error'].values[0]
pathLS = coeffLact['Std. Error'].values[1]
day0L = coeffLact['Std. Error'].values[33]
day0LC = coeffLact['Std. Error'].values[18]
sL, sLC = [], []
sL.append(interLS+pathLS)
sLC.append(interLS)
for i, j in zip(iL, jL):
    sL.append(interLS + pathLS +coeffLact['Std. Error'].values[j])
    sLC.append(interLS + coeffLact['Std. Error'].values[i])
sL.append(interLS+pathLS+day0L)
sLC.append(interLS+day0LC)
sL = pd.DataFrame(sL, index=tickList)
sLC = pd.DataFrame(sLC, index=tickList)

xl, yl, el = xL.index.values, xL.values.reshape(-1), sL.values.reshape(-1)
xlc, ylc,elc  = xLC.index.values, xLC.values.reshape(-1), sLC.values.reshape(-1) 


# Uberis
# -----------------------------------------------------------------------------
# Coefficients
interU = coeffUber['Estimate'].values[0]
pathU = coeffUber['Estimate'].values[1]
day0U = coeffUber['Estimate'].values[33]
day0UC = coeffUber['Estimate'].values[18]
xU, xUC = [], []
xU.append(interU+pathU)
xUC.append(interU)
for i, j in zip(iL, jL):
    xU.append(interU + pathU + (coeffUber['Estimate'].values[j]))
    xUC.append(interU + (coeffUber['Estimate'].values[i]))
xU.append(interU+pathU+day0U)
xUC.append(interU+day0UC)
xU = pd.DataFrame(xU, index=tickList)
xUC = pd.DataFrame(xUC, index=tickList)

# Standard errors
interUS = coeffUber['Std. Error'].values[0]
pathUS = coeffUber['Std. Error'].values[1]
day0U = coeffUber['Std. Error'].values[33]
day0UC = coeffUber['Std. Error'].values[18]
sU, sUC = [], []
sU.append(interUS+pathUS)
sUC.append(interUS)
for i, j in zip(iL, jL):
    sU.append(interUS + pathUS + coeffUber['Std. Error'].values[j])
    sUC.append(interUS + coeffUber['Std. Error'].values[i])
sU.append(interUS+pathUS+day0U)
sUC.append(interUS+day0UC)
sU = pd.DataFrame(sU, index=tickList)
sUC = pd.DataFrame(sUC, index=tickList)

xu, yu, eu = xU.index.values, xU.values.reshape(-1), sU.values.reshape(-1)
xuc, yuc, euc   = xUC.index.values, xUC.values.reshape(-1), sUC.values.reshape(-1)


# Enterococcus
# -----------------------------------------------------------------------------
# Coefficients
interE = coeffEnter['Estimate'].values[0]
pathE = coeffEnter['Estimate'].values[1]
day0E = coeffEnter['Estimate'].values[33]
day0EC = coeffEnter['Estimate'].values[18]
xE, xEC = [], []
xE.append(interE+pathE)
xEC.append(interE)
for i, j in zip(iL, jL):
    xE.append(interE + pathE + (coeffEnter['Estimate'].values[j]))
    xEC.append(interE + (coeffEnter['Estimate'].values[i]))
xE.append(interE+pathE+day0E)
xEC.append(interE+day0EC)
xE = pd.DataFrame(xE, index=tickList)
xEC = pd.DataFrame(xEC, index=tickList)

# Standard errors
interES = coeffEnter['Std. Error'].values[0]
pathES = coeffEnter['Std. Error'].values[1]
day0E = coeffEnter['Std. Error'].values[33]
day0ES = coeffEnter['Std. Error'].values[18]
sE, sEC = [], []
sE.append(interES+pathES)
sEC.append(interES)
for i, j in zip(iL, jL):
    sE.append(interES + pathES + coeffEnter['Std. Error'].values[j])
    sEC.append(interES + coeffEnter['Std. Error'].values[i])
sE.append(interES+pathES+day0E)
sEC.append(interES+day0EC)
sE = pd.DataFrame(sE, index=tickList)
sEC = pd.DataFrame(sEC, index=tickList)

xe, ye, ee = xE.index.values, xE.values.reshape(-1), sE.values.reshape(-1)
xec, yec, eec = xEC.index.values, xEC.values.reshape(-1), sEC.values.reshape(-1)


# Epidermidis
# -----------------------------------------------------------------------------
# Coefficients
interEp = coeffEpi['Estimate'].values[0]
pathEp = coeffEpi['Estimate'].values[1]
day0Ep = coeffEpi['Estimate'].values[33]
day0EpC = coeffEpi['Estimate'].values[18]
xEp, xEpC = [], []
xEp.append(interEp+pathEp)
xEpC.append(interEp)
for i, j in zip(iL, jL):
    xEp.append(interEp + pathEp + (coeffEpi['Estimate'].values[j]))
    xEpC.append(interEp + (coeffEpi['Estimate'].values[i]))
xEp.append(interEp+pathEp+day0Ep)
xEpC.append(interEp+day0EpC)
xEp = pd.DataFrame(xEp, index=tickList)
xEpC = pd.DataFrame(xEpC, index=tickList)

# Standard errors
interEpS = coeffEpi['Std. Error'].values[0]
pathEpS = coeffEpi['Std. Error'].values[1]
day0Ep = coeffEpi['Std. Error'].values[33]
day0EpC = coeffEpi['Std. Error'].values[18]
sEp, sEpC = [], []
sEp.append(interEpS+pathEpS)
sEpC.append(interEpS)
for i, j in zip(iL, jL):
    sEp.append(interEpS + pathEpS + coeffEpi['Std. Error'].values[j])
    sEpC.append(interEpS + coeffEpi['Std. Error'].values[i])
sEp.append(interEpS+pathEpS+day0Ep)
sEpC.append(interEpS+day0EpC)
sEp = pd.DataFrame(sEp, index=tickList)
sEpC = pd.DataFrame(sEpC, index=tickList)

xep, yep, eep = xEp.index.values, xEp.values.reshape(-1), sEp.values.reshape(-1)
xepc, yepc, eepc = xEpC.index.values, xEpC.values.reshape(-1), sEpC.values.reshape(-1)


# =============================================================================
# Figure
# =============================================================================

fig, axs = plt.subplots(figsize=(15, 7), tight_layout=True) 
sns.set_theme()
# fig.suptitle('Margins-Plot', fontsize=20)

axs.errorbar(xa, ya, yerr=ea, fmt='-o', color='#48D1CC', label=r'$Staph. aureus \ cases $')
axs.errorbar(xd, yd, yerr=ed, fmt='-o', color='#1874CD', label=r'$Strep. dysgalactiae \ cases $')
# axs.errorbar(xs, ys, yerr=es, fmt='-o', color='#9A32CD', label=r'$Staph. simulans \ cases $')
# axs.errorbar(xl, yl, yerr=el, fmt='-o', color='#FF7D40', label=r'$Lactococcus \ lactis \ cases $')
axs.errorbar(xu, yu, yerr=eu, fmt='-o', color='#8A3324', label=r'$Strep. uberis \ cases $')
axs.errorbar(xe, ye, yerr=ee, fmt='-o', color='#CD4F39', label=r'$Enterococcus \ sp. \ cases $')
axs.errorbar(xep, yep, yerr=eep, fmt='-o', color='#FFC125', label=r'$Staph. epidermidis \ cases $')

axs.errorbar(xac, yac, yerr=eac, fmt='-o', color='#006400', label=r'$Staph. aureus \ controls $')
axs.errorbar(xdc, ydc, yerr=edc, fmt='-o', color='#3D9140', label=r'$Strep. dysgalactiae \ control $')
# axs.errorbar(xsc, ysc, yerr=esc, fmt='-o', color='#66CD00', label=r'$Staph. simulans \ control $')
# axs.errorbar(xlc, ylc, yerr=elc, fmt='-o', color='#228B22', label=r'$Lactococcus \ lactis \ controls $')
axs.errorbar(xuc, yuc, yerr=euc, fmt='-o', color='#BCEE68', label=r'$Strep. uberis \ control $')
axs.errorbar(xec, yec, yerr=eec, fmt='-o', color='#6E8B3D', label=r'$Enterococcus \ sp. \ controls $')
axs.errorbar(xepc, yepc, yerr=eepc, fmt='-o', color='#76EE00', label=r'$Staph. epidermidis \ controls $')


# axs.set_xticklabels(tickList)
axs.tick_params(axis='x', labelsize=16)
axs.tick_params(axis='y', labelsize=16)
axs.set_ylabel('Predicted values for ln(OCC)', fontsize=20)
axs.set_xlabel('Days relative to sample', fontsize=20)
axs.legend(loc='lower right', bbox_to_anchor=(1.3, 0.3), fontsize=14)

plt.show()


# =============================================================================
# Figure with 4 subplots, OCC
# =============================================================================

fig, axs = plt.subplots(2,2, figsize=(18, 13), 
                        sharex=True, sharey=True)
sns.set_theme()

axs[0, 0].plot(xa, ya, marker='o', color='#48D1CC', label=r'$Staph. aureus $')
axs[0, 0].fill_between(xa, ya-ea, ya+ea, color='#48D1CC', alpha=0.5)
axs[0, 0].plot(xac, yac, marker='o', color='#006400', label=r'$ Baseline $')
axs[0, 0].fill_between(xac, yac-eac, yac+eac, color='#006400', alpha=0.5)
axs[0, 0].legend(loc='lower right', bbox_to_anchor=(1.1, 0.85), fontsize=14)
axs[0, 0].tick_params(axis='y', labelsize=16)
axs[0, 0].set_ylabel('Predicted values for ln(OCC)', fontsize=20)
axs[0, 0].set_ylim([2, 7.5])

axs[0, 1].plot(xd, yd, marker='o', color='#1874CD', label=r'$Strep. dysgalactiae  $')
axs[0, 1].fill_between(xd, yd-ed, yd+ed, color='#1874CD', alpha=0.5)
axs[0, 1].plot(xdc, ydc, marker='o', color='#3D9140', label=r'$Baseline $')
axs[0, 1].fill_between(xdc, ydc-edc, ydc+edc, color='#3D9140', alpha=0.5)
axs[0, 1].legend(loc='lower right', bbox_to_anchor=(1.1, 0.85), fontsize=14)

axs[1, 0].plot(xu, yu, marker='o', color='#8A3324', label=r'$Strep. uberis  $')
axs[1, 0].fill_between(xu, yu-eu, yu+eu, color='#8A3324', alpha=0.5)
axs[1, 0].plot(xuc, yuc, marker='o', color='#BCEE68', label=r'$Baseline $')
axs[1, 0].fill_between(xuc, yuc-euc, yuc+euc, color='#BCEE68', alpha=0.5)
axs[1, 0].legend(loc='lower right', bbox_to_anchor=(1.1, 0.85), fontsize=14)
axs[1, 0].tick_params(axis='x', labelsize=16)
axs[1, 0].tick_params(axis='y', labelsize=16)
axs[1, 0].set_ylabel('Predicted values for ln(OCC)', fontsize=20)
axs[1, 0].set_xlabel('Days relative to sample', fontsize=20)

axs[1, 1].plot(xep, yep, marker='o', color='#FFC125', label=r'$Staph. epidermidis  $')
axs[1, 1].fill_between(xep, yep-eep, yep+eep, color='#FFC125', alpha=0.5)
axs[1, 1].plot(xepc, yepc, marker='o', color='#76EE00', label=r'$Baseline $')
axs[1, 1].fill_between(xepc, yepc-eepc, yepc+eepc, color='#76EE00', alpha=0.5)

axs[1, 1].legend(loc='lower right', bbox_to_anchor=(1.1, 0.85), fontsize=14)
axs[1, 1].tick_params(axis='x', labelsize=16)
axs[1, 1].set_xlabel('Days relative to sample', fontsize=20)

plt.show()


# =============================================================================
# Figure with 3 subplots, OCC
# =============================================================================

fig, axs = plt.subplots(3,1, figsize=(15, 15), 
                        sharex=True, sharey=True)
sns.set_theme()

axs[0].plot(xs, ys, marker='o', color='#E066FF', label=r'$Staph. simulans  $')
axs[0].fill_between(xs, ys-es, ys+es, color='#E066FF', alpha=0.5)
axs[0].plot(xsc, ysc, marker='o', color='#66CD00', label=r'$Baseline $')
axs[0].fill_between(xsc, ysc-esc, ysc+esc, color='#66CD00', alpha=0.5)
axs[0].legend(loc='lower right', bbox_to_anchor=(1.1, 0.85), fontsize=14)
axs[0].tick_params(axis='y', labelsize=16)
axs[0].set_ylabel('Predicted values for ln(OCC)', fontsize=20)
axs[0].set_ylim([2, 7.5])

axs[1].plot(xl, yl, marker='o', color='#8968CD', label=r'$Lactococcus \ lactis  $')
axs[1].fill_between(xl, yl-el, yl+el, color='#8968CD', alpha=0.5)
axs[1].plot(xlc, ylc, marker='o', color='#228B22', label=r'$Baseline $')
axs[1].fill_between(xlc, ylc-elc, ylc+elc, color='#228B22', alpha=0.5)
axs[1].legend(loc='lower right', bbox_to_anchor=(1.1, 0.85), fontsize=14)
axs[1].tick_params(axis='y', labelsize=16)
axs[1].set_ylabel('Predicted values for ln(OCC)', fontsize=20)

axs[2].plot(xe, ye, marker='o', color='#000080', label=r'$Enterococcus \ sp.  $')
axs[2].fill_between(xe, ye-ee, ye+ee, color='#000080', alpha=0.5)
axs[2].plot(xec, yec, marker='o', color='#6E8B3D', label=r'$Baseline $')
axs[2].fill_between(xec, yec-eec, yec+eec, color='#6E8B3D', alpha=0.5)
axs[2].legend(loc='lower right', bbox_to_anchor=(1.1, 0.85), fontsize=14)
axs[2].tick_params(axis='x', labelsize=16)
axs[2].tick_params(axis='y', labelsize=16)
axs[2].set_ylabel('Predicted values for ln(OCC)', fontsize=20)
axs[2].set_xlabel('Days relative to sample', fontsize=20)

axs[2].legend(loc='lower right', bbox_to_anchor=(1.1, 0.85), fontsize=14)
axs[2].tick_params(axis='x', labelsize=16)
axs[2].set_xlabel('Days relative to sample', fontsize=20)

plt.show()



# =============================================================================
# Figure with 3 subplots (2), OCC
# =============================================================================

fig, axs = plt.subplots(3,1, figsize=(15, 15), 
                        sharex=True, sharey=True)
sns.set_theme()

axs[0].plot(xa, ya, marker='o', color='#48D1CC', label=r'$Staph. aureus  $')
axs[0].fill_between(xa, ya-ea, ya+ea, color='#48D1CC', alpha=0.5)
axs[0].plot(xac, yac, marker='o', color='#006400', label=r'$Baseline $')
axs[0].fill_between(xac, yac-eac, yac+eac, color='#006400', alpha=0.5)
axs[0].legend(loc='lower right', bbox_to_anchor=(1.1, 0.85), fontsize=14)
axs[0].tick_params(axis='y', labelsize=16)
axs[0].set_ylabel('Predicted values for ln(OCC)', fontsize=20)
axs[0].set_ylim([2, 7.5])

axs[1].plot(xd, yd, marker='o', color='#1874CD', label=r'$Strep. dysgalactiae  $')
axs[1].fill_between(xd, yd-ed, yd+ed, color='#1874CD', alpha=0.5)
axs[1].plot(xdc, ydc, marker='o', color='#3D9140', label=r'$Baseline $')
axs[1].fill_between(xdc, ydc-edc, ydc+edc, color='#3D9140', alpha=0.5)
axs[1].legend(loc='lower right', bbox_to_anchor=(1.1, 0.85), fontsize=14)
axs[1].tick_params(axis='y', labelsize=16)
axs[1].set_ylabel('Predicted values for ln(OCC)', fontsize=20)

axs[2].plot(xep, yep, marker='o', color='#000080', label=r'$Staph. epidermidis  $')
axs[2].fill_between(xep, yep-eep, yep+eep, color='#000080', alpha=0.5)
axs[2].plot(xuc, yuc, marker='o', color='#6E8B3D', label=r'$Baseline $')
axs[2].fill_between(xepc, yepc-eepc, yepc+eepc, color='#6E8B3D', alpha=0.5)
axs[2].legend(loc='lower right', bbox_to_anchor=(1.1, 0.85), fontsize=14)
axs[2].tick_params(axis='x', labelsize=16)
axs[2].tick_params(axis='y', labelsize=16)
axs[2].set_ylabel('Predicted values for ln(OCC)', fontsize=20)
axs[2].set_xlabel('Days relative to sample', fontsize=20)

axs[2].legend(loc='lower right', bbox_to_anchor=(1.1, 0.85), fontsize=14)
axs[2].tick_params(axis='x', labelsize=16)
axs[2].set_xlabel('Days relative to sample', fontsize=20)

plt.show()

















































    