# -*- coding: utf-8 -*-
"""
Created on Tue Jun 21 09:23:53 2022

@author: cholofss
"""

"""
A script containing:
    * Creating patterns relative to time of infection.
    * Separating into groups based on wheter the first sample was
      positive or negative. 
"""

# =============================================================================
# Importing modules
# =============================================================================

# Data handling and plotting
# -----------------------------------------------------------------------------
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from datetime import timedelta, date


# =============================================================================
# Data
# =============================================================================

# New relativeTime ID
# -----------------------------------------------------------------------------
dataStaphID = pd.read_csv('../data/Regression/StaphAureusID.csv')
dataDysID = pd.read_csv('../data/Regression/StrepDysgalactiaeID.csv')
dataSimID = pd.read_csv('../data/Regression/StaphSimulansID.csv')
dataLactID = pd.read_csv('../data/Regression/LactococcusLactisID.csv')
dataUberID = pd.read_csv('../data/Regression/StrepUberisID.csv')
dataEpiID = pd.read_csv('../data/Regression/StaphEpidermidisID.csv')
dataEnterID = pd.read_csv('../data/Regression/EnterococcusID.csv')


# Separating lactations based on first sample
# -----------------------------------------------------------------------------
# Aureus
posA = [53771, 59633, 59903, 60722, 61132, 61431, 61432, 61562, 61742, 61781,
        61981, 62101, 63011]
negA = [58384, 58433, 58453, 61491, 61532, 61741, 62841]

# Dysgalactiae
posD = [56516, 58824, 59204, 59773, 60872, 60942, 61042, 61052, 61981, 62292]
negD = [57444, 59902, 60858, 61131, 61721, 62181, 62531, 62581]

# Simulans
posS = [58543, 59393, 60691, 62751]
negS = [59902]

# Lactis
posL = [59203, 59204]
negL = [59882, 62211]

# Uberis
posU = [51465, 57045, 58343, 59773]
negU = [59633]

# Enterococcus
posE = [57044, 57045, 60722, 62581]
negE = [59204, 61352]

# Epidermidis
posEp = [56516, 58543, 58823, 59123, 59393, 59394, 59633, 59772, 60332, 60651,
         60731, 60931, 61041, 61042, 61432, 61672, 61981, 62241, 62551, 62581,
         62871]
negEp = [56515, 57444, 57564, 58433, 58453, 58623, 59204, 50602, 59892, 59902, 
         60272, 60632, 60821, 61131, 61141, 61211, 61302, 61371, 61441, 61551,
         61562, 61671, 61701, 61721, 62101]


# Lists for looping
# -----------------------------------------------------------------------------
dataList = [dataStaphID, dataDysID, dataSimID, dataLactID, dataUberID, dataEpiID, dataEnterID]
nameList = [r'$Staph. aureus$', r'$Strep. dysgalactiae$', r'$Staph. simulans$',
            r'$Lactococcus \ lactis$', r'$Strep. uberis$', r'$Staph. epidermidis$',
            r'$Enterococcus \ spp.$']
posList = [posA, posD, posS, posL, posU, posE, posEp]
negList = [negA, negD, negS, negL, negU, negE, negEp]


# =============================================================================
# Figures
# =============================================================================

for data, name, pos, neg in zip(dataList, nameList, posList, negList):
    
    # Separating first sample positive or negative
    # -------------------------------------------------------------------------
    dataPos = pd.DataFrame()
    dataNeg = pd.DataFrame()

    for row_ in range(len(data)):
        row = data.loc[row_, :]
        tag = row['tagLact']
        if tag in pos:
            dataPos = dataPos.append(row)
        elif tag in neg:
            dataNeg = dataNeg.append(row)


    # =========================================================================
    # Figure 1: ln(OCC), EC-IQR and MY
    # =========================================================================
    
    # data = dataStaph.copy()
    
    # Separating pathogen and not
    # -------------------------------------------------------------------------
    pathPos = dataPos.copy()
    pathNeg = dataNeg.copy()
    base = data[data['Pathogen'] == 0]
    
    # Grouping by relative time
    # -------------------------------------------------------------------------
    p1 = pathPos.groupby(by=pathPos['RelativeTime']).mean()
    s1 = pathPos.groupby(by=pathPos['RelativeTime']).std()
    p2 = pathNeg.groupby(by=pathNeg['RelativeTime']).mean()
    s2 = pathNeg.groupby(by=pathNeg['RelativeTime']).std()
    baseline = base.groupby(by=base['RelativeTime']).mean()
    stdB = base.groupby(by=base['RelativeTime']).mean()
    
    """
    p1 = pathPos.copy()
    p1 = p1.sort_values('RelativeTime')
    p2 = pathNeg.copy()
    """
    # Data to plot
    # -------------------------------------------------------------------------
    x1 = p1.copy()
    s1 = s1.copy()
    x2 = p2.copy()
    s2 = s2.copy()
    xB = baseline.copy()
    sB = stdB.copy()
        
    
    # =========================================================================
    # Figure 1: ln(OCC), EC-IQR and MY
    # =========================================================================
    
    fig, axs = plt.subplots(3, figsize=(13, 7), tight_layout=True,
                            sharex=True, sharey=False) 
    sns.set_theme()
    fig.suptitle(name, fontsize=14)
    
    
    # OCC
    # -------------------------------------------------------------------------
    x3 = x1['lnocc']
    x4 = x3.ewm(span=7, adjust=False).mean()
    s3 = x1['lnocc'] - 1*s1['lnocc']
    s4 = x1['lnocc'] + 1*s1['lnocc']
    
    x5 = x2['lnocc']
    x6 = x5.ewm(span=7, adjust=False).mean()
    s5 = x2['lnocc'] - 1*s2['lnocc']
    s6 = x2['lnocc'] + 1*s2['lnocc']
    
    x7 = xB['lnocc']
    x8 = x7.ewm(span=7, adjust=False).mean()
    s7 = xB['lnocc'] - 1*sB['lnocc']
    s8 = xB['lnocc'] + 1*sB['lnocc']
    
    axs[0].scatter(x3.index, x3, color='#104E8B', label='1st sample positive', s=18)
    axs[0].plot(x4, color='#48D1CC', linewidth=2.0)
    axs[0].fill_between(x3.index, s3, s4, color='#104E8B', alpha=0.35)
    
    axs[0].scatter(x5.index, x5, color='#1C86EE', label='1st sample negative', s=18)
    axs[0].plot(x6, color='#BDFCC9', linewidth=2.0)
    axs[0].fill_between(x5.index, s5, s6, color='#1C86EE', alpha=0.35)
    
    axs[0].scatter(xB.index, x7, color='#228B22', label='Baseline', s=18)
    axs[0].plot(x8, color='#00C957', linewidth=2.0)
    # axs[0].fill_between(xB.index, s7, s8, color='#228B22', alpha=0.35)
    
    axs[0].set_ylabel('ln(OCC) (1000 c/ml)', fontsize=13, rotation=90)
    axs[0].tick_params(axis='y', labelsize=12)
    axs[0].legend(loc='lower right', bbox_to_anchor=(1.1, 0.7), fontsize=10)
    # axs[0].set_ylim([3.0, 8])
    
    
    # EC
    # -------------------------------------------------------------------------
    x9 = x1['ECIQR']
    x10 = x9.ewm(span=7, adjust=False).mean()
    s9 = x1['ECIQR'] - 1*s1['ECIQR']
    s10 = x1['ECIQR'] + 1*s1['ECIQR']
    
    x11 = x2['ECIQR']
    x12 = x11.ewm(span=7, adjust=False).mean()
    s11 = x2['ECIQR'] - 1*s2['ECIQR']
    s12 = x2['ECIQR'] + 1*s2['ECIQR']
    
    x13 = xB['ECIQR']
    x14 = x13.ewm(span=7, adjust=False).mean()
    s13 = xB['ECIQR'] - 1*sB['ECIQR']
    s14 = xB['ECIQR'] + 1*sB['ECIQR']
    
    axs[1].scatter(x9.index, x9, label='1st sample positive', color='#8B4513', s=18)
    axs[1].plot(x10, color='#CD950C', linewidth=2.0)
    axs[1].fill_between(x9.index, s9, s10, color='#8B4513', alpha=0.35)
    
    axs[1].scatter(x11.index, x11, label='1st sample negative', color='#FF7F24', s=18)
    axs[1].plot(x12, color='#FFB90F', linewidth=2.0)
    axs[1].fill_between(x11.index, s11, s12, color='#FF7F24', alpha=0.35)
    
    axs[1].scatter(x13.index, x13, label='Baseline', color='#228B22', s=18)
    axs[1].plot(x14, color='#00C957', linewidth=2.0)
    # axs[1].fill_between(xB.index, s7, s8, color='#228B22', alpha=0.35)
    
    axs[1].set_ylabel('EC (IQR)', fontsize=13, rotation=90)
    axs[1].tick_params(axis='y', labelsize=12)
    axs[1].legend(loc='lower right', bbox_to_anchor=(1.1, 0.7), fontsize=10)
    # axs[1].set_ylim([0.8, 2.0])
    
    
    # Milk yield
    # -------------------------------------------------------------------------
    x15 = x1['MY']
    x16 = x15.ewm(span=7, adjust=False).mean()
    s15 = x1['MY'] - 1*s1['MY']
    s16 = x1['MY'] + 1*s1['MY']
    
    x17 = x2['MY']
    x18 = x17.ewm(span=7, adjust=False).mean()
    s17 = x2['MY'] - 1*s2['MY']
    s18 = x2['MY'] + 1*s2['MY']
    
    x19 = xB['MY']
    x20 = x19.ewm(span=7, adjust=False).mean()
    s19 = xB['MY'] - 1*sB['MY']
    s20 = xB['MY'] + 1*sB['MY']
    
    axs[2].scatter(x15.index, x15, label='1st sample positive', color='#4B0082', s=18)
    axs[2].plot(x16, color='#6959CD', linewidth=2.0)
    axs[2].fill_between(x15.index, s15, s16, color='#4B0082', alpha=0.35)
    
    axs[2].scatter(x17.index, x17, label='1st sample negative', color='#8470FF', s=18)
    axs[2].plot(x18, color='#836FFF', linewidth=2.0)
    axs[2].fill_between(x17.index, s17, s18, color='#8470FF', alpha=0.35)
    
    axs[2].scatter(xB.index, x19, label='Baseline', color='#228B22', s=18)
    axs[2].plot(x20, color='#00C957', linewidth=2.0)
    # axs[2].fill_between(xB.index, s19, s20, color='#228B22', alpha=0.35)
    
    axs[2].set_ylabel('MY (kg)', fontsize=13, rotation=90)
    axs[2].tick_params(axis='y', labelsize=12)
    axs[2].legend(loc='lower right', bbox_to_anchor=(1.1, 0.7), fontsize=10)
    # axs[2].set_ylim([3.0, 32.0])
    
    
    # x-label
    # -------------------------------------------------------------------------
    axs[2].set_xlabel('Days relative to sample', fontsize=18)
    axs[2].xaxis.set_major_locator(plt.MaxNLocator(10))
    axs[2].tick_params(axis='x', labelsize=12)
    
    plt.show()   
    
    
    
    # =========================================================================
    # Figure 2: OCC
    # =========================================================================
    
    fig, axs = plt.subplots(figsize=(13, 7), tight_layout=True,
                            sharex=True, sharey=False) 
    sns.set_theme()
    fig.suptitle(name, fontsize=14)
    
    
    # OCC
    # -------------------------------------------------------------------------
    x3 = x1['OCC']
    x4 = x3.ewm(span=7, adjust=False).mean()
    s3 = x1['OCC'] - 1*s1['OCC']
    s4 = x1['OCC'] + 1*s1['OCC']
    
    x5 = x2['OCC']
    x6 = x5.ewm(span=7, adjust=False).mean()
    s5 = x2['OCC'] - 1*s2['OCC']
    s6 = x2['OCC'] + 1*s2['OCC']
    
    x7 = xB['OCC']
    x8 = x7.ewm(span=7, adjust=False).mean()
    s7 = xB['OCC'] - 1*sB['OCC']
    s8 = xB['OCC'] + 1*sB['OCC']
    
    axs.scatter(x3.index, x3, label='1st sample positive', color='#4B0082', s=18)
    axs.plot(x4, color='#836FFF', linewidth=2.0)
    axs.fill_between(x3.index, s3, s4, color='#4B0082', alpha=0.35)
    
    axs.scatter(x5.index, x5, label='1st sample negative', color='#7AC5CD', s=18)
    axs.plot(x6, color='#98F5FF', linewidth=2.0)
    axs.fill_between(x5.index, s5, s6, color='#7AC5CD', alpha=0.35)
    
    axs.scatter(xB.index, x7, label='Baseline', color='#CD661D', s=18)
    axs.plot(x8, color='#FFB90F', linewidth=2.0)
    axs.fill_between(xB.index, s7, s8, color='#CD661D', alpha=0.35)
    
    axs.set_ylabel('OCC (1000 c/ml)', fontsize=18, rotation=90)
    axs.tick_params(axis='y', labelsize=14)
    axs.legend(loc='lower right', bbox_to_anchor=(1.15, 0.75), fontsize=18)
    # axs[0].set_ylim([3.0, 8])
    
    # x-label
    # -------------------------------------------------------------------------
    axs.set_xlabel('Days relative to sample', fontsize=18)
    axs.xaxis.set_major_locator(plt.MaxNLocator(10))
    axs.tick_params(axis='x', labelsize=14)
    
    plt.show()   
