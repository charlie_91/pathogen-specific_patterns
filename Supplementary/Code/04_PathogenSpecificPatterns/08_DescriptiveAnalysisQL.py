# -*- coding: utf-8 -*-
"""
Created on Tue Jun 21 09:23:53 2022

@author: cholofss
"""

"""
A script containing the code for descriptive analysis of cows infected with
the different pathogen. The cows are separated into groups with no infection, 
one infection and more than one infection. 
Boxplots are created for the different groups.
The patterns in the traits is visualised for the cows with more than one
infection. 
NB: The name of the pathogen analysed needs to be changed in the script!
"""

# =============================================================================
# Importing modules
# =============================================================================

# Data handling and plotting
# -----------------------------------------------------------------------------
import pandas as pd
import numpy as np
import math
import matplotlib.pyplot as plt
import seaborn as sns

# Functions
# -----------------------------------------------------------------------------
import sys
sys.path.append('../Code/Functions')
# import wavelets as wl
import boxplot as box


# =============================================================================
# Data 
# =============================================================================

# Frame with all the cows
# -----------------------------------------------------------------------------
cowFrame = pd.read_csv('../data/cowFrameTagLact.csv')
cowFrame = cowFrame.dropna(subset=['tagLact'])
cowFrame['tagLact'] = cowFrame['tagLact'].astype(int)
cowFrame = cowFrame[cowFrame['quarter'] != 'COW']


# Dictionaries with frames for each cow individually
# -----------------------------------------------------------------------------
tags = list(cowFrame.tagLact.unique())
cows = {}

for sheet in tags:
    frame = pd.read_csv(f"../data/cowsTagLact/{sheet}.csv")
    # Setting days in milk as index
    frame['dim'] = frame['dim'].astype(int)
    frame = frame.set_index('dim')
    frame = frame.sort_index()
    frame = frame[frame['quarter'] != 'COW']
    # Storing dateframe for each cow
    cows[sheet] = frame
    

# Finding postive culture results
# -----------------------------------------------------------------------------
aur = cowFrame[cowFrame['aurBact'] == 1]                    
dys = cowFrame[cowFrame['dysBact'] == 1]  
uber = cowFrame[cowFrame['uberBact'] == 1]  
sim = cowFrame[cowFrame['simuBact'] == 1]  
lact = cowFrame[cowFrame['lactoBact'] == 1]  
enter = cowFrame[cowFrame['enterBact'] == 1]  
epi = cowFrame[cowFrame['epiBact'] == 1]  

bov = cowFrame[cowFrame['bovBact'] == 1]  
chrom = cowFrame[cowFrame['chromoBact'] == 1]  
hemo = cowFrame[cowFrame['hemoBact'] == 1]  
homi = cowFrame[cowFrame['homiBact'] == 1]  
viri = cowFrame[cowFrame['viriBact'] == 1]  
xylo = cowFrame[cowFrame['xyloBact'] == 1]  


pat1 = pd.concat([aur, dys, uber, sim, lact, enter, epi,
                  bov, chrom, hemo, homi, viri, xylo])
pat1['dim'] = pat1['dim'].astype(int)
pat1 = pat1.set_index('dim')



# Frame with healthy cows
# -----------------------------------------------------------------------------
healthyFrame = pd.read_csv('../data/healthyFrame.csv')
healthyFrame = healthyFrame.dropna(subset=['tagLact'])
healthyFrame['tagLact'] = healthyFrame['tagLact'].astype(int)
healthyFrame = healthyFrame.set_index('dim')


# Dictionary with healthy cows
# -----------------------------------------------------------------------------
healthyTags = list(healthyFrame.tagLact.unique())
healthyCows = {}

for sheet in healthyTags:
    frame = pd.read_csv(f"../data/Baseline/{sheet}.csv")
    # Setting days in milk as index
    frame['dim'] = frame['dim'].astype(int)
    frame = frame.set_index('dim')
    frame = frame.sort_index()
    frame = frame.dropna(subset=['OCC'])
    # Storing dateframe for each cow
    healthyCows[sheet] = frame


# =============================================================================
# Quarter-level patterns
# =============================================================================

# Average pattern for infected lactations
# -----------------------------------------------------------------------------

x = pat1.copy()
x = x.groupby(by=x.index).mean()


# Baseline pattern
x = healthyFrame.groupby(by=healthyFrame.index).mean()


# Figure
# -----------------------------------------------------------------------------
fig, axs = plt.subplots(3, figsize=(13, 7), tight_layout=True,
                        sharex=True, sharey=False)
sns.set_theme() 
# fig.suptitle('Quarter-level patterns for ' + str(label), fontsize=20)

# EC
x1 = x['EC1']
x2 = x['EC2']
x3 = x['EC3']
x4 = x['EC4']

axs[0].plot(x1, color='#8A3324', label='RF', linewidth=2.0)
axs[0].plot(x2, color='#458B74', label='LF', linewidth=2.0)
axs[0].plot(x3, color='#EE7621', label='RH', linewidth=2.0)
axs[0].plot(x4, color='#7AC5CD', label='LH', linewidth=2.0)

axs[0].set_ylabel('EC (mS)', fontsize=16)
axs[0].tick_params(axis='y', labelsize=16)
axs[0].legend(loc='lower right', bbox_to_anchor=(1.05, 0.3))
axs[0].set_ylim([3, 8])

# Yield
x5 = x['MY1']
x6 = x['MY2']
x7 = x['MY3']
x8 = x['MY4']

axs[1].plot(x5, color='#8A3324', label='RF', linewidth=2.0)
axs[1].plot(x6, color='#458B74', label='LF', linewidth=2.0)
axs[1].plot(x7, color='#EE7621', label='RH', linewidth=2.0)
axs[1].plot(x8, color='#7AC5CD', label='LH', linewidth=2.0)

axs[1].set_ylabel('MY (kg)', fontsize=16)
axs[1].tick_params(axis='y', labelsize=16)
axs[1].legend(loc='lower right', bbox_to_anchor=(1.05, 0.3))
axs[1].set_ylim([0.1, 7])

# Flow
x9 = x['FA1']
x10 = x['FA2']
x11 = x['FA3']
x12 = x['FA4']

axs[2].plot(x9, color='#8A3324', label='RF', linewidth=2.0)
axs[2].plot(x10, color='#458B74', label='LF', linewidth=2.0)
axs[2].plot(x11, color='#EE7621', label='RH', linewidth=2.0)
axs[2].plot(x12, color='#7AC5CD', label='LH', linewidth=2.0)

axs[2].set_ylabel('Flow (average)', fontsize=16)
axs[2].tick_params(axis='y', labelsize=15)
axs[2].legend(loc='lower right', bbox_to_anchor=(1.05, 0.3))
axs[2].set_ylim([0.1, 2])

# X-label
axs[2].set_xlabel('Days in milk', fontsize=20)
axs[2].xaxis.set_major_locator(plt.MaxNLocator(12))
axs[2].tick_params(axis='x', labelsize=16, rotation=12)

plt.show()   


                            