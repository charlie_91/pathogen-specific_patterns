# -*- coding: utf-8 -*-
"""
Created on Tue Jun 14 10:00:53 2022

@author: cholofss
"""

"""
A script containing the code for visually analysing the OCC-patterns, 
EC-patterns and of milk yield, as a function of days in milk.
The cows are separated into groups depending on the pathogen causing 
the infection. Groups are created with cows with no PAT-1 infection and 
cows with no infections by any pathogen.
Figures are created for the baseline, for each pathogens and then for each 
individual cow, comparing the cow with both healthy and infected averages. 
"""


# =============================================================================
# Importing modules
# =============================================================================

# Data handling and plotting
# -----------------------------------------------------------------------------
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# Functions
# -----------------------------------------------------------------------------
import sys
sys.path.append('../Code/Functions')
import patterns as pt
import wavelets as wl

# =============================================================================
# Data 
# =============================================================================
# =============================================================================
# All cows
# =============================================================================

# Frame
# -----------------------------------------------------------------------------
cowFrame = pd.read_csv('../Data/CowFrameTagLact.csv')
cowFrame = cowFrame.dropna(subset=['tagLact'])
cowFrame['tagLact'] = cowFrame['tagLact'].astype(int)
cowFrame['dim'] = cowFrame['dim'].astype(int)
cowFrame = cowFrame.set_index('dim')
cowFrame = cowFrame[cowFrame['quarter'] != 'COW']

# Dictionary
# -----------------------------------------------------------------------------
tags = list(cowFrame.tagLact.unique())
cows = {}

for sheet in tags:
    frame = pd.read_csv(f'../data/CowsTagLact/{sheet}.csv')     
    frame['dim'] = frame['dim'].astype(int)
    frame = frame.set_index('dim')
    frame = frame[frame['quarter'] != 'COW']
    cows[sheet] = frame
  

# =============================================================================
# Baseline    
# =============================================================================

# Frame 
# -----------------------------------------------------------------------------
baselineFrame = pd.read_csv('../data/healthyFrame.csv')
baselineFrame = baselineFrame.dropna(subset=['tagLact'])
baselineFrame['tagLact'] = baselineFrame['tagLact'].astype(int)
baselineFrame['dim'] = baselineFrame['dim'].astype(int)
baselineFrame = baselineFrame.set_index('dim')
baselineFrame = baselineFrame[baselineFrame['quarter'] != 'COW']
    
# Dictionary
# -----------------------------------------------------------------------------
tags = list(baselineFrame.tagLact.unique())
cowsB = list(baselineFrame.tag.unique())
baselineCows = {}

for sheet in tags:
    frame = pd.read_csv(f'../data/Baseline/{sheet}.csv')     
    frame['dim'] = frame['dim'].astype(int)
    frame = frame.set_index('dim')
    baselineCows[sheet] = frame
    

# =============================================================================
# Lists for looping
# =============================================================================

aurBact = cowFrame[cowFrame['aurBact'] == 1]
aurCow = list(aurBact.tagLact.unique())
aurLact = pd.DataFrame()
for cow in aurCow:
    cow_ = cows[cow]
    aurLact = aurLact.append(cow_)
    
epiBact = cowFrame[cowFrame['epiBact'] == 1]
epiCow = list(epiBact.tagLact.unique())
epiLact = pd.DataFrame()
for cow in epiCow:
    cow_ = cows[cow]
    epiLact = epiLact.append(cow_)
    
simuBact = cowFrame[cowFrame['simuBact'] == 1]
simuCow = list(simuBact.tagLact.unique())
simuLact = pd.DataFrame()
for cow in simuCow:
    cow_ = cows[cow]
    simuLact = simuLact.append(cow_)
    
dysBact = cowFrame[cowFrame['dysBact'] == 1]
dysCow = list(dysBact.tagLact.unique())
dysLact = pd.DataFrame()
for cow in dysCow:
    cow_ = cows[cow]
    dysLact = dysLact.append(cow_)

uberBact = cowFrame[cowFrame['uberBact'] == 1]
uberCow = list(uberBact.tagLact.unique())
uberLact = pd.DataFrame()
for cow in uberCow:
    cow_ = cows[cow]
    uberLact = uberLact.append(cow_)
    
lactBact = cowFrame[cowFrame['lactoBact'] == 1]
lactCow = list(lactBact.tagLact.unique())
lactLact = pd.DataFrame()
for cow in lactCow:
    cow_ = cows[cow]
    lactLact = lactLact.append(cow_)
    
enterBact = cowFrame[cowFrame['enterBact'] == 1]
enterCow = list(enterBact.tagLact.unique())
enterLact = pd.DataFrame()
for cow in enterCow:
    cow_ = cows[cow]
    enterLact = enterLact.append(cow_)
    
bovBact = cowFrame[cowFrame['bovBact'] == 1]
bovCow = list(bovBact.tagLact.unique())
bovLact = pd.DataFrame()
for cow in bovCow:
    cow_ = cows[cow]
    bovLact = bovLact.append(cow_)
    
chromoBact = cowFrame[cowFrame['chromoBact'] == 1]
chromoCow = list(chromoBact.tagLact.unique())
chromoLact = pd.DataFrame()
for cow in chromoCow:
    cow_ = cows[cow]
    chromoLact = chromoLact.append(cow_)
    
hemoBact = cowFrame[cowFrame['hemoBact'] == 1]
hemoCow = list(hemoBact.tagLact.unique())
hemoLact = pd.DataFrame()
for cow in hemoCow:
    cow_ = cows[cow]
    hemoLact = hemoLact.append(cow_)
    
viriBact = cowFrame[cowFrame['viriBact'] == 1]
viriCow = list(viriBact.tagLact.unique())
viriLact = pd.DataFrame()
for cow in viriCow:
    cow_ = cows[cow]
    viriLact = viriLact.append(cow_)
    
homiBact = cowFrame[cowFrame['homiBact'] == 1]
homiCow = list(homiBact.tagLact.unique())
homiLact = pd.DataFrame()
for cow in homiCow:
    cow_ = cows[cow]
    homiLact = homiLact.append(cow_)
    
xyloBact = cowFrame[cowFrame['xyloBact'] == 1]
xyloCow = list(xyloBact.tagLact.unique())
xyloLact = pd.DataFrame()
for cow in xyloCow:
    cow_ = cows[cow]
    xyloLact = xyloLact.append(cow_)


bact = [aurBact, epiBact, simuBact, dysBact, uberBact, lactBact, enterBact,
        bovBact, chromoBact, hemoBact, viriBact, homiBact, xyloBact]
bactLact = [aurLact, epiLact, simuLact, dysLact, uberLact, lactLact,
            enterLact, bovLact, chromoLact, hemoLact, viriLact, homiLact,
            xyloLact]
bactNames = ['Aureus', 'Epidermidis', 'Simulans', 'Dysgalactiae', 
             'Uberis', 'Lactis', 'Enterococcus', 'Bovis', 'Chromogens',
             'Haemolyticus', 'Viridans', 'Hominis', 'Xylosus']
bactNames = [r'$Staph. aureus$', r'$Staph. epidermidis$', r'$Staph. simulans$',
             r'$Strep. dysgalactiae$', r'$Strep. uberis$', r'$Lactococcus \ lactis$', 
             r'$Enterococcus \ spp.$', r'$Corynebacterium \ bovis$', r'$Staph. chromogens$',
             r'$Staph. haemolyticus$', r'$Aerococcus \ viridans$', 
             r'$Staph. hominis$', r'$Staph. xylosus$']
bactLabel = ['aurBact', 'epiBact', 'simuBact', 'dysBact', 'uberBact', 
             'lactoBact', 'enterBact', 'bovBact', 'chromoBact', 
             'hemoBact', 'viriBact', 'homiBact', 'xyloBact']


# =============================================================================
# Average for the different pathogens
# =============================================================================

traits = ['lnocc', 'ECIQR', 'amount']
labels = ['ln(OCC) (1000 c/ml)', 'EC (IQR)', 'MY (kg)']

for path, name in zip(bactLact, bactNames):
    xb = baselineFrame.groupby(by=baselineFrame.index).mean() 
    x = path.groupby(by=path.index).mean() 
    pt.plot_avg_pattern(path, name, xb, traits, labels)
    
    
# =============================================================================
# Average and std for the different pathogens
# =============================================================================

values = {}

for path, name in zip(bactLact, bactNames):
    xb = baselineFrame.groupby(by=baselineFrame.index).mean() 
    xs = baselineFrame.groupby(by=baselineFrame.index).mean()
    x = path.groupby(by=path.index).mean() 
    val = pt.plot_avg_std_pattern(path, name, xb, xs, traits, labels)
    values[name] = val
    
    
# =============================================================================
# Calculating difference; raw
# =============================================================================

differences = {}

for pathogen in values:
    lists = values[pathogen]
    diffOCC = lists[3].subtract(lists[0])
    diffEC = lists[9].subtract(lists[6])
    diffMY = lists[15].subtract(lists[12])
    
    differences[pathogen] = [diffOCC, diffEC, diffMY]
    
# =============================================================================
# Calculating difference; EWMA 
# =============================================================================

differences = {}

for pathogen in values:
    lists = values[pathogen]
    diffOCC = lists[4].subtract(lists[1])
    diffEC = lists[10].subtract(lists[7])
    diffMY = lists[16].subtract(lists[13])
    
    differences[pathogen] = [diffOCC, diffEC, diffMY]
    
# =============================================================================
# Calculating difference; DWT
# =============================================================================

differences = {}

for pathogen in values:
    lists = values[pathogen]
    diffOCC = lists[5].subtract(lists[2])
    diffEC = lists[11].subtract(lists[8])
    diffMY = lists[17].subtract(lists[14])
    
    differences[pathogen] = [diffOCC, diffEC, diffMY]
    
    
# =============================================================================
# Visualization of differences
# =============================================================================

# Figure
# -----------------------------------------------------------------------------
fig, axs = plt.subplots(3, figsize=(13, 7), tight_layout=True,
                        sharex=True, sharey=False) 
sns.set_theme()
fig.suptitle('Difference between pathogen-specific lactations and baseline', fontsize=18)


# OCC
x1 = differences[r'$Staph. aureus$'][0]
x2 = differences[r'$Strep. dysgalactiae$'][0]
x3 = differences[r'$Staph. simulans$'][0]
x4 = differences[r'$Lactococcus \ lactis$'][0]
x5 = differences[r'$Staph. epidermidis$'][0]
x6 = differences[r'$Enterococcus \ spp.$'][0]
x7 = differences[r'$Strep. uberis$'][0]


axs[0].plot(x1, label=r'$Staph. aureus$', color='#FFD39B', linewidth=1.6)
axs[0].plot(x2, label=r'$Strep. dysgalactiae$', color='#8B4C39', linewidth=1.6)
axs[0].plot(x3, label=r'$Staph. simulans$', color='#EE6A50', linewidth=1.6)
axs[0].plot(x4, label=r'$Lactococcus \ lactis$', color='#CDAF95', linewidth=1.6)
axs[0].plot(x5, label=r'$Staph. epidermidis$', color='#71C671', linewidth=1.6)
axs[0].plot(x6, label=r'$Enterococcus \ spp.$', color='#00868B', linewidth=1.6)
axs[0].plot(x7, label=r'$Strep. uberis$', color='#6495ED', linewidth=1.6)

axs[0].set_ylabel('ln(OCC) (1000 c/ml)', fontsize=13)
axs[0].tick_params(axis='y', labelsize=12)
axs[0].legend(loc='lower right', bbox_to_anchor=(1.15, -0.1), fontsize=9)
# axs[0].set_ylim([3.0, 8])


# EC
x8 = differences[r'$Staph. aureus$'][1]
x9 = differences[r'$Strep. dysgalactiae$'][1]
x10 = differences[r'$Staph. simulans$'][1]
x11 = differences[r'$Lactococcus \ lactis$'][1]
x12 = differences[r'$Staph. epidermidis$'][1]
x13 = differences[r'$Enterococcus \ spp.$'][1]
x14 = differences[r'$Strep. uberis$'][1]


axs[1].plot(x8, label=r'$Staph. aureus$', color='#FFD39B', linewidth=1.6)
axs[1].plot(x9, label=r'$Strep. dysgalactiae$', color='#8B4C39', linewidth=1.6)
axs[1].plot(x10, label=r'$Staph. simulans$', color='#EE6A50', linewidth=1.6)
axs[1].plot(x11, label=r'$Lactococcus \ lactis$', color='#CDAF95', linewidth=1.6)
axs[1].plot(x12, label=r'$Staph. epidermidis$', color='#71C671', linewidth=1.6)
axs[1].plot(x13, label=r'$Enterococcus \ spp.$', color='#00868B', linewidth=1.6)
axs[1].plot(x14, label=r'$Strep. uberis$', color='#6495ED', linewidth=1.6)


axs[1].set_ylabel('EC (IQR)', fontsize=13)
axs[1].tick_params(axis='y', labelsize=12)
axs[1].legend(loc='lower right', bbox_to_anchor=(1.15, -0.1), fontsize=9)
# axs[1].set_ylim([0.8, 2.0])


# Milk yield
x15 = differences[r'$Staph. aureus$'][2]
x16 = differences[r'$Strep. dysgalactiae$'][2]
x17 = differences[r'$Staph. simulans$'][2]
x18 = differences[r'$Lactococcus \ lactis$'][2]
x19 = differences[r'$Staph. epidermidis$'][2]
x20 = differences[r'$Enterococcus \ spp.$'][2]
x21 = differences[r'$Strep. uberis$'][2]


axs[2].plot(x15, label=r'$Staph. aureus$', color='#FFD39B', linewidth=1.6)
axs[2].plot(x16, label=r'$Strep. sysgalactiae$', color='#8B4C39', linewidth=1.6)
axs[2].plot(x17, label=r'$Staph. simulans$', color='#EE6A50', linewidth=1.6)
axs[2].plot(x18, label=r'$Lactococcus \ lactis$', color='#CDAF95', linewidth=1.6)
axs[2].plot(x19, label=r'$Staph. epidermidis$', color='#71C671', linewidth=1.6)
axs[2].plot(x20, label=r'$Enterococcus \ spp.$', color='#00868B', linewidth=1.6)
axs[2].plot(x21, label=r'$Strep. uberis$', color='#6495ED', linewidth=1.6)

axs[2].set_ylabel('MY (kg)', fontsize=13)
axs[2].tick_params(axis='y', labelsize=12)
axs[2].legend(loc='lower right', bbox_to_anchor=(1.15, -0.1), fontsize=9)
# axs[2].set_ylim([3.0, 32.0])


# X-label
axs[2].set_xlabel('Days in milk', fontsize=18)
axs[2].xaxis.set_major_locator(plt.MaxNLocator(12))
axs[2].tick_params(axis='x', labelsize=12, rotation=12)

plt.show()   


# =============================================================================
# Comparing patterns
# =============================================================================

# Testing with one pathogen first

lists = values['$Strep. dysgalactiae$']

diffOCC = lists[4].subtract(lists[1], fill_value=0)
diffEC = lists[10].subtract(lists[7], fill_value=0)
diffMY = lists[16].subtract(lists[13], fill_value=0)


# =============================================================================
# Visulisation
# =============================================================================

# Figure
# -----------------------------------------------------------------------------
fig, axs = plt.subplots(3, figsize=(13, 7), tight_layout=True,
                        sharex=True, sharey=False) 
sns.set_theme()

# OCC
x1 = diffOCC.copy()
axs[0].plot(x1, color='#1874CD', label='Difference', linewidth=2.0)
axs[0].set_ylabel('ln(OCC) (1000 c/ml)', fontsize=13, rotation=70)
axs[0].tick_params(axis='y', labelsize=12)
axs[0].legend()

# EC
x2 = diffEC.copy()
axs[1].plot(x2, color='#FFB90F', label='Difference', linewidth=2.0)
axs[1].set_ylabel('EC (IQR)', fontsize=13, rotation=70)
axs[1].tick_params(axis='y', labelsize=12)
axs[1].legend()

# Milk yield
x3 = diffMY.copy()
axs[2].plot(x3, label='Difference', color='purple', linewidth=2.0)
axs[2].set_ylabel('MY (kg)', fontsize=13, rotation=70)
axs[2].tick_params(axis='y', labelsize=12)
axs[2].legend()

# X-label
axs[2].set_xlabel('Days in milk', fontsize=18)
axs[2].xaxis.set_major_locator(plt.MaxNLocator(12))
axs[2].tick_params(axis='x', labelsize=12, rotation=12)

plt.show()   
