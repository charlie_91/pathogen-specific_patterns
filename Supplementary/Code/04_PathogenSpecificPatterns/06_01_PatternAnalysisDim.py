# -*- coding: utf-8 -*-
"""
Created on Tue Jun 14 10:00:53 2022

@author: cholofss
"""

"""
A script containing the code for visually analysing the OCC-patterns, 
EC-patterns and of milk yield, as a function of days in milk.
The cows are separated into groups depending on the pathogen causing 
the infection. Groups are created with cows with no PAT-1 infection and 
cows with no infections by any pathogen.
Figures are created for the baseline, for each pathogens and then for each 
individual cow, comparing the cow with both healthy and infected averages. 
"""


# =============================================================================
# Importing modules
# =============================================================================

# Data handling and plotting
# -----------------------------------------------------------------------------
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# Functions
# -----------------------------------------------------------------------------
import sys
sys.path.append('../Code/Functions')
import patterns as pt
import wavelets as wl

# =============================================================================
# Data 
# =============================================================================
# =============================================================================
# All cows
# =============================================================================

# Frame
# -----------------------------------------------------------------------------
cowFrame = pd.read_csv('../data/CowFrameTagLact.csv')
cowFrame = cowFrame.dropna(subset=['tagLact'])
cowFrame['tagLact'] = cowFrame['tagLact'].astype(int)
cowFrame['dim'] = cowFrame['dim'].astype(int)
cowFrame = cowFrame.set_index('dim')
cowFrame = cowFrame[cowFrame['quarter'] != 'COW']

# Dictionary
# -----------------------------------------------------------------------------
tags = list(cowFrame.tagLact.unique())
cows = {}

for sheet in tags:
    frame = pd.read_csv(f'../data/CowsTagLact/{sheet}.csv')     
    frame['dim'] = frame['dim'].astype(int)
    frame = frame.set_index('dim')
    frame = frame[frame['quarter'] != 'COW']
    cows[sheet] = frame
  

# =============================================================================
# Baseline    
# =============================================================================

# Frame 
# -----------------------------------------------------------------------------
baselineFrame = pd.read_csv('../data/healthyFrame.csv')
baselineFrame = baselineFrame.dropna(subset=['tagLact'])
baselineFrame['tagLact'] = baselineFrame['tagLact'].astype(int)
baselineFrame['dim'] = baselineFrame['dim'].astype(int)
baselineFrame = baselineFrame.set_index('dim')
baselineFrame = baselineFrame[baselineFrame['quarter'] != 'COW']
    
# Dictionary
# -----------------------------------------------------------------------------
tags = list(baselineFrame.tagLact.unique())
cowsB = list(baselineFrame.tag.unique())
baselineCows = {}

for sheet in tags:
    frame = pd.read_csv(f'../data/Baseline/{sheet}.csv')     
    frame['dim'] = frame['dim'].astype(int)
    frame = frame.set_index('dim')
    baselineCows[sheet] = frame
    
    
# =============================================================================
# Infected measurements
# =============================================================================
    
# Frame
# -----------------------------------------------------------------------------
# When only including measurements with positive sample
# -----------------------------------------------------------------------------

infectionMFrame = pd.read_csv('../data/infectionFrame.csv')
infectionMFrame = infectionMFrame.dropna(subset=['tagLact'])
infectionMFrame['tagLact'] = infectionMFrame['tagLact'].astype(int)
infectionMFrame['dim'] = infectionMFrame['dim'].astype(int)
infectionMFrame = infectionMFrame.set_index('dim')
infectionMFrame = infectionMFrame[infectionMFrame['quarter'] != 'COW']

# Dictionary
# -----------------------------------------------------------------------------
tagsInf = list(infectionMFrame.tagLact.unique())
cowsInf = list(infectionMFrame.tag.unique())
infectionMCows = {}

for sheet in tagsInf:
    frame = pd.read_csv(f'../data/infectedCows/{sheet}.csv')     
    frame['dim'] = frame['dim'].astype(int)
    frame = frame.set_index('dim')
    infectionMCows[sheet] = frame
    
    
# =============================================================================
# Infected lactations
# =============================================================================

infectionLFrame, infectionLCows = pd.DataFrame(), {}

for cow_ in tagsInf:
    cow = cows[cow_]
    infectionLCows[cow_] = cow
    infectionLFrame = infectionLFrame.append(cow)  
    
    
# =============================================================================
# PAT-1
# =============================================================================

# Frame
# -----------------------------------------------------------------------------
pat1Frame = pd.read_csv('../data/pat1Frame.csv')
pat1Frame = pat1Frame.dropna(subset=['tagLact'])
pat1Frame['tagLact'] = pat1Frame['tagLact'].astype(int)
pat1Frame['dim'] = pat1Frame['dim'].astype(int)
pat1Frame = pat1Frame.set_index('dim')
pat1Frame = pat1Frame[pat1Frame['quarter'] != 'COW']

# Dictionary
# -----------------------------------------------------------------------------
tagPat1 = list(pat1Frame.tagLact.unique())
pat1Cows = {}

for sheet in tagPat1:
    frame = pd.read_csv(f'../data/pat1Cows/{sheet}.csv')
    frame['dim'] = frame['dim'].astype(int)
    frame = frame.set_index('dim')
    pat1Cows[sheet] = frame
    

# =============================================================================
# PAT-2
# =============================================================================

# Frame
# -----------------------------------------------------------------------------
pat2Frame = pd.read_csv('../data/pat2Frame.csv')
pat2Frame = pat2Frame.dropna(subset=['tagLact'])
pat2Frame['tagLact'] = pat2Frame['tagLact'].astype(int)
pat2Frame['dim'] = pat2Frame['dim'].astype(int)
pat2Frame = pat2Frame.set_index('dim')
pat2Frame = pat2Frame[pat2Frame['quarter'] != 'COW']

# Dictionary
# -----------------------------------------------------------------------------
tagPat2 = list(pat2Frame.tagLact.unique())
pat2Cows = {}

for sheet in tagPat2:
    frame = pd.read_csv(f'../data/pat2Cows/{sheet}.csv')
    frame['dim'] = frame['dim'].astype(int)
    frame = frame.set_index('dim')
    pat2Cows[sheet] = frame
    

# =============================================================================
# Lists for looping
# =============================================================================

aurBact = cowFrame[cowFrame['aurBact'] == 1]
aurCow = list(aurBact.tagLact.unique())
aurLact = pd.DataFrame()
for cow in aurCow:
    cow_ = cows[cow]
    aurLact = aurLact.append(cow_)
    
epiBact = cowFrame[cowFrame['epiBact'] == 1]
epiCow = list(epiBact.tagLact.unique())
epiLact = pd.DataFrame()
for cow in epiCow:
    cow_ = cows[cow]
    epiLact = epiLact.append(cow_)
    
simuBact = cowFrame[cowFrame['simuBact'] == 1]
simuCow = list(simuBact.tagLact.unique())
simuLact = pd.DataFrame()
for cow in simuCow:
    cow_ = cows[cow]
    simuLact = simuLact.append(cow_)
    
dysBact = cowFrame[cowFrame['dysBact'] == 1]
dysCow = list(dysBact.tagLact.unique())
dysLact = pd.DataFrame()
for cow in dysCow:
    cow_ = cows[cow]
    dysLact = dysLact.append(cow_)

uberBact = cowFrame[cowFrame['uberBact'] == 1]
uberCow = list(uberBact.tagLact.unique())
uberLact = pd.DataFrame()
for cow in uberCow:
    cow_ = cows[cow]
    uberLact = uberLact.append(cow_)
    
lactBact = cowFrame[cowFrame['lactoBact'] == 1]
lactCow = list(lactBact.tagLact.unique())
lactLact = pd.DataFrame()
for cow in lactCow:
    cow_ = cows[cow]
    lactLact = lactLact.append(cow_)
    
enterBact = cowFrame[cowFrame['enterBact'] == 1]
enterCow = list(enterBact.tagLact.unique())
enterLact = pd.DataFrame()
for cow in enterCow:
    cow_ = cows[cow]
    enterLact = enterLact.append(cow_)
    
bovBact = cowFrame[cowFrame['bovBact'] == 1]
bovCow = list(bovBact.tagLact.unique())
bovLact = pd.DataFrame()
for cow in bovCow:
    cow_ = cows[cow]
    bovLact = bovLact.append(cow_)
    
chromoBact = cowFrame[cowFrame['chromoBact'] == 1]
chromoCow = list(chromoBact.tagLact.unique())
chromoLact = pd.DataFrame()
for cow in chromoCow:
    cow_ = cows[cow]
    chromoLact = chromoLact.append(cow_)
    
hemoBact = cowFrame[cowFrame['hemoBact'] == 1]
hemoCow = list(hemoBact.tagLact.unique())
hemoLact = pd.DataFrame()
for cow in hemoCow:
    cow_ = cows[cow]
    hemoLact = hemoLact.append(cow_)
    
viriBact = cowFrame[cowFrame['viriBact'] == 1]
viriCow = list(viriBact.tagLact.unique())
viriLact = pd.DataFrame()
for cow in viriCow:
    cow_ = cows[cow]
    viriLact = viriLact.append(cow_)
    
homiBact = cowFrame[cowFrame['homiBact'] == 1]
homiCow = list(homiBact.tagLact.unique())
homiLact = pd.DataFrame()
for cow in homiCow:
    cow_ = cows[cow]
    homiLact = homiLact.append(cow_)
    
xyloBact = cowFrame[cowFrame['xyloBact'] == 1]
xyloCow = list(xyloBact.tagLact.unique())
xyloLact = pd.DataFrame()
for cow in xyloCow:
    cow_ = cows[cow]
    xyloLact = xyloLact.append(cow_)


bact = [aurBact, epiBact, simuBact, dysBact, uberBact, lactBact, enterBact,
        bovBact, chromoBact, hemoBact, viriBact, homiBact, xyloBact]
bactLact = [aurLact, epiLact, simuLact, dysLact, uberLact, lactLact,
            enterLact, bovLact, chromoLact, hemoLact, viriLact, homiLact,
            xyloLact]
bactNames = ['Aureus', 'Epidermidis', 'Simulans', 'Dysgalactiae', 
             'Uberis', 'Lactis', 'Enterococcus', 'Bovis', 'Chromogens',
             'Haemolyticus', 'Viridans', 'Hominis', 'Xylosus']
bactLabel = ['aurBact', 'epiBact', 'simuBact', 'dysBact', 'uberBact', 
             'lactoBact', 'enterBact', 'bovBact', 'chromoBact', 
             'hemoBact', 'viriBact', 'homiBact', 'xyloBact']


# =============================================================================
# OBS! 
# =============================================================================
# What data to use
# -----------------------------------------------------------------------------
cows = baselineCows.copy()
cowFrame = baselineFrame.copy()


# =============================================================================
# Visulisation
# =============================================================================

# =============================================================================
# Baseline
# =============================================================================
xm = cowFrame.groupby(by=cowFrame.index).mean() 
xs = cowFrame.groupby(by=cowFrame.index).std()

# Figure
# -----------------------------------------------------------------------------
fig, axs = plt.subplots(3, figsize=(13, 7), tight_layout=True,
                        sharex=True, sharey=False) 
sns.set_theme()
# fig.suptitle('Avergae values based on the cows with no infections', fontsize=14)


# OCC
x1 = xm['lnocc']
x2 = xm['lnocc'].ewm(span=7, adjust=False).mean()
x2_ = 2*x2 - x2.ewm(span=7, adjust=False).mean()
xs1 = xm['lnocc'] - 1*xs['lnocc']
xs2 = xm['lnocc'] + 1*xs['lnocc']

# axs[0].plot(x1, color='#1874CD', label='Raw', linewidth=2.0)
axs[0].plot(x2, color='#03A89E', label='Smoothed', linewidth=2.0)
# axs[0].plot(x2_, color='blue', label='Double Smoothed', linewidth=2.0)
axs[0].scatter(xm.index, x1, color='#03A89E', label='OCC')
# axs[0].fill_between(xm.index, xs1, xs2, color='#1874CD', alpha=0.35)
axs[0].set_ylabel('ln(OCC) (1000 c/ml)', fontsize=13, rotation=70)
axs[0].tick_params(axis='y', labelsize=12)
axs[0].legend()
# axs[0].set_ylim([3.0, 8])


# EC
x3 = xm['ECAvg']
x4 = xm['ECAvg'].ewm(span=7, adjust=False).mean()
xs3= xm['ECAvg'] - 1*xs['ECAvg']
xs4 = xm['ECAvg'] + 1*xs['ECAvg']

# axs[1].plot(x3, color='#CD661D', label='Raw', linewidth=2.0)
axs[1].plot(x4, color='#FFB90F', label='Smoothed', linewidth=2.0)
axs[1].scatter(xm.index, x3, label='EC (Avg)', color='#FFB90F')
# axs[1].fill_between(xm.index, xs3, xs4, color='#CD661D', alpha=0.35)
axs[1].set_ylabel('EC (Avg)', fontsize=13, rotation=70)
axs[1].tick_params(axis='y', labelsize=12)
axs[1].legend()
# axs[1].set_ylim([0.8, 2.0])


# Milk yield
x5 = xm['MYIQR']
x6 = xm['MYIQR'].ewm(span=7, adjust=False).mean()
xs5 = xm['MYIQR'] - 1*xs['MYIQR']
xs6 = xm['MYIQR'] + 1*xs['MYIQR']


# axs[2].plot(x5, label='Raw', color='purple', linewidth=2.0)
axs[2].plot(x6, color='pink', label='Smoothed', linewidth=2.0)
axs[2].scatter(xm.index, x5, label='Yield [kg]', color='purple')
# axs[2].fill_between(xm.index, xs5, xs6, color='purple', alpha=0.35)
axs[2].set_ylabel('Yield (IQR)', fontsize=13, rotation=70)
axs[2].tick_params(axis='y', labelsize=12)
axs[2].legend()
# axs[2].set_ylim([3.0, 32.0])


# X-label
axs[2].set_xlabel('Days in milk', fontsize=18)
axs[2].xaxis.set_major_locator(plt.MaxNLocator(12))
axs[2].tick_params(axis='x', labelsize=12, rotation=12)

plt.show()   


# =============================================================================
# Parity groups
# =============================================================================

parity1 = cowFrame[cowFrame['lactation'] == 1]
parity2 = cowFrame[cowFrame['lactation'] == 2]
parity3 = cowFrame[cowFrame['lactation'] >= 3]

xm1 = parity1.groupby(by=parity1.index).mean() 
xs1 = parity1.groupby(by=parity1.index).std()
xm2 = parity2.groupby(by=parity2.index).mean() 
xs2 = parity2.groupby(by=parity2.index).std()
xm3 = parity3.groupby(by=parity3.index).mean() 
xs3 = parity3.groupby(by=parity3.index).std()

# Figure
# -----------------------------------------------------------------------------
fig, axs = plt.subplots(3, figsize=(13, 7), tight_layout=True,
                        sharex=True, sharey=False) 
sns.set_theme()
# fig.suptitle('Baseline', fontsize=14)


# OCC
# -----------------------------------------------------------------------------
# First parity
x1 = xm1['lnocc']
x1S = x1.ewm(span=7, adjust=False).mean()
xw1 = wl.lowpassfilter(x1.values.squeeze(), thresh=0.3, wavelet='coif8')
x1s = xm1['lnocc'] - 1*xs1['lnocc']
x2s = xm1['lnocc'] + 1*xs1['lnocc']

# Second parity
x3 = xm2['lnocc']
x2S = x3.ewm(span=7, adjust=False).mean()
xw3 = wl.lowpassfilter(x3.values.squeeze(), thresh=0.3, wavelet='coif8')
x3s = xm2['lnocc'] - 1*xs2['lnocc']
x4s = xm2['lnocc'] + 1*xs2['lnocc']

# Third or higher parity
x5 = xm3['lnocc']
x3S = x5.ewm(span=7, adjust=False).mean()
xw5 = wl.lowpassfilter(x5.values.squeeze(), thresh=0.3, wavelet='coif8')
x5s = xm3['lnocc'] - 1*xs3['lnocc']
x6s = xm3['lnocc'] + 1*xs3['lnocc']

axs[0].scatter(xm1.index, x1, color='#1874CD', label='1st parity', s=2)
axs[0].plot(x1S, color='#48D1CC', label='Smoothed', linewidth=2.0)
# axs[0].plot(xw1, color='#48D1CC', label='Smoothed', linewidth=2.0)
# axs[0].fill_between(xm1.index, x1s, x2s, color='#1874CD', alpha=0.35)

axs[0].scatter(xm2.index, x3, color='#CD661D', label='2nd parity', s=2)
axs[0].plot(x2S, color='#FFB90F', label='Smoothed', linewidth=2.0)
# axs[0].plot(xw3, color='#FFB90F', label='Smoothed', linewidth=2.0)
# axs[0].fill_between(xm2.index, x3s, x4s, color='#CD661D', alpha=0.35)

axs[0].scatter(xm3.index, x5, color='#4B0082', label='3rd+ parity', s=2)
axs[0].plot(x3S, color='#836FFF', label='Smoothed', linewidth=2.0)
# axs[0].plot(xw5, color='#836FFF', label='Smoothed', linewidth=2.0)
# axs[0].fill_between(xm3.index, x5s, x6s, color='#4B0082', alpha=0.35)


axs[0].set_ylabel('ln(OCC) (1000 c/ml)', fontsize=13, rotation=90)
axs[0].tick_params(axis='y', labelsize=12)
axs[0].legend(loc='lower right', bbox_to_anchor=(1.1, 0.3), fontsize=10)
# axs[0].set_ylim([3.0, 8])


# EC
# -----------------------------------------------------------------------------
# First parity
x7 = xm1['ECIQR']
x4S = x7.ewm(span=7, adjust=False).mean()
xw7 = wl.lowpassfilter(x7.values.squeeze(), thresh=0.3, wavelet='coif8')
x7s = xm1['ECIQR'] - 1*xs1['ECIQR']
x8s = xm1['ECIQR'] + 1*xs1['ECIQR']

# Second parity
x9 = xm2['ECIQR']
x5S = x9.ewm(span=7, adjust=False).mean()
xw9 = wl.lowpassfilter(x9.values.squeeze(), thresh=0.3, wavelet='coif8')
x9s = xm2['ECIQR'] - 1*xs2['ECIQR']
x10s = xm2['ECIQR'] + 1*xs2['ECIQR']

# Third or higher parity
x11 = xm3['ECIQR']
x6S = x11.ewm(span=7, adjust=False).mean()
xw11 = wl.lowpassfilter(x11.values.squeeze(), thresh=0.3, wavelet='coif8')
x11s = xm3['ECIQR'] - 1*xs3['ECIQR']
x12s = xm3['ECIQR'] + 1*xs3['ECIQR']

axs[1].scatter(xm1.index, x7, label='1st parity', color='#1874CD', s=2)
axs[1].plot(x4S, color='#48D1CC', label='Smoothed', linewidth=2.0)
# axs[1].plot(xw7, color='#48D1CC', label='Smoothed', linewidth=2.0)
# axs[1].fill_between(xm1.index, x7s, x8s, color='#1874CD', alpha=0.35)

axs[1].scatter(xm2.index, x9, label='2nd parity', color='#CD661D', s=2)
axs[1].plot(x5S, color='#FFB90F', label='Smoothed', linewidth=2.0)
# axs[1].plot(xw9, color='#FFB90F', label='Smoothed', linewidth=2.0)
# axs[1].fill_between(xm2.index, x9s, x10s, color='#CD661D', alpha=0.35)

axs[1].scatter(xm3.index, x11, label='3rd+ parity', color='#4B0082', s=2)
axs[1].plot(x6S, color='#836FFF', label='Smoothed', linewidth=2.0)
# axs[1].plot(xw11, color='#836FFF', label='Smoothed', linewidth=2.0)
# axs[1].fill_between(xm3.index, x11s, x12s, color='#4B0082', alpha=0.35)

axs[1].set_ylabel('EC (IQR)', fontsize=13, rotation=90)
axs[1].tick_params(axis='y', labelsize=12)
axs[1].legend(loc='lower right', bbox_to_anchor=(1.1, 0.3), fontsize=10)
# axs[1].set_ylim([0.8, 2.0])


# Milk yield
# -----------------------------------------------------------------------------
# First parity
x13 = xm1['amount']
x7S = x13.ewm(span=7, adjust=False).mean()
xw13 = wl.lowpassfilter(x13.values.squeeze(), thresh=0.3, wavelet='coif8')
x13s = xm1['amount'] - 1*xs1['amount']
x14s = xm1['amount'] + 1*xs1['amount']

# Second parity
x15 = xm2['amount']
x8S = x15.ewm(span=7, adjust=False).mean()
xw15 = wl.lowpassfilter(x15.values.squeeze(), thresh=0.3, wavelet='coif8')
x15s = xm2['amount'] - 1*xs2['amount']
x16s = xm2['amount'] + 1*xs2['amount']

# Third or higher parity
x17 = xm3['amount']
x9S = x17.ewm(span=7, adjust=False).mean()
xw17 = wl.lowpassfilter(x17.values.squeeze(), thresh=0.3, wavelet='coif8')
x17s = xm3['amount'] - 1*xs3['amount']
x18s = xm3['amount'] + 1*xs3['amount']

axs[2].scatter(xm1.index, x13, label='1st parity', color='#1874CD', s=2)
axs[2].plot(x7S, color='#48D1CC', label='Smoothed', linewidth=2.0)
# axs[2].plot(xw13, color='#48D1CC', label='Smoothed', linewidth=2.0)
# axs[2].fill_between(xm1.index, x13s, x14s, color='#1874CD', alpha=0.35)

axs[2].scatter(xm2.index, x15, label='2nd parity', color='#CD661D', s=2)
axs[2].plot(x8S, color='#FFB90F', label='Smoothed', linewidth=2.0)
# axs[2].plot(xw15, color='#FFB90F', label='Smoothed', linewidth=2.0)
# axs[2].fill_between(xm2.index, x15s, x16s, color='#CD661D', alpha=0.35)

axs[2].scatter(xm3.index, x17, label='3rd parity', color='#4B0082', s=2)
axs[2].plot(x9S, color='#836FFF', label='Smoothed', linewidth=2.0)
# axs[2].plot(xw17, color='#836FFF', label='Smoothed', linewidth=2.0)
# axs[2].fill_between(xm3.index, x17s, x18s, color='#1874CD', alpha=0.35)

axs[2].set_ylabel('MY (kg)', fontsize=13, rotation=90)
axs[2].tick_params(axis='y', labelsize=12)
axs[2].legend(loc='lower right', bbox_to_anchor=(1.1, 0.3), fontsize=10)
# axs[2].set_ylim([3.0, 32.0])


# x-label
# -----------------------------------------------------------------------------
axs[2].set_xlabel('Days in milk', fontsize=18)
axs[2].xaxis.set_major_locator(plt.MaxNLocator(12))
axs[2].tick_params(axis='x', labelsize=12, rotation=12)

plt.show()   


# =============================================================================
# Average for the different pathogens
# =============================================================================

traits = ['lnocc', 'ECVar', 'amount']
labels = ['ln(OCC) (1000 c/ml)', 'EC (VAR)', 'MY (kg)']

for path, name in zip(bactLact, bactNames):
    xb = baselineFrame.groupby(by=baselineFrame.index).mean() 
    x = path.groupby(by=path.index).mean() 
    pt.plot_avg_pattern(path, name, xb, traits, labels)
    
    
# =============================================================================
# Average and std for the different pathogens
# =============================================================================

values = {}

for path, name in zip(bactLact, bactNames):
    xb = baselineFrame.groupby(by=baselineFrame.index).mean() 
    xs = baselineFrame.groupby(by=baselineFrame.index).std()
    x = path.groupby(by=path.index).mean() 
    val = pt.plot_avg_std_pattern(path, name, xb, xs, traits, labels)
    values[name] = val
    

# =============================================================================
# Individual cows
# =============================================================================

for path, name, nameLabel in zip(bact, bactNames, bactLabel):
    
    cowList = list(path.tagLact.unique())
    xp = path.groupby(by=path.index).mean()
    xb = baselineFrame.groupby(by=baselineFrame.index).mean()
    
    for cow_ in cowList:
        tag = cow_
        xc = cows[cow_]
        pt.plot_ind_pattern(xc, xp, xb, tag, path, name, nameLabel, save=True)
        
        
# =============================================================================
# Individual cows - quarter-level
# =============================================================================

for cow in cows:
    xc = cows[cow]
    tag = cow
    pt.plot_ind_ql_pattern(xc, tag, save=True)
    
    
# =============================================================================
# Individual cows - quarter-level, pathogens
# =============================================================================
    
for path, name in zip(bact, bactNames):
    cowList = list(path.tagLact.unique())
    for cow in cowList:
        xc = cows[cow]
        tag = cow
        pt.plot_ind_ql_pattern(xc, tag, name, save=True)