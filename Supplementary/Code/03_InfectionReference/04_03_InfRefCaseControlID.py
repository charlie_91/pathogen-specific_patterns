# -*- coding: utf-8 -*-
"""
Created on Tue Jun 21 09:23:53 2022

@author: cholofss
"""

"""
A script containing:
    * Code for finding the duration of infection. 
    * Mapping to relative time and relative measurement.
    * Finding three controls for each case based on DIM.
"""

# =============================================================================
# Importing modules
# =============================================================================

# Data handling and plotting
# -----------------------------------------------------------------------------
import pandas as pd
import numpy as np
import random as rd
from datetime import timedelta, date
from itertools import groupby


# =============================================================================
# Data 
# =============================================================================

# Frame with all the cows
# -----------------------------------------------------------------------------
cowFrame = pd.read_csv('../data/cowFrameTagLact.csv')
cowFrame = cowFrame.dropna(subset=['tagLact'])
cowFrame['tagLact'] = cowFrame['tagLact'].astype(int)

# Only QMS
# -----------------------------------------------------------------------------
cowFrame = cowFrame[cowFrame['quarter'] != 'COW']
lact = list(cowFrame.taglact.unique())
des = cowFrame.describe()

# Dictionaries with frames for each cow individually
# -----------------------------------------------------------------------------
tags = list(cowFrame.tagLact.unique())
cows = {}

for sheet in tags:
    frame = pd.read_csv(f"../data/cowsTagLact/{sheet}.csv")
    frame['dim'] = frame['dim'].astype(int)
    frame = frame[frame['quarter'] != 'COW']
    cows[sheet] = frame
    
    
# =============================================================================
# Baseline    
# =============================================================================

# Frame 
# -----------------------------------------------------------------------------
baseline = pd.read_csv('../data/healthyFrame.csv')
baseline['tagLact'] = baseline['tagLact'].astype(int)
baseline = baseline[baseline['quarter'] != 'COW']
tagsB = list(baseline.tagLact.unique())


# =============================================================================
# Finding time relative to infection
# =============================================================================

# Pathogens
# -----------------------------------------------------------------------------
aur = cowFrame[cowFrame['aurBact'] == 1]
aurCow = list(aur.tagLact.unique())
dys = cowFrame[cowFrame['dysBact'] == 1]
dysCow = list(dys.tagLact.unique())
sim = cowFrame[cowFrame['simuBact'] == 1]
simCow = list(sim.tagLact.unique())
lact = cowFrame[cowFrame['lactoBact'] == 1]
lactCow = list(lact.tagLact.unique())
uber = cowFrame[cowFrame['uberBact'] == 1]
uberCow = list(uber.tagLact.unique())
epi = cowFrame[cowFrame['epiBact'] == 1]
epiCow = list(epi.tagLact.unique())
enter = cowFrame[cowFrame['enterBact'] == 1]
enterCow = list(enter.tagLact.unique())

# Combining pathogens
# -----------------------------------------------------------------------------
pat1 = pd.concat([aur, dys, sim, lact, uber, epi, enter])
pat1Cows = aurCow + dysCow + simCow + lactCow + uberCow + epiCow + enterCow
pat1Cows = list(set(pat1Cows))


# Baseline
# -----------------------------------------------------------------------------
baselineCows = {}
baseline = pd.DataFrame()
for cow_ in tagsB:
    if cow_ in cows.keys():
        cow = cows[cow_]
        baselineCows[cow_] = cow
        baseline = baseline.append(cow)
        

# =============================================================================
# Functions
# =============================================================================

# Function for creating period of consecutive days
# -----------------------------------------------------------------------------

def create_period(start, stop, relativeTime, relativeTimePos, j):

    period = []
    tempDate = start
    period.append(tempDate.strftime('%Y-%m-%d'))
    relativeTime[tempDate.strftime('%Y-%m-%d')] = -15
    relativeTime[stop.strftime('%Y-%m-%d')] = 0
    
    relativeTimePos[tempDate.strftime('%Y-%m-%d')] = 15
    relativeTimePos[stop.strftime('%Y-%m-%d')] = 0
    
    i = -14
    j = 14
    while tempDate < stop:
        tempDate += timedelta(days=1)
        dateStr = tempDate.strftime('%Y-%m-%d')
        relativeTime[dateStr] = i
        relativeTimePos[dateStr] = j
        period.append(dateStr)
        i += 1
        j -= 1
    
    return period, relativeTime, relativeTimePos
  
        
# =============================================================================
# Pathogen-specific analysis
# =============================================================================

# OBS
# Change name for pathogen
# *****************************************************************************
path = aur.copy()
pathCow = aurCow
label = 'aurBact'
# *****************************************************************************

# label = ['aurBact', 'dysBact', 'lactoBact', 'simuBact', 'uberBact']


# Creating dictionary and frame with cows with positive samples for 
# given pathogen
pathDict = {}
pathFrame = pd.DataFrame()
for cow_ in pathCow:
    cow = cows[cow_]
    pathDict[cow_] = cow
    pathFrame = pathFrame.append(cow)

# Lists for storing time and DIM when infection "starts"
infectionTime = {}
infectionDIM = {}


for cow_ in pathCow:
    # For each cow
    # -------------------------------------------------------------------------
    cow = pathDict[cow_]
    idx = list(cow.index)
    infectionInfo = []
    infectionIdx = []
    infectionStart = []
    infectionDim = []
    relativeTime = {}
    relativeTimePos = {}
    
    
    # Finding dates with postive sample
    # -------------------------------------------------------------------------
    for i in range(len(idx)):
        row = cow.loc[idx[i], :]
        # if row[label].any() == 1:
        if row[label] == 1:
            infectionInfo.append(row['datetag'])
            infectionIdx.append(idx[i])
            infectionDim.append(row['dim'])
        info = list(set(infectionInfo))
        info = sorted(info)
        infoM = list(set(infectionIdx))
    infectionStart.append(info)
        
    
    # Adding days before and after positive sample
    # -------------------------------------------------------------------------
    periods = []
    j = 1
    for i in range(len(info)):
        stop = info[i]
        stopM = infoM[i]
        start = pd.to_datetime(stop) - timedelta(days=15)
        stop = pd.to_datetime(stop) 
        period = create_period(start, stop, relativeTime, relativeTimePos, j)
        periods.append(period[0])
        j = j+1
        
        
    # Creating one list with dates 
    # -------------------------------------------------------------------------
    if len(periods) == 1:
        period = periods[0]
    if len(periods) == 2:
        period = periods[0] + periods[1]
    elif len(periods) == 3:
        period = periods[0] + periods[1] + periods[2]    
    elif len(periods) == 4: 
        period = periods[0] + periods[1] + periods[2] + periods[3]   
    elif len(periods) == 5:
        period = periods[0] + periods[1] + periods[2] + periods[3] + periods[4]
    elif len(periods) == 6:
        period = periods[0] + periods[1] + periods[2] + periods[3] + periods[4] + periods[5]
    elif len(periods) == 7:
        period = periods[0] + periods[1] + periods[2] + periods[3] + periods[4] + periods[5] + periods[6]
    elif len(periods) > 7:
        print('More than 7 periods')
        
            
    # Variable for period or not
    # -------------------------------------------------------------------------
    infectionPeriod = []
    relativeIdx = []
    
    for i in range(len(idx)):
        row = cow.loc[idx[i], :]
        date_ = row['datetag']
        if date_ in period:
            infectionPeriod.append(1)
            relativeIdx.append(idx[i])
        elif date_ not in period:
            infectionPeriod.append(0)
            relativeIdx.append(np.nan)
        else:
            print('What?')
            
            
    # Mapping to relative time and unique relative time
    # -------------------------------------------------------------------------
    time = cow['datetag']
    mapping = time.map(relativeTime)
    cow['RelativeTime'] = mapping
    mappingPos = time.map(relativeTimePos)
    cow['RelativeTimePos'] = mappingPos
    
    
    
    # Adding to dictionaries and frames
    # -------------------------------------------------------------------------        
    infectionTime[cow_] = infectionStart
    infectionDIM[cow_] = list(set(infectionDim))
    cow['Period'] = infectionPeriod
    cow['RelativeIdx'] = relativeIdx
    cow['Pathogen'] = [1 for x in range(len(cow))]
    
    
    # Dropping duplicates
    # -------------------------------------------------------------------------
    cow = cow.drop_duplicates(subset=['tagtag', 'datetag', 'dim', 
                                      'OCC', 'ECIQR'])
    
    pathDict[cow_] = cow
        
    
# =============================================================================
# Mapping to relative measure
# =============================================================================

for cow_ in pathCow:
    cow = pathDict[cow_]
    ind = cow['RelativeIdx']
    ind = ind.values
    ind = np.array(ind, dtype=float)


    x = [ind[s] for s in np.ma.clump_unmasked(np.ma.masked_invalid(ind))]
    result = [list(v) for k,v in groupby(ind, np.isfinite) if k]


    relativeMeas = {}
    for l in x:
        flip = np.flip(l)
        n = 0
        for i in range(len(l)):
            relativeMeas[flip[i]] = n
            n -= 1
            
            
    measure = cow['RelativeIdx']
    mappingM = measure.map(relativeMeas)
    cow['RelativeMeasure'] = mappingM   
    
    
# =============================================================================
# Separating into periods
# =============================================================================

# Mapping relative time to 3 periods
rtDict = {-15:3, -14:3, -13:3, -12:3, -11:3, -10:3, -9:2, -8:2, -7:2, -6:2,
          -5:1, -4:1, -3:1, -2:1, -1:1, 0:0}
rtIDDict = {'-15.6':'3.6', '-14.6':'3.6', '-13.6':'3.6', '-12.6':'3.6', 
            '-11.6':'3.6', '-10.6':'3.6', '-9.6':'2.6', '-8.6':'2.6',
            '-7.6':'2.6', '-6.6':'2.6', '-5.6':'1.6', '-4.6':'1.6',
            '-3.6':'1.6', '-2.6':'1.6', '-1.6':'1.6', '0.6':'0.6',
                '-15.5':'3.5', '-14.5':'3.5', '-13.5':'3.5', '-12.5':'3.5', 
                '-11.5':'3.5', '-10.5':'3.5', '-9.5':'2.5', '-8.5':'2.5',
                '-7.5':'2.5', '-6.5':'2.5', '-5.5':'1.5', '-4.5':'1.5',
                '-3.5':'1.5', '-2.5':'1.5', '-1.5':'1.5', '0.5':'0.5',
                    '-15.4':'3.4', '-14.4':'3.4', '-13.4':'3.6', '-12.4':'3.4', 
                    '-11.4':'3.4', '-10.4':'3.4', '-9.4':'2.4', '-8.4':'2.4',
                    '-7.4':'2.4', '-6.4':'2.4', '-5.4':'1.4', '-4.4':'1.4',
                    '-3.4':'1.4', '-2.4':'1.4', '-1.4':'1.4', '0.4':'0.4',
                        '-15.3':'3.3', '-14.3':'3.3', '-13.3':'3.3', '-12.3':'3.3', 
                        '-11.3':'3.3', '-10.3':'3.3', '-9.3':'2.3', '-8.3':'2.3',
                        '-7.3':'2.3', '-6.3':'2.3', '-5.3':'1.3', '-4.3':'1.3',
                        '-3.3':'1.3', '-2.3':'1.3', '-1.3':'1.3', '0.3':'0.3',
                            '-15.2':'3.2', '-14.2':'3.2', '-13.2':'3.2', '-12.2':'3.2', 
                            '-11.2':'3.2', '-10.2':'3.2', '-9.2':'2.2', '-8.2':'2.2',
                            '-7.2':'2.2', '-6.2':'2.2', '-5.2':'1.2', '-4.2':'1.2',
                            '-3.2':'1.2', '-2.2':'1.2', '-1.2':'1.2', '0.2':'0.2',
                                '-15.1':'3.1', '-14.1':'3.1', '-13.1':'3.1', '-12.1':'3.1', 
                                '-11.1':'3.1', '-10.1':'3.1', '-9.1':'2.1', '-8.1':'2.1',
                                '-7.1':'2.1', '-6.1':'2.1', '-5.1':'1.1', '-4.1':'1.1',
                                '-3.1':'1.1', '-2.1':'1.1', '-1.1':'1.1', '0.1':'0.1'}


for cow_ in pathCow:
    cow = pathDict[cow_]
    rt = cow['RelativeTime']
    rtID = cow['RelativeTimePos']

    mappingRT = rt.map(rtDict)
    cow['RelativePeriod'] = mappingRT
    
    mappingRTPos = rtID.map(rtIDDict)
    cow['RelativePeriodPos'] = mappingRTPos

    
# =============================================================================
# Creating and exporting files
# =============================================================================

# Correcting for missing parity before updating
# -----------------------------------------------------------------------------
tag1 = [53771, 62181]
tag2 = [59902, 59882]
tag4 = [58384]

for cow_ in pathCow:
    if cow_ in tag1:
        cow = pathDict[cow_]
        cow['ParityGroup'] = [1 for x in range(len(cow))]
        cow['ParityNumber'] = [1 for x in range(len(cow))]
    elif cow_ in tag2:
        cow = pathDict[cow_]
        cow['ParityGroup'] = [2 for x in range(len(cow))]
        cow['ParityNumber'] = [2 for x in range(len(cow))]
    elif cow_ in tag4:
        cow = pathDict[cow_]
        cow['ParityGroup'] = [3 for x in range(len(cow))]
        cow['ParityNumber'] = [4 for x in range(len(cow))]
    else:
        cow = pathDict[cow_]
        cow['ParityGroup'] = cow['Parity']
        cow['ParityNumber'] = cow['lactation']
        

# Updated frame
# -----------------------------------------------------------------------------
pathFrameUp = pd.DataFrame()

for cow_ in pathCow:
    cow = pathDict[cow_]
    pathFrameUp = pathFrameUp.append(cow)
    
    
# Adding unique case ID
# -----------------------------------------------------------------------------
test = pathFrameUp[pathFrameUp['Period'] == 1]
test = test.reset_index()
caseID = []
idx = list(test.index)
c = 1

for i in range(1, len(test)-1):
    prev = test.loc[idx[i-1], :]
    row = test.loc[idx[i], :]

    cow1 = prev['tagLact']
    cow2 = row['tagLact']
    
    per1 = prev['RelativePeriod']
    per2 = row['RelativePeriod']
    
    if cow1 == cow2:
        if (per2-per1) < 2:
            caseID.append(c)
        else:
            caseID.append(c)
            c = c+1
    else:
        caseID.append(c)
        c = c+1

# Adding for the last rows
mis = len(test) - len(caseID)
for i in range(mis):
    caseID.append(c)
    
test['CaseID'] = caseID


"""
pathFrameUp.to_excel('StaphAureus.xlsx')
pathFrameUp.to_csv('StaphAureus.csv')
"""

# =============================================================================
# Baseline: 3 controls for each case
# =============================================================================

controlCows = {}

# For each case
# -----------------------------------------------------------------------------
for case_ in pathCow:
    case = pathDict[case_]
    dimList = infectionDIM[case_]
    
    # For each infection
    # -------------------------------------------------------------------------
    for i in range(len(dimList)):
        mid = dimList[i]
        controls = {}
        
        # Finding controls
        # ---------------------------------------------------------------------
        for control_ in tagsB:
            if control_ in baselineCows.keys():
                control = baselineCows[control_]
                idx = list(control.index)
                dates = []
                # Finding date for relevant DIM
                for i in range(len(idx)):
                    row = control.loc[idx[i], :]
                    dim = row['dim']
                    date_ = row['datetag']
                    if dim == mid:
                        dates.append(date_)
                controls[control_] = list(set(dates))
                
        # Randomly selecting three controls
        # ---------------------------------------------------------------------
        pool = []
        for cont in controls.keys():
            date2 = controls[cont]
            if len(date2) > 0:
                pool.append(cont)
        controlList = rd.sample(pool, 3)

        # For each control
        # ---------------------------------------------------------------------
        for cont in controlList:
            cow = baselineCows[cont]
            date_ = controls[cont][0]
            idx = list(cow.index)
            
            # Creating period
            # -----------------------------------------------------------------
            relativeTimeB = {}
            relativeTimeBPos = {}
            relativeTimeB[date_] = 0
            start = pd.to_datetime(date_) - timedelta(days=14)
            stop = pd.to_datetime(date_)
            period = create_period(start, stop, relativeTimeB, relativeTimeBPos, j=1)
            periodList = period[0]
        
            # Variable for period or not
            # -----------------------------------------------------------------
            infectionPeriod = []
            relativeIdx = []
            for i in range(len(idx)):
                row = cow.loc[idx[i], :]
                date_ = row['datetag']
                if date_ in periodList:
                    infectionPeriod.append(1)
                    relativeIdx.append(idx[i])
                elif date_ not in periodList:
                    infectionPeriod.append(0)
                    relativeIdx.append(np.nan)
            
            # Mapping to relative time
            # -----------------------------------------------------------------
            time = cow['datetag']
            mapping = time.map(relativeTimeB)
            mappingPos = time.map(relativeTimeBPos)
            
            # Adding to frame
            cow['Period'] = infectionPeriod
            cow['RelativeTime'] = mapping
            cow['RelativeTimePos'] = mappingPos
            cow['RelativeIdx'] = relativeIdx
            cow['Pathogen'] = [0 for x in range(len(cow))]
            cow['ParityGroup'] = cow['Parity']
            cow['ParityNumber'] = cow['lactation']
            
            tag = str(case_) + ('_') + str(cont)
            
            controlCows[tag] = cow
    

# Mapping to relative measure
# -----------------------------------------------------------------------------
for cow_ in controlCows:
    cow = controlCows[cow_]
    ind = cow['RelativeIdx']
    ind = ind.values
    ind = np.array(ind, dtype=float)

    x = [ind[s] for s in np.ma.clump_unmasked(np.ma.masked_invalid(ind))]
    result = [list(v) for k,v in groupby(ind, np.isfinite) if k]

    relativeMeas = {}
    for l in x:
        flip = np.flip(l)
        n = 0
        for i in range(len(l)):
            relativeMeas[flip[i]] = n
            n -= 1
                 
    measure = cow['RelativeIdx']
    mappingM = measure.map(relativeMeas)
    cow['RelativeMeasure'] = mappingM   
    
    
# =============================================================================
# Separating into periods
# =============================================================================

for cow_ in controlCows:
    cow = controlCows[cow_]
    rt = cow['RelativeTime']
    rtID = cow['RelativeTimePos']

    mappingRT = rt.map(rtDict)
    cow['RelativePeriod'] = mappingRT
    
    mappingRTID = rtID.map(rtIDDict)
    cow['RelativePeriodID'] = mappingRTID
        
           
# Updated frame
# -----------------------------------------------------------------------------        
controlFrame = pd.DataFrame()

for cow_ in controlCows:
    cow = controlCows[cow_]
    controlFrame = controlFrame.append(cow)
    
    
# Adding unique case ID
# -----------------------------------------------------------------------------
testBase = controlFrame[controlFrame['Period'] == 1]
testBase = testBase.reset_index()
caseIDBase = []

idx = list(testBase.index)
c = c+1

for i in range(1, len(testBase)-1):
    prev = testBase.loc[idx[i-1], :]
    row = testBase.loc[idx[i], :]

    cow1 = prev['tagLact']
    cow2 = row['tagLact']
    
    per1 = prev['RelativePeriod']
    per2 = row['RelativePeriod']
    
    if cow1 == cow2:
        if (per2-per1) < 2:
            caseIDBase.append(c)
        else:
            caseIDBase.append(c)
            c = c+1
    else:
        caseIDBase.append(c)
        c = c+1

# Adding for the last rows 
mis = len(testBase) - len(caseIDBase)
for i in range(mis):
    caseIDBase.append(c)

testBase['CaseID'] = caseIDBase
            

# =============================================================================
# Merging and exporting files
# =============================================================================

# Merging
data = pd.concat([test, testBase])

# Reducing columns
columns = ['tagtag', 'datetag', 'tagLact', 'dim', 'amount', 'OCC', 'ECIQR', 
           'ECMax', 'ECAvg', 'ECVar', 'MYIQR', 'FIQR', 'mastitis', 'lnocc', 
           'ParityNumber', 'ParityGroup', 'Pathogen', 
           'RelativeTime', 'RelativeMeasure', 'RelativeTimePos', 
           'RelativePeriod', 'CaseID']
data = data[columns]
# Renaming columns
data.columns = ['Tag', 'Date', 'tagLact', 'DIM', 'MY', 'OCC', 'ECIQR', 
                'ECMax', 'ECAvg', 'ECVar', 'MYIQR', 'FIQR', 'Mastitis',
                'lnocc', 'ParityNumer', 'ParityGroup', 'Pathogen', 
                'RelativeTime', 'RelativeMeasure', 'RelativeTimePos',
                'RelativePeriod', 'CaseID']

# Exporting
data.to_excel('StaphAureusID.xlsx')
data.to_csv('StaphAureusID.csv')
data.to_stata('StaphAureusID.dta')


"""
# Merging
data = pd.concat([pathFrameUp, controlFrame])

# Reducing columns
columns = ['tagtag', 'datetag', 'tagLact', 'dim', 'amount', 'OCC', 'ECIQR', 
           'ECMax', 'ECAvg', 'ECVar',
           'MYIQR', 'FIQR', 'lnocc', 'ParityNumber', 'ParityGroup', 'Pathogen', 
           'Period', 'RelativeTime', 'RelativeMeasure', 'RelativeTimeID', 
           'RelativePeriod', 'RelativePeriodID']
data = data[columns]
data.columns = ['Tag', 'Date', 'tagLact', 'DIM', 'MY', 'OCC', 'ECIQR', 
                'ECMax', 'ECAvg', 'ECVar',
                'MYIQR', 'FIQR', 'lnocc', 'ParityNumer', 'ParityGroup', 'Pathogen', 
                'Period', 'RelativeTime', 'RelativeMeasure', 'RelativeTimeID',
                'RelativePeriod', 'RelativePeriodID']

# Replacing nan with 0's
# data['Pathogen'] = data['Pathogen'].replace(np.nan, 0)
data = data[data['Period'] == 1]
data.isna().sum()

# Exporting
data.to_excel('StaphAuruesID.xlsx')
data.to_csv('StaphAureusID.csv')
data.to_stata('StaphAureusID.dta')


# Statistics of merged file
InsidePeriod = data[data['Period'] == 1]
inf = InsidePeriod[InsidePeriod['Pathogen'] == 1]
lactInf = list(inf['tagLact'].unique())
lactNo = list(InsidePeriod['tagLact'].unique())

"""