# -*- coding: utf-8 -*-
"""
Created on Mon Jul 25 10:21:00 2022

@author: cholofss
"""

"""
A script containing the code for different visualizations of the patterns
in the traits as a function of the days in milk. 
"""

# =============================================================================
# Importing modules
# =============================================================================

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import wavelets as wl


# =============================================================================
# Figure for average pattern
# =============================================================================

def plot_avg_pattern(path, name,  xb, traits, labels):
    """
    A function for plotting the average values as a function of days in milk.
    Comaring the average for a given pathogen against the baseline. 
    
    Parameters
    ----------
    path : DataFrame
        Dataframe containing the samples with positive results for the 
        specific pathogen.
    name : Str
        Name of the pathogen.
    xb : Dataframe
        Dataframe containing the healthy cows, the baseline.
    traits: List
        List containing three traits to plot.
    labels: List
        List containing labels for the three traits.
    Returns
    -------
    Figure.

    """
    
    x = path.groupby(by=path.index).mean() 
    baseline = xb.copy()
    a, b, c = traits[0], traits[1], traits[2]
    al, bl, cl = labels[0], labels[1], labels[2]
    
    # Figure
    # -------------------------------------------------------------------------
    fig, axs = plt.subplots(3, figsize=(13, 7), tight_layout=True,
                            sharex=True, sharey=False)
    sns.set_theme()
    # fig.suptitle('Avergae values based on the cows infected with ' + str(name), fontsize=14)
    
    # First trait
    # -------------------------------------------------------------------------
    x1 = x[a]
    xw1 = wl.lowpassfilter(x1.values.squeeze(), thresh=0.3, wavelet='coif8')
    
    x2 = baseline[a]
    xw2 = wl.lowpassfilter(x2.values.squeeze(), thresh=0.3, wavelet='coif8')
    
    if len(xw1) > len(x1):
        xw1 = xw1[:-1]
    if len(xw2) > len(x2):
        xw2 = xw2[:-1]
    
    # axs[0].plot(x1, color='#03A89E', linewidth=2.0)
    axs[0].plot(x1.index, xw1, color='#1874CD', linewidth=2.0)
    axs[0].scatter(x1.index, x1, color='#03A89E', label=name)
    
    # axs[0].plot(x2, color='#006400', linewidth=2.0)
    axs[0].plot(x2.index, xw2, color='#006400', linewidth=2.0)
    axs[0].scatter(x2.index, x2, color='#A2CD5A', label='Baseline')
    
    axs[0].set_ylabel(al, fontsize=11)
    axs[0].tick_params(axis='y', labelsize=10)
    axs[0].legend(loc='lower right', bbox_to_anchor=(1.1, 0.3), fontsize=10)
    
    # Second trait
    # -------------------------------------------------------------------------
    x3 = x[b]
    xw3 = wl.lowpassfilter(x3.values.squeeze(), thresh=0.3, wavelet='coif8')
    
    x4 = baseline[b]
    xw4 = wl.lowpassfilter(x4.values.squeeze(), thresh=0.3, wavelet='coif8')
    
    if len(xw3) > len(x3):
        xw3 = xw3[:-1]
    if len(xw4) > len(x4):
        xw4 = xw4[:-1]

    # axs[1].plot(x3.index, x3, color='#FFB90F', linewidth=2.0)
    axs[1].plot(x3.index, xw3, color='#FF9912', linewidth=2.0)
    axs[1].scatter(x3.index, x3, label=name, color='#FFB90F')
    
    axs[1].plot(x4, color='#006400', linewidth=2.0)
    axs[1].plot(x4.index, xw4, color='#006400', linewidth=2.0)
    axs[1].scatter(x4.index, x4, label='Baseline', color='#A2CD5A')
    
    axs[1].set_ylabel(bl, fontsize=11)
    axs[1].tick_params(axis='y', labelsize=10)
    axs[1].legend(loc='lower right', bbox_to_anchor=(1.1, 0.3), fontsize=10)
    
    # Third trait
    # -------------------------------------------------------------------------
    x5 = x[c]
    xw5 = wl.lowpassfilter(x5.values.squeeze(), thresh=0.3, wavelet='coif8')
    
    x6 = baseline[c]
    xw6 = wl.lowpassfilter(x6.values.squeeze(), thresh=0.3, wavelet='coif8')
    
    if len(xw5) > len(x5):
        xw5 = xw5[:-1]
    if len(xw6) > len(x6):
        xw6 = xw6[:-1]
    
    # axs[2].plot(x5, color='purple', linewidth=2.0)
    axs[2].plot(x5.index, xw5, color='pink', linewidth=2.0)
    axs[2].scatter(x5.index, x5, label=name, color='purple')
    
    # axs[2].plot(x6, color='#006400', linewidth=2.0)
    axs[2].plot(x6.index, xw6, color='#006400', linewidth=2.0)
    axs[2].scatter(x6.index, x6, label='Baseline', color='#A2CD5A')
    
    axs[2].set_ylabel(cl, fontsize=11)
    axs[2].legend(loc='lower right', bbox_to_anchor=(1.1, 0.3), fontsize=10)
    
    # X-label
    axs[2].set_xlabel('Days in milk', fontsize=11)
    axs[2].xaxis.set_major_locator(plt.MaxNLocator(12))
    axs[2].tick_params(axis='x', rotation=12)
    
    plt.show()  
    

# =============================================================================
# Figure for average pattern including area for deviation
# =============================================================================

def plot_avg_std_pattern(path, name, xb, xs, traits, labels, span=7):
    """
    A function for plotting the average values as a function of days in milk.
    Comaring the average for a given pathogen against the baseline. 
    
    Parameters
    ----------
    path : DataFrame
        Dataframe containing the samples with positive results for the 
        specific pathogen.
    name : Str
        Name of the pathogen.
    xb : Dataframe
        Dataframe containing the healthy cows, the baseline.
    span: Int
        Number of days the moving average is calculated across.

    Returns
    -------
    Figure.

    """
    
    xm = path.groupby(by=path.index).mean() 
    xs = path.groupby(by=path.index).std()
    baseline = xb.copy()
    xsb = xs.copy()
    
    a, b, c = traits[0], traits[1], traits[2]
    al, bl, cl = labels[0], labels[1], labels[2]
    
    # Figure
    # -------------------------------------------------------------------------
    fig, axs = plt.subplots(3, figsize=(13, 7), tight_layout=True,
                            sharex=True, sharey=False)
    sns.set_theme() 
    fig.suptitle(str(name), fontsize=20)
    
    # OCC
    # -------------------------------------------------------------------------
    x1 = xm[a]
    x2 = xm.loc[:, a].ewm(span=span, adjust=False).mean()
    xw1 = wl.lowpassfilter(x1.values.squeeze(), thresh=0.3, wavelet='coif8')
    # xw1 = pd.DataFrame(xw1, index=x1.index)
    x3 = baseline[a]
    x4 = baseline.loc[:, a].ewm(span=span, adjust=False).mean()
    xw2 = wl.lowpassfilter(x3.values.squeeze(), thresh=0.3, wavelet='coif8')
    xs1 = xm[a] - 1*xs[a]
    xs2 = xm[a] + 1*xs[a]
    xsb1 = baseline[a] - 1*xsb[a]
    xsb2 = baseline[a] + 1*xsb[a]
    
    if len(xw1) > len(x1):
        xw1 = xw1[:-1]
    xw1 = pd.Series(xw1, index=x1.index)
    
    if len(xw2) > len(x3):
        xw2 = xw2[:-1]
    xw2 = pd.Series(xw2, index=x3.index)
    
    
    # axs[0].plot(x1, color='#03A89E', label='OCC [1000 c/ml]', linewidth=2.0)
    axs[0].scatter(x1.index, x1, color='#03A89E', label=name)
    axs[0].plot(x2, color='#1874CD', linewidth=2.0)
    # axs[0].plot(x1.index, xw1, color='#191970', label='DWT', linewidth=2.0)
    
    axs[0].scatter(x3.index, x3, color='#A2CD5A', label='Baseline')
    axs[0].plot(x4, color='#006400', linewidth=2.0)
    
    axs[0].fill_between(baseline.index, xsb1, xsb2, color='#A2CD5A', alpha=0.35)
    axs[0].fill_between(xm.index, xs1, xs2, color='#03A89E', alpha=0.35)
    
    axs[0].set_ylabel(al, fontsize=16)
    axs[0].tick_params(axis='y', labelsize=16)
    axs[0].legend(loc='lower right', bbox_to_anchor=(1.15, 0.5), fontsize=14)
    axs[0].set_ylim([1.5, 8])
    
    # EC
    # -------------------------------------------------------------------------
    x5 = xm[b]
    x6 = xm.loc[:, b].ewm(span=span, adjust=False).mean()
    xw5 = wl.lowpassfilter(x5.values.squeeze(), thresh=0.3, wavelet='coif8')
    # xw5 = pd.DataFrame(xw5, index=x5.index)
    x7 = baseline[b]
    x8 = baseline.loc[:, b].ewm(span=span, adjust=False).mean()
    xw6 = wl.lowpassfilter(x7.values.squeeze(), thresh=0.3, wavelet='coif8')
    xs3 = xm[b] - xs[b]
    xs4 = xm[b] + xs[b]
    xsb3 = baseline[b] - 1*xsb[b]
    xsb4 = baseline[b] + 1*xsb[b]
    
    if len(xw5) > len(x5):
        xw5 = xw5[:-1]
    xw5 = pd.Series(xw5, index=x5.index)
        
    if len(xw6) > len(x7):
        xw6 = xw6[:-1]
    xw6 = pd.Series(xw6, index=x7.index)
    
    
    # axs[1].plot(x5, label='EC (IQR)', color='#FFB90F', linewidth=2.0)
    axs[1].scatter(x5.index, x5, label=name, color='#FFB90F')
    axs[1].plot(x6, color='#FF9912', linewidth=2.0)
    # axs[1].plot(x5.index, xw5, color='#5E2612', label='DWT', linewidth=2.0)

    axs[1].scatter(x7.index, x7, label='Baseline', color='#A2CD5A')
    axs[1].plot(x8, color='#006400', linewidth=2.0)

    axs[1].fill_between(baseline.index, xsb3, xsb4, color='#A2CD5A', alpha=0.35)
    axs[1].fill_between(xm.index, xs3, xs4, color='#FFB90F', alpha=0.35)
    
    axs[1].set_ylabel(bl, fontsize=16)
    axs[1].tick_params(axis='y', labelsize=16)
    axs[1].legend(loc='lower right', bbox_to_anchor=(1.15, 0.5), fontsize=14)
    # axs[1].set_ylim([0.5, 1.7])
    axs[1].set_ylim([-0.1, 0.6])
    
    # Milk yield
    # -------------------------------------------------------------------------
    x9 = xm[c]
    x10 = xm.loc[:, c].ewm(span=span, adjust=False).mean()
    xw9 = wl.lowpassfilter(x9.values.squeeze(), thresh=0.3, wavelet='coif8')
    # xw9 = pd.DataFrame(xw9, index=x9.index)
    x11 = baseline[c]
    x12 = baseline.loc[:, c].ewm(span=span, adjust=False).mean()
    xw10 = wl.lowpassfilter(x11.values.squeeze(), thresh=0.3, wavelet='coif8')
    xs5 = xm[c] - xs[c]
    xs6 = xm[c] + xs[c]
    xsb5 = baseline[c] - 1*xsb[c]
    xsb6 = baseline[c] + 1*xsb[c]
    
    if len(xw9) > len(x9):
        xw9 = xw9[:-1]
    xw9 = pd.Series(xw9, index=x9.index)
        
    if len(xw10) > len(x11):
        xw10 = xw10[:-1]
    xw10 = pd.Series(xw10, index=x11.index)
    
    # axs[2].plot(x9, label='Yield [kg]', color='purple', linewidth=2.0)
    axs[2].scatter(x9.index, x9, label=name, color='purple')
    axs[2].plot(x10, color='pink', linewidth=2.0)
    # axs[2].plot(x9.index, xw9, color='#EE30A7', label='DWT', linewidth=2.0)

    axs[2].scatter(x11.index, x11, label='Baseline', color='#A2CD5A')
    axs[2].plot(x12, color='#006400', linewidth=2.0)

    axs[2].fill_between(baseline.index, xsb5, xsb6, color='#A2CD5A', alpha=0.35)
    axs[2].fill_between(xm.index, xs5, xs6, color='purple', alpha=0.35)
    
    axs[2].set_ylabel(cl, fontsize=16)
    axs[2].tick_params(axis='y', labelsize=16)
    axs[2].legend(loc='lower right', bbox_to_anchor=(1.15, 0.5), fontsize=14)
    axs[2].set_ylim([2, 25])
    
    # X-label
    axs[2].set_xlabel('Days in milk', fontsize=16)
    axs[2].xaxis.set_major_locator(plt.MaxNLocator(12))
    axs[2].tick_params(axis='x', labelsize=16)
    
    plt.show()  
    
    
    # Returning smoothed values
    # -------------------------------------------------------------------------
    values = [x3, x4, xw2, x1, x2, xw1,
              x7, x8, xw6, x5, x6, xw5,
              x11, x12, xw10, x9, x10, xw9]
    
    return values


# =============================================================================
# Figure for comparing individual cows against average
# =============================================================================

def plot_ind_pattern(xc, xp, xb, tag, path, name, label, save=False):
    """
    A function for plotting the patterns as a function of days in milk
    for each individual cow. Comparing the pattern agains average for
    infection with the given pathogen and the baseline.

    Parameters
    ----------
    xc : DataFrame
        Dataframe for the cow.
    xp : DataFrame
        Dataframe for all infections with the given pathogen.
    xb : DataFrame
        Dataframe with the baseline.
    tag : str
        Tagnumber for the cow.
    path : DataFrame
        Dataframe for pathogen, grouped by mean value.
    name : str
        Name of the pathogen.
    label : str
        Label for pathogen.
    save : Boolean, optional
        If the figure shall be saved. The default is False.

    Returns
    -------
    Figure.

    """
    

    x = xp.copy()
    baseline = xb.copy()
    cow = xc.copy()
    
    fig, axs = plt.subplots(4, figsize=(13, 7), tight_layout=True,
                            sharex=True, sharey=False)
    sns.set_theme() 
    fig.suptitle('Cow with taglact: ' + str(tag), fontsize=14)
    
    # OCC
    # -------------------------------------------------------------------------
    x1 = x['lnocc']
    x1EMA = x.loc[:,'lnocc'].ewm(span=7, adjust=False).mean()
    x2 = baseline['lnocc']
    x2EMA = baseline.loc[:,'lnocc'].ewm(span=7, adjust=False).mean()
    x3 = cow['lnocc']
    x3EMA = cow.loc[:,'lnocc'].ewm(span=7, adjust=False).mean()
    
    axs[0].plot(x2EMA, color='#006400', linewidth=2.0)
    axs[0].scatter(x2.index, x2, color='#A2CD5A', label='Baseline')
    # axs[0].plot(x1EMA, color='#1874CD', linewidth=2.0)
    # axs[0].scatter(x1.index, x1, color='#03A89E', label=str(name))
    axs[0].plot(x3EMA, color='#1874CD', linewidth=2.0)
    axs[0].scatter(x3.index, x3, color='#03A89E', label='Cow')
    
    axs[0].set_ylabel('ln(OCC) (1000 c/ml)', fontsize=13)
    axs[0].tick_params(axis='y', labelsize=12)
    axs[0].legend(loc='lower right', bbox_to_anchor=(1.1, 0.5))
    # axs[0].set_ylim([0, 2500])
    
    # EC
    # ---------------------------------------------------------------------
    x3 = x['ECIQR']
    x3EMA = x.loc[:,'ECIQR'].ewm(span=7, adjust=False).mean()
    x4 = baseline['ECIQR']
    x4EMA = baseline.loc[:,'ECIQR'].ewm(span=7, adjust=False).mean()
    x5 = cow['ECIQR']
    x5EMA = cow.loc[:,'ECIQR'].ewm(span=7, adjust=False).mean()
    
    axs[1].plot(x4EMA, color='#006400', linewidth=2.0)
    axs[1].scatter(x4.index, x4, label='Baseline', color='#A2CD5A')
    # axs[1].plot(x3EMA, color='#FF9912', linewidth=2.0)
    # axs[1].scatter(x3.index, x3, label=str(name), color='#FFB90F')
    axs[1].plot(x5EMA, color='#FF9912', linewidth=2.0)
    axs[1].scatter(x5.index, x5, color='#FFB90F', label='Cow')
    
    axs[1].set_ylabel('EC (IQR)', fontsize=13)
    axs[1].tick_params(axis='y', labelsize=12)
    axs[1].legend(loc='lower right', bbox_to_anchor=(1.1, 0.5))
    
    # Milk yield
    # ---------------------------------------------------------------------
    x5 = x['amount']
    x5EMA = x.loc[:,'amount'].ewm(span=7, adjust=False).mean()
    x6 = baseline['amount']
    x6EMA = baseline.loc[:,'amount'].ewm(span=7, adjust=False).mean()
    x7 = cow['amount']
    x7EMA = cow.loc[:,'amount'].ewm(span=7, adjust=False).mean()
    
    axs[2].plot(x6EMA, color='#006400', linewidth=2.0)
    axs[2].scatter(x6.index, x6, label='Baseline', color='#A2CD5A')
    # axs[2].plot(x5EMA, color='purple', linewidth=2.0)
    # axs[2].scatter(x5.index, x5, label=str(name), color='pink')
    axs[2].plot(x7EMA, color='purple', linewidth=2.0)
    axs[2].scatter(x7.index, x7, color='#CD96CD', label='Cow')
    
    axs[2].set_ylabel('MY (kg)', fontsize=13)
    axs[2].legend(loc='lower right', bbox_to_anchor=(1.1, 0.5))
    
    
    # Culture results
    # ---------------------------------------------------------------------
    
    color_labels = cow['quarter'].unique()
    rgb_values = sns.color_palette('YlGnBu', 6)
    color_map = dict(zip(color_labels, rgb_values))
    
    # colors = {'COW':'#00008B', 'RF':'#6E8B3D', 'LF':'#FF3030', 'RH':'#8B6914', 'LH':'#9F79EE'}
    
    # axs[3].scatter(cow.index, cow[label], label='Bact', color='saddlebrown', marker='o')
    axs[3].scatter(cow.index, cow[label], c=cow['quarter'].map(color_map), marker='o')
    # axs[3].scatter(cow.index, cow['aurPcr'], label='PCR', color='lightcoral', marker='d')
    
    axs[3].set_ylabel(str(name), fontsize=13)
    axs[3].tick_params(axis='y', labelsize=12)
    axs[3].legend(loc='lower right', bbox_to_anchor=(1.1, 0.5))
    
    """
    
    # Culture results
    # -------------------------------------------------------------------------
    color_map = {np.nan: 0, 'RF':'#8A3324', 'LF':'#458B74', 'RH':'#EE7621', 'LH':'#7AC5CD'}
    x['Quarter'] = x['quarter']
    sns.scatterplot(data=x, x=x.index, y=label, hue='Quarter',
                    hue_order=['RF', 'LF', 'RH', 'LH'], palette=color_map, 
                    markers=['o', 'd', '8', '*'])

    axs[3].set_ylabel('Culture results ' + str(label), fontsize=15)
    axs[3].tick_params(axis='y', labelsize=16)
    axs[3].legend(loc='lower right', bbox_to_anchor=(1.05, 0.4))
    """

    # X-label
    # -------------------------------------------------------------------------
    axs[3].set_xlabel('Date', fontsize=13)
    axs[3].tick_params(axis='x', rotation=12)
    axs[3].xaxis.set_major_locator(plt.MaxNLocator(12))
    axs[3].set_ylim([-0.2, 1.1])
    
    # X-label
    axs[3].set_xlabel('Days in milk', fontsize=18)
    axs[3].xaxis.set_major_locator(plt.MaxNLocator(12))
    axs[3].tick_params(axis='x', rotation=12)
    
    # Save figure
    if save:
        fig.savefig(str(name) + '_' + str(tag) + '.pdf', bbox_inches='tight')
    
    plt.show()  
    
    
# =============================================================================
# Patterns at quarter-level for individual cows
# =============================================================================


def plot_ind_ql_pattern(xc, tag, name, save=False):
    
    
    x = xc.copy()
    
    # Figure
    # -------------------------------------------------------------------------
    fig, axs = plt.subplots(3, figsize=(13, 7), tight_layout=True,
                            sharex=True, sharey=False)
    sns.set_theme() 
    fig.suptitle('Pathogen: '+ str(name) + ' Tag: ' + str(tag), fontsize=14)

    # EC
    x1 = x['EC1']
    x2 = x['EC2']
    x3 = x['EC3']
    x4 = x['EC4']

    axs[0].plot(x1, color='#8A3324', label='RF', linewidth=2.0)
    axs[0].plot(x2, color='#458B74', label='LF', linewidth=2.0)
    axs[0].plot(x3, color='#EE7621', label='RH', linewidth=2.0)
    axs[0].plot(x4, color='#7AC5CD', label='LH', linewidth=2.0)

    axs[0].set_ylabel('EC (mS)', fontsize=16)
    axs[0].tick_params(axis='y', labelsize=16)
    axs[0].legend(loc='lower right', bbox_to_anchor=(1.05, 0.3))

    # Yield
    x5 = x['MY1']
    x6 = x['MY2']
    x7 = x['MY3']
    x8 = x['MY4']

    axs[1].plot(x5, color='#8A3324', label='RF', linewidth=2.0)
    axs[1].plot(x6, color='#458B74', label='LF', linewidth=2.0)
    axs[1].plot(x7, color='#EE7621', label='RH', linewidth=2.0)
    axs[1].plot(x8, color='#7AC5CD', label='LH', linewidth=2.0)

    axs[1].set_ylabel('MY (kg)', fontsize=16)
    axs[1].tick_params(axis='y', labelsize=16)
    axs[1].legend(loc='lower right', bbox_to_anchor=(1.05, 0.3))

    # Flow
    x9 = x['FA1']
    x10 = x['FA2']
    x11 = x['FA3']
    x12 = x['FA4']

    axs[2].plot(x9, color='#8A3324', label='RF', linewidth=2.0)
    axs[2].plot(x10, color='#458B74', label='LF', linewidth=2.0)
    axs[2].plot(x11, color='#EE7621', label='RH', linewidth=2.0)
    axs[2].plot(x12, color='#7AC5CD', label='LH', linewidth=2.0)

    axs[2].set_ylabel('Flow (average)', fontsize=16)
    axs[2].tick_params(axis='y', labelsize=15)
    axs[2].legend(loc='lower right', bbox_to_anchor=(1.05, 0.3))

    # X-label
    axs[2].set_xlabel('Days in milk', fontsize=20)
    axs[2].xaxis.set_major_locator(plt.MaxNLocator(12))
    axs[2].tick_params(axis='x', labelsize=16, rotation=12)
    
    # Save figure
    if save:
        fig.savefig(str(name) + '_' + str(tag) + '_QL.pdf', bbox_inches='tight')


    plt.show()   