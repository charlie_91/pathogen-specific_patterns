# -*- coding: utf-8 -*-
"""
Created on Mon Jul 25 09:28:05 2022

@author: cholofss
"""

"""
A function for creating boxplots
"""


# =============================================================================
# Importing modules
# =============================================================================

import pandas as pd
import numpy as np
import math
import matplotlib.pyplot as plt
import seaborn as sns


# =============================================================================
# Function 
# =============================================================================

def boxplot(xDf, x, y, hue, xlabel, ylabel, title):
    """
    
    Parameters
    ----------
    xDf : DataFrame
        Dataframe with the data.
    x : String
        Name of the x-variable.
    y : String
        Name of the y-variable.
    hue : String
        What to separate the boxes on.
    xlabel : String
        Title for x-label.
    ylabel : String
        Title for y-label.
    title : String
        Title of the figure.

    Returns
    -------
    A figure.

    """
    
    fig = plt.figure(figsize =(10, 7))
    ax = fig.add_subplot(111)
    sns.set_theme()                         
    
    ax = (
        xDf.set_index('Lactation', append=True)
          .stack()                       
          .to_frame()                   
          .reset_index()                
          .rename(columns={'level_2': hue, 0: y})  
          .drop('level_0', axis='columns')   
          .pipe((sns.boxplot, 'data'), x=x, y=y, hue=hue, 
                hue_order=['No infection', 'One infection', 'More than one infection'], palette='YlGnBu')  
    )
      
         
    # Labels
    ax.set_ylabel(ylabel, fontsize=14)
    ax.set_xlabel(xlabel, fontsize=14)
    ax.tick_params(axis='x', labelsize=12)
    ax.tick_params(axis='y', labelsize=12)
    ax.set_title(title, fontsize=16)
     
    # Show plot
    plt.show()
    
    
    